import TicTacToeApp from "@counterfactual/apps/build/TicTacToeApp.json";
import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import BalanceRefundApp from "@counterfactual/contracts/build/ETHBalanceRefundApp.json";
import ETHBucketApp from "@counterfactual/contracts/build/ETHBucket.json";
import ETHVirtualAppAgreementApp from "@counterfactual/contracts/build/ETHVirtualAppAgreement.json";
import MinimumViableMultisig from "@counterfactual/contracts/build/MinimumViableMultisig.json";
import MultiSendApp from "@counterfactual/contracts/build/MultiSend.json";
import NonceRegistryApp from "@counterfactual/contracts/build/NonceRegistry.json";
import ProxyFactory from "@counterfactual/contracts/build/ProxyFactory.json";
import StateChannelTransactionApp from "@counterfactual/contracts/build/StateChannelTransaction.json";
import LibTransfer from "@counterfactual/contracts/build/Transfer.json";
import * as waffle from "ethereum-waffle";
import { Wallet } from "ethers";
import { BigNumber } from "ethers/utils";

export async function configureNetworkContext(wallet: Wallet) {
  const libTransferContract = await waffle.deployContract(wallet, LibTransfer);

  const balanceRefundContract = await waffle.deployContract(
    wallet,
    BalanceRefundApp
  );

  const mvmContract = await waffle.deployContract(
    wallet,
    MinimumViableMultisig
  );

  const proxyFactoryContract = await waffle.deployContract(
    wallet,
    ProxyFactory
  );

  const tttContract = await waffle.deployContract(wallet, TicTacToeApp);

  const appRegistryContract = await waffle.deployContract(
    wallet,
    AppRegistryApp,
    [],
    { gasLimit: new BigNumber("5500000") }
  );

  waffle.link(
    StateChannelTransactionApp,
    "contracts/libs/Transfer.sol:Transfer",
    libTransferContract.address
  );
  const stateChannelTransactionContract = await waffle.deployContract(
    wallet,
    StateChannelTransactionApp
  );

  const nonceRegistryContract = await waffle.deployContract(
    wallet,
    NonceRegistryApp
  );

  const multiSendContract = await waffle.deployContract(wallet, MultiSendApp);

  waffle.link(
    ETHVirtualAppAgreementApp,
    "contracts/libs/Transfer.sol:Transfer",
    libTransferContract.address
  );
  const ethVirtualAppAgreementContract = await waffle.deployContract(
    wallet,
    ETHVirtualAppAgreementApp
  );

  const ethBucketContract = await waffle.deployContract(wallet, ETHBucketApp);

  return {
    AppRegistry: appRegistryContract.address,
    NonceRegistry: nonceRegistryContract.address,
    ETHBalanceRefundApp: balanceRefundContract.address,
    ETHBucket: ethBucketContract.address,
    ETHVirtualAppAgreement: ethVirtualAppAgreementContract.address,
    MinimumViableMultisig: mvmContract.address,
    ProxyFactory: proxyFactoryContract.address,
    TicTacToe: tttContract.address,
    StateChannelTransaction: stateChannelTransactionContract.address,
    MultiSend: multiSendContract.address
  };
}
