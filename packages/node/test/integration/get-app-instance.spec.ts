import { Node as NodeTypes } from "@counterfactual/types";
import { JsonRpcProvider } from "ethers/providers";
import { v4 as generateUUID } from "uuid";

import {
  IMessagingService,
  InstallMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeMessage
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  getMultisigCreationTransactionHash,
  makeInstallProposalRequest
} from "./utils";

describe("Node method follows spec - getAppInstanceDetails", () => {
  jest.setTimeout(15000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  it("can accept a valid call to get the desired AppInstance details", async done => {
    nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
      const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
        nodeA.publicIdentifier,
        nodeB.publicIdentifier
      );

      const installAppInstanceRequestId = generateUUID();
      let installedAppInstanceId: String;

      nodeA.on(NODE_EVENTS.INSTALL, async (res: InstallMessage) => {
        installedAppInstanceId = res.data.installedId!;

        const getAppInstancesRequest: NodeTypes.MethodRequest = {
          requestId: generateUUID(),
          type: NodeTypes.MethodName.GET_APP_INSTANCE_DETAILS,
          params: {
            appInstanceId: installedAppInstanceId
          } as NodeTypes.GetAppInstanceDetailsParams
        };

        const response: NodeTypes.MethodResponse = await nodeA.callStatic(
          getAppInstancesRequest
        );
        const appInstanceInfo = (response.result as NodeTypes.GetAppInstanceDetailsResult)
          .appInstance;

        // expect(installedAppInstanceId).toEqual(appInstanceInfo.id);
        expect(nodeA.publicIdentifier).toEqual(
          appInstanceInfo.proposedByIdentifier
        );
        expect(nodeB.publicIdentifier).toEqual(
          appInstanceInfo.proposedToIdentifier
        );
        done();
      });

      nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, (res: ProposeMessage) => {
        const appInstanceId = res.data.appInstanceId!;
        expect(appInstanceId).toBeDefined();
        const installAppInstanceRequest: NodeTypes.MethodRequest = {
          requestId: installAppInstanceRequestId,
          type: NodeTypes.MethodName.INSTALL,
          params: {
            proposedId: appInstanceId
          } as NodeTypes.InstallParams
        };

        nodeB.call(installAppInstanceRequest);
      });

      nodeA.call(appInstanceInstallationProposalRequest);
    });
    await getMultisigCreationTransactionHash(nodeA, [
      nodeA.publicIdentifier,
      nodeB.publicIdentifier
    ]);
  });
});
