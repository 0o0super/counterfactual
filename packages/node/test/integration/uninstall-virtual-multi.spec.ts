import { Node as NodeTypes } from "@counterfactual/types";
import { JsonRpcProvider } from "ethers/providers";
import { BigNumber } from "ethers/utils";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { APP_INSTANCE_STATUS } from "../../src/db-schema";
import { xkeyKthAddress } from "../../src/machine";
import {
  InstallVirtualMessage,
  NODE_EVENTS,
  ProposeVirtualMessage
} from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  awaitEvent,
  confirmProposedVirtualAppInstanceOnNode,
  generateUninstallVirtualRequest,
  getApps,
  getFreeBalanceState,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeDepositRequest,
  makeInstallVirtualRequest,
  makeTTTVirtualAppInstanceProposalReq,
  subscriptEvents
} from "./utils";

describe("Node method follows spec - uninstall", () => {
  jest.setTimeout(80000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeD: Node;
  let nodeE: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  const ALICE_INITIAL_DEPOSIT = 10000;
  const BOB_INITIAL_DEPOSIT = 10000;
  const ALICE_APP_DEPOSIT = 100;
  const BOB_APP_DEPOSIT = 200;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC, nodeD, nodeE] = await createNodes(
      5,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
    subscriptEvents(nodeA);
    subscriptEvents(nodeB);
    subscriptEvents(nodeC);
    subscriptEvents(nodeD);
    subscriptEvents(nodeE);
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });
  describe(
    "Node A and D install a Virtual AppInstance through an intermediary Node B and C," +
      "then Node A uninstalls the installed AppInstance",
    () => {
      it("sends uninstall ", async done => {
        const nodes = [nodeA, nodeB, nodeC, nodeD, nodeE];

        const stateEncoding =
          "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)";
        const actionEncoding =
          "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)";

        const initialState = {
          players: [
            xkeyKthAddress(nodeA.publicIdentifier, 0), // <-- winner
            xkeyKthAddress(nodeE.publicIdentifier, 0)
          ],
          turnNum: 0,
          winner: 1, // Hard-coded winner for test
          board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        };

        // get all ledger channel setup
        const multiSigAddresses: Map<string, string> = new Map();
        for (let i = 0; i < nodes.length - 1; i += 1) {
          getMultisigCreationTransactionHash(nodes[i], [
            nodes[i].publicIdentifier,
            nodes[i + 1].publicIdentifier
          ]);
          let createdMultiSigAddr = (await awaitEvent(
            nodes[i + 1],
            NODE_EVENTS.CREATE_CHANNEL
          )) as any;
          if (createdMultiSigAddr.data) {
            createdMultiSigAddr = createdMultiSigAddr.data;
          }
          createdMultiSigAddr = createdMultiSigAddr as NodeTypes.CreateChannelResult;
          multiSigAddresses.set(
            `node${i}node${i + 1}`,
            createdMultiSigAddr.multisigAddress
          );
        }

        // deposit some ether
        for (let i = 0; i < nodes.length; i += 1) {
          if (i !== 0) {
            const leftMiltiSigAddr = multiSigAddresses.get(
              `node${i - 1}node${i}`
            )!;
            const leftDepositReq = makeDepositRequest(
              leftMiltiSigAddr,
              new BigNumber(ALICE_INITIAL_DEPOSIT)
            );
            nodes[i].call(leftDepositReq);
            await awaitEvent(nodes[i - 1], NODE_EVENTS.DEPOSIT_CONFIRMED);
          }
          if (i !== nodes.length - 1) {
            const rightMiltiSigAddr = multiSigAddresses.get(
              `node${i}node${i + 1}`
            )!;
            const rightDepositReq = makeDepositRequest(
              rightMiltiSigAddr,
              new BigNumber(BOB_INITIAL_DEPOSIT)
            );
            nodes[i].call(rightDepositReq);
            await awaitEvent(nodes[i + 1], NODE_EVENTS.DEPOSIT_CONFIRMED);
          }
        }

        // propose virtual app install and wait for response
        const installVirtualAppInstanceProposalRequest = makeTTTVirtualAppInstanceProposalReq(
          nodeE.publicIdentifier,
          global["networkContext"].TicTacToe,
          initialState,
          {
            stateEncoding,
            actionEncoding
          },
          [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier,
            nodeD.publicIdentifier
          ],
          new BigNumber(ALICE_APP_DEPOSIT),
          new BigNumber(BOB_APP_DEPOSIT)
        );
        nodeA.call(installVirtualAppInstanceProposalRequest);
        const response = (await awaitEvent(
          nodeE,
          NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL
        )) as ProposeVirtualMessage;
        const appInstanceId = response.data.appInstanceId!;
        expect(appInstanceId).toBeDefined();

        // check if node A and D actually stor proposed app
        const proposedAppInstanceA = (await getProposedAppInstances(nodeA))[0];
        const proposedAppInstanceE = (await getProposedAppInstances(nodeE))[0];
        confirmProposedVirtualAppInstanceOnNode(
          installVirtualAppInstanceProposalRequest.params,
          proposedAppInstanceA
        );
        confirmProposedVirtualAppInstanceOnNode(
          installVirtualAppInstanceProposalRequest.params,
          proposedAppInstanceE
        );
        expect(proposedAppInstanceE.proposedByIdentifier).toEqual(
          nodeA.publicIdentifier
        );
        expect(proposedAppInstanceA.id).toEqual(proposedAppInstanceE.id);

        // Node D initiate install
        const installVirtualReq = makeInstallVirtualRequest(
          appInstanceId,
          response.data.intermediaries
        );
        nodeE.call(installVirtualReq);
        const installVirtResp = (await awaitEvent(
          nodeA,
          NODE_EVENTS.INSTALL_VIRTUAL
        )) as InstallVirtualMessage;

        // check if app actually got installed
        const virtualAppInstanceNodeA = (await getApps(
          nodeA,
          APP_INSTANCE_STATUS.INSTALLED
        ))[0];
        const virtualAppInstanceNodeE = (await getApps(
          nodeE,
          APP_INSTANCE_STATUS.INSTALLED
        ))[0];
        expect(virtualAppInstanceNodeA).toEqual(virtualAppInstanceNodeE);

        // Node A initiate uninstall app
        const uninstallReq = generateUninstallVirtualRequest(
          installVirtResp.data.installedId!,
          [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier,
            nodeD.publicIdentifier
          ]
        );
        nodeA.call(uninstallReq);
        await Promise.all(
          nodes.map(n => awaitEvent(n, NODE_EVENTS.UNINSTALL_VIRTUAL))
        );
        // await new Promise(r => setTimeout(r, 1500));

        // check if actually uninstalled
        expect(await getApps(nodeA, APP_INSTANCE_STATUS.INSTALLED)).toEqual([]);
        expect(await getApps(nodeE, APP_INSTANCE_STATUS.INSTALLED)).toEqual([]);

        // check if balance correct
        for (let i = 0; i < nodes.length - 1; i += 1) {
          const freeBalanceState = await getFreeBalanceState(
            nodes[i],
            multiSigAddresses.get(`node${i}node${i + 1}`)!
          );
          let leftBalance: BigNumber;
          let rightBalance: BigNumber;
          if (
            xkeyKthAddress(nodes[i].publicIdentifier, 0) ===
            freeBalanceState.alice
          ) {
            leftBalance = freeBalanceState.aliceBalance;
            rightBalance = freeBalanceState.bobBalance;
          } else {
            leftBalance = freeBalanceState.bobBalance;
            rightBalance = freeBalanceState.aliceBalance;
          }
          expect(leftBalance).toEqual(
            new BigNumber(ALICE_INITIAL_DEPOSIT + BOB_APP_DEPOSIT) // because Node A win
          );
          expect(rightBalance).toEqual(
            new BigNumber(BOB_INITIAL_DEPOSIT - BOB_APP_DEPOSIT)
          );
        }
        done();
      });
    }
  );
});
