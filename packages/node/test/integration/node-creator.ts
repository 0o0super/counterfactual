import { Wallet } from "ethers";
import { WeiPerEther } from "ethers/constants";
import { JsonRpcProvider, TransactionRequest } from "ethers/providers";
import { BigNumber } from "ethers/utils";
import { fromMnemonic } from "ethers/utils/hdnode";
import { v4 as generateUUID } from "uuid";

import { IMessagingService, MNEMONIC_PATH, Node, NodeConfig } from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";
import { A_MNEMONIC } from "../test-constants.jest";

export const getSomeEther = async (
  mnemonic: string,
  provider: JsonRpcProvider
) => {
  const CF_PATH = "m/44'/60'/0'/25446";
  const masterHDNode = fromMnemonic(A_MNEMONIC).derivePath(CF_PATH);
  const masterWallet = new Wallet(masterHDNode.privateKey, provider);
  const toAddress = fromMnemonic(mnemonic).derivePath(CF_PATH).address;

  const tx: TransactionRequest = {
    nonce: await provider.getTransactionCount(masterHDNode.address),
    gasLimit: 21000,
    gasPrice: new BigNumber("20000000000"),
    to: toAddress,
    value: WeiPerEther.mul(5),
    data: "0x"
  };

  const signedTx = await masterWallet.sign(tx);
  const txRes = await provider.sendTransaction(signedTx);
  await provider.waitForTransaction(txRes.hash!, 1);
};

export const createNodes = async (
  num: number,
  messagingService: IMessagingService,
  nodeConfig: NodeConfig,
  firebaseServiceFactory: LocalFirebaseServiceFactory,
  provider: JsonRpcProvider
) => {
  const nodes: Node[] = [];
  for (let i = 0; i < num; i += 1) {
    const storeService = firebaseServiceFactory.createStoreService(
      process.env.FIREBASE_STORE_SERVER_KEY! + generateUUID()
    );
    const node = await Node.create(
      messagingService,
      storeService,
      nodeConfig,
      provider,
      global["networkContext"]
    );
    const mn: string = await storeService.get(MNEMONIC_PATH);
    await getSomeEther(mn, provider);
    nodes.push(node);
  }

  return nodes;
};
