import { One } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";

import {
  CreateChannelMessage,
  IMessagingService,
  Node,
  NODE_EVENTS,
  NodeConfig
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  getMultisigCreationTransactionHash,
  makeDepositRequest,
  makeWithdrawRequest
} from "./utils";

describe("Node method follows spec - withdraw", () => {
  jest.setTimeout(30000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  it("has the right balance for both parties after withdrawal", async done => {
    nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async (data: CreateChannelMessage) => {
      const { multisigAddress } = data.data;

      expect(multisigAddress).toBeDefined();

      nodeA.on(NODE_EVENTS.DEPOSIT_CONFIRMED, async () => {
        expect(
          (await provider.getBalance(multisigAddress!)).toNumber()
        ).toEqual(1);

        nodeA.on(NODE_EVENTS.WITHDRAWAL_CONFIRMED, async () => {
          expect(
            (await provider.getBalance(multisigAddress!)).toNumber()
          ).toEqual(0);
          done();
        });
        const withdrawReq = makeWithdrawRequest(multisigAddress!, One);
        await nodeA.call(withdrawReq);
      });
      const depositReq = makeDepositRequest(multisigAddress!, One);
      await nodeA.call(depositReq);
    });
    await getMultisigCreationTransactionHash(nodeA, [
      nodeA.publicIdentifier,
      nodeB.publicIdentifier
    ]);
  });
});
