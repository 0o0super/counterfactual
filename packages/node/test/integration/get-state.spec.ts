import { Node as NodeTypes } from "@counterfactual/types";
import { JsonRpcProvider } from "ethers/providers";
import { v4 as generateUUID } from "uuid";

import {
  IMessagingService,
  InstallMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeMessage
} from "../../src";
import { ERRORS } from "../../src/methods/errors";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  generateGetStateRequest,
  getMultisigCreationTransactionHash,
  makeInstallProposalRequest
} from "./utils";

describe("Node method follows spec - getAppInstances", () => {
  jest.setTimeout(15000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  it("returns the right response for getting the state of a non-existent AppInstance", async () => {
    const getStateReq = generateGetStateRequest(generateUUID());
    expect(nodeA.callStatic(getStateReq)).rejects.toEqual(
      ERRORS.NO_MULTISIG_FOR_APP_INSTANCE_ID
    );
  });

  it("returns the right state for an installed AppInstance", async done => {
    nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
      const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
        nodeA.publicIdentifier,
        nodeB.publicIdentifier
      );

      nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, async (data: ProposeMessage) => {
        const installAppInstanceRequest: NodeTypes.MethodRequest = {
          requestId: generateUUID(),
          type: NodeTypes.MethodName.INSTALL,
          params: {
            proposedId: data.data.appInstanceId!
          } as NodeTypes.InstallParams
        };

        nodeA.on(NODE_EVENTS.INSTALL, async (msg: InstallMessage) => {
          const getStateReq = generateGetStateRequest(msg.data.installedId!);

          const getStateResult = await nodeA.callStatic(getStateReq);
          const state = (getStateResult.result as NodeTypes.GetStateResult)
            .state;
          expect(state.foo).toEqual(
            (appInstanceInstallationProposalRequest.params as NodeTypes.ProposeInstallParams)
              .initialState.foo
          );
          expect(state.bar).toEqual(
            (appInstanceInstallationProposalRequest.params as NodeTypes.ProposeInstallParams)
              .initialState.bar
          );
          done();
        });

        await nodeB.call(installAppInstanceRequest);
      });

      await nodeA.call(appInstanceInstallationProposalRequest);
    });

    await getMultisigCreationTransactionHash(nodeA, [
      nodeA.publicIdentifier,
      nodeB.publicIdentifier
    ]);
  });
});
