import { One } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";

import {
  CreateChannelMessage,
  IMessagingService,
  Node,
  NODE_EVENTS,
  NodeConfig
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  getFreeBalanceState,
  getMultisigCreationTransactionHash,
  makeDepositRequest
} from "./utils";

describe("Node method follows spec - deposit", () => {
  jest.setTimeout(60000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  it("has the right balance for both parties after deposits", async done => {
    nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async (data: CreateChannelMessage) => {
      const { multisigAddress } = data.data;
      const depositReq = makeDepositRequest(multisigAddress!, One);

      nodeA.call(depositReq);
      nodeB.on(NODE_EVENTS.DEPOSIT_CONFIRMED, async () => {
        nodeB.call(depositReq);
        nodeA.on(NODE_EVENTS.DEPOSIT_CONFIRMED, async () => {
          expect(
            (await provider.getBalance(multisigAddress!)).toNumber()
          ).toEqual(2);

          const freeBalanceState = await getFreeBalanceState(
            nodeA,
            multisigAddress!
          );

          expect(freeBalanceState.aliceBalance).toEqual(One);
          expect(freeBalanceState.bobBalance).toEqual(One);

          done();
        });
      });
    });
    await getMultisigCreationTransactionHash(nodeA, [
      nodeA.publicIdentifier,
      nodeB.publicIdentifier
    ]);
  });
});
