import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { NODE_EVENTS, ProposeVirtualMessage } from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedVirtualAppInstanceOnNode,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeInstallVirtualProposalRequest,
  makeRejectInstallRequest
} from "./utils";

describe("Node method follows spec - rejectInstallVirtual", () => {
  jest.setTimeout(20000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC] = await createNodes(
      3,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });
  describe(
    "Node A makes a proposal through an intermediary Node B to install a " +
      "Virtual AppInstance with Node C. Node C rejects proposal. Node A confirms rejection",
    () => {
      it("sends proposal with non-null initial state", async done => {
        nodeA.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
          nodeC.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
            const intermediaries = [nodeB.publicIdentifier];
            const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
              nodeA.publicIdentifier,
              nodeC.publicIdentifier,
              intermediaries
            );

            nodeA.on(NODE_EVENTS.REJECT_INSTALL_VIRTUAL, async () => {
              expect((await getProposedAppInstances(nodeA)).length).toEqual(0);
              done();
            });

            nodeC.on(
              NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
              async (msg: ProposeVirtualMessage) => {
                expect(msg.data.appInstanceId).toBeDefined();
                const proposedAppInstanceA = (await getProposedAppInstances(
                  nodeA
                ))[0];
                const proposedAppInstanceC = (await getProposedAppInstances(
                  nodeC
                ))[0];

                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceA
                );
                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceC
                );

                expect(proposedAppInstanceC.proposedByIdentifier).toEqual(
                  nodeA.publicIdentifier
                );
                expect(proposedAppInstanceA.id).toEqual(
                  proposedAppInstanceC.id
                );

                const rejectReq = makeRejectInstallRequest(
                  msg.data.appInstanceId!
                );

                await nodeC.call(rejectReq);

                expect((await getProposedAppInstances(nodeC)).length).toEqual(
                  0
                );
              }
            );

            await nodeA.call(installVirtualAppInstanceProposalRequest);
          });

          await getMultisigCreationTransactionHash(nodeB, [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier
          ]);
        });

        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });
    }
  );
});
