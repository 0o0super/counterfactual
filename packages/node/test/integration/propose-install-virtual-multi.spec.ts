import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { ERRORS } from "../../src/methods/errors";
import { NODE_EVENTS, ProposeVirtualMessage } from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedVirtualAppInstanceOnNode,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeInstallVirtualProposalRequest
} from "./utils";

describe("Node method follows spec - proposeInstallVirtual", () => {
  jest.setTimeout(80000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeD: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC, nodeD] = await createNodes(
      5,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });
  describe(
    "Node A makes a proposal through an intermediary Node B and C to install a " +
      "Virtual AppInstance with Node D. All Nodes confirm receipt of proposal",
    () => {
      it("sends proposal with non-null initial state", async done => {
        nodeD.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
          // A propose to D
          const intermediaries = [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier
          ];
          const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
            nodeA.publicIdentifier,
            nodeD.publicIdentifier,
            intermediaries
          );
          // handle event
          nodeD.on(
            NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
            async (msg: ProposeVirtualMessage) => {
              const appInstanceId = msg.data.appInstanceId;
              expect(appInstanceId).toBeDefined();

              const proposedAppInstanceA = (await getProposedAppInstances(
                nodeA
              ))[0];
              const proposedAppInstanceB = (await getProposedAppInstances(
                nodeB
              ))[0];
              const proposedAppInstanceC = (await getProposedAppInstances(
                nodeC
              ))[0];
              const proposedAppInstanceD = (await getProposedAppInstances(
                nodeD
              ))[0];

              confirmProposedVirtualAppInstanceOnNode(
                installVirtualAppInstanceProposalRequest.params,
                proposedAppInstanceA
              );
              confirmProposedVirtualAppInstanceOnNode(
                installVirtualAppInstanceProposalRequest.params,
                proposedAppInstanceB
              );
              confirmProposedVirtualAppInstanceOnNode(
                installVirtualAppInstanceProposalRequest.params,
                proposedAppInstanceC
              );
              confirmProposedVirtualAppInstanceOnNode(
                installVirtualAppInstanceProposalRequest.params,
                proposedAppInstanceD
              );

              expect(proposedAppInstanceD.proposedByIdentifier).toEqual(
                nodeA.publicIdentifier
              );
              expect(proposedAppInstanceA.id).toEqual(proposedAppInstanceB.id);
              expect(proposedAppInstanceB.id).toEqual(proposedAppInstanceC.id);
              expect(proposedAppInstanceC.id).toEqual(proposedAppInstanceD.id);
              done();
            }
          );

          await nodeA.call(installVirtualAppInstanceProposalRequest);
        });

        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
        await getMultisigCreationTransactionHash(nodeB, [
          nodeB.publicIdentifier,
          nodeC.publicIdentifier
        ]);
        await getMultisigCreationTransactionHash(nodeC, [
          nodeC.publicIdentifier,
          nodeD.publicIdentifier
        ]);
      });

      it("sends proposal with null initial state", async () => {
        const intermediaries = [nodeB.publicIdentifier, nodeC.publicIdentifier];
        const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
          nodeA.publicIdentifier,
          nodeD.publicIdentifier,
          intermediaries,
          true
        );

        expect(
          nodeA.call(installVirtualAppInstanceProposalRequest)
        ).rejects.toEqual(ERRORS.NULL_INITIAL_STATE_FOR_PROPOSAL);
      });
    }
  );
});
