import { AppInstanceInfo, Node as NodeTypes } from "@counterfactual/types";
import { JsonRpcProvider } from "ethers/providers";
import { v4 as generateUUID } from "uuid";

import {
  IMessagingService,
  InstallMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeMessage
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  getInstalledAppInstances,
  getMultisigCreationTransactionHash,
  makeInstallProposalRequest
} from "./utils";

describe("Node method follows spec - getAppInstances", () => {
  jest.setTimeout(15000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  it("can accept a valid call to get empty list of app instances", async () => {
    const appInstances: AppInstanceInfo[] = await getInstalledAppInstances(
      nodeA
    );
    expect(appInstances).toEqual([]);
  });

  it("can accept a valid call to get non-empty list of app instances", async done => {
    nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
      const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
        nodeA.publicIdentifier,
        nodeB.publicIdentifier
      );

      const installAppInstanceRequestId = generateUUID();
      // let installedAppInstanceId: String;

      const getAppInstancesRequestId = generateUUID();
      const getAppInstancesRequest: NodeTypes.MethodRequest = {
        requestId: getAppInstancesRequestId,
        type: NodeTypes.MethodName.GET_APP_INSTANCES,
        params: {} as NodeTypes.GetAppInstancesParams
      };

      nodeA.on(NODE_EVENTS.INSTALL, async (res: InstallMessage) => {
        const installResult = res.data.installedId!;
        // installedAppInstanceId = installResult;
        expect(installResult).toBeDefined();
        const methodResponse = await nodeA.callStatic(getAppInstancesRequest);
        const getResult = methodResponse.result as NodeTypes.GetAppInstancesResult;

        expect(getAppInstancesRequest.type).toEqual(methodResponse.type);
        expect(methodResponse.requestId).toEqual(getAppInstancesRequestId);

        const appInstance = getResult.appInstances[0];
        // expect(appInstance.id).toEqual(installedAppInstanceId);
        expect(appInstance.proposedByIdentifier).toEqual(
          nodeA.publicIdentifier
        );
        expect(appInstance.proposedToIdentifier).toEqual(
          nodeB.publicIdentifier
        );
        done();
      });

      nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, (res: ProposeMessage) => {
        const appInstanceId = res.data.appInstanceId!;
        const installAppInstanceRequest: NodeTypes.MethodRequest = {
          requestId: installAppInstanceRequestId,
          type: NodeTypes.MethodName.INSTALL,
          params: {
            proposedId: appInstanceId
          } as NodeTypes.InstallParams
        };

        nodeB.call(installAppInstanceRequest);
      });

      nodeA.call(appInstanceInstallationProposalRequest);
    });

    await getMultisigCreationTransactionHash(nodeA, [
      nodeA.publicIdentifier,
      nodeB.publicIdentifier
    ]);
  });
});
