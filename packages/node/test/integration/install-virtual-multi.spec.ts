import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { APP_INSTANCE_STATUS } from "../../src/db-schema";
import {
  CreateChannelMessage,
  InstallVirtualMessage,
  NODE_EVENTS,
  ProposeVirtualMessage
} from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedVirtualAppInstanceOnNode,
  getApps,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeInstallVirtualProposalRequest,
  makeInstallVirtualRequest
} from "./utils";

describe("Node method follows spec - proposeInstallVirtual", () => {
  jest.setTimeout(80000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeD: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC, nodeD] = await createNodes(
      4,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });
  describe(
    "Node A makes a proposal through an intermediary Node B and C to install a " +
      "Virtual AppInstance with Node D. All Nodes confirm receipt of proposal",
    () => {
      it("sends proposal with non-null initial state", async done => {
        nodeD.once(
          NODE_EVENTS.CREATE_CHANNEL,
          async (msg: CreateChannelMessage) => {
            nodeD.once(
              NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
              async (msg: ProposeVirtualMessage) => {
                nodeA.once(
                  NODE_EVENTS.INSTALL_VIRTUAL,
                  async (msg: InstallVirtualMessage) => {
                    const virtualAppInstanceNodeA = (await getApps(
                      nodeA,
                      APP_INSTANCE_STATUS.INSTALLED
                    ))[0];

                    const virtualAppInstanceNodeD = (await getApps(
                      nodeD,
                      APP_INSTANCE_STATUS.INSTALLED
                    ))[0];

                    expect(virtualAppInstanceNodeA).toEqual(
                      virtualAppInstanceNodeD
                    );
                    done();
                  }
                );

                const proposedAppInstanceA = (await getProposedAppInstances(
                  nodeA
                ))[0];
                const proposedAppInstanceD = (await getProposedAppInstances(
                  nodeD
                ))[0];

                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceA
                );
                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceD
                );

                expect(proposedAppInstanceD.proposedByIdentifier).toEqual(
                  nodeA.publicIdentifier
                );
                expect(proposedAppInstanceA.id).toEqual(
                  proposedAppInstanceD.id
                );
                expect(msg.data.appInstanceId).toBeDefined();
                const installVirtualReq = makeInstallVirtualRequest(
                  msg.data.appInstanceId!,
                  msg.data.intermediaries
                );
                nodeD.call(installVirtualReq);
              }
            );
            const intermediaries = [
              nodeB.publicIdentifier,
              nodeC.publicIdentifier
            ];
            const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
              nodeA.publicIdentifier,
              nodeD.publicIdentifier,
              intermediaries
            );
            await nodeA.call(installVirtualAppInstanceProposalRequest);
          }
        );

        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
        await getMultisigCreationTransactionHash(nodeB, [
          nodeB.publicIdentifier,
          nodeC.publicIdentifier
        ]);
        await getMultisigCreationTransactionHash(nodeC, [
          nodeC.publicIdentifier,
          nodeD.publicIdentifier
        ]);
      });
    }
  );
});
