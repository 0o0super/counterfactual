import { Node as NodeTypes } from "@counterfactual/types";
import { JsonRpcProvider } from "ethers/providers";

import {
  CreateChannelMessage,
  IMessagingService,
  Node,
  NODE_EVENTS,
  NodeConfig
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  getChannelAddresses,
  getMultisigCreationTransactionHash
} from "./utils";

describe("Node can create multisig, other owners get notified", () => {
  jest.setTimeout(60000);
  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_MULTISIG_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC] = await createNodes(
      3,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  describe("Queued channel creation", () => {
    it("Node A can create multiple back-to-back channels with Node B and Node C", async done => {
      const ownersABPublicIdentifiers = [
        nodeA.publicIdentifier,
        nodeB.publicIdentifier
      ];

      const ownersACPublicIdentifiers = [
        nodeA.publicIdentifier,
        nodeC.publicIdentifier
      ];

      nodeA.on(
        NODE_EVENTS.CREATE_CHANNEL,
        async (data: CreateChannelMessage) => {
          const { initiatingXpub, respondingXpub } = data.data;
          if (
            JSON.stringify([initiatingXpub, respondingXpub]) ===
            JSON.stringify(ownersABPublicIdentifiers)
          ) {
            const openChannelsNodeA = await getChannelAddresses(nodeA);
            const openChannelsNodeB = await getChannelAddresses(nodeB);
            expect(openChannelsNodeA.size).toEqual(1);
            expect(openChannelsNodeB.size).toEqual(1);

            await confirmChannelCreation(
              nodeA,
              nodeB,
              ownersABPublicIdentifiers,
              data.data
            );
          } else {
            const openChannelsNodeA = await getChannelAddresses(nodeA);
            const openChannelsNodeC = await getChannelAddresses(nodeC);

            expect(openChannelsNodeA.size).toEqual(2);
            expect(openChannelsNodeC.size).toEqual(1);

            await confirmChannelCreation(
              nodeA,
              nodeC,
              ownersACPublicIdentifiers,
              data.data
            );

            done();
          }
        }
      );

      await getMultisigCreationTransactionHash(
        nodeA,
        ownersABPublicIdentifiers
      );

      await getMultisigCreationTransactionHash(
        nodeA,
        ownersACPublicIdentifiers
      );

      // expect(txHash1).toBeDefined();
      // expect(txHash2).toBeDefined();
    });
  });
});

async function confirmChannelCreation(
  nodeA: Node,
  nodeB: Node,
  ownersPublicIdentifiers: string[],
  data: NodeTypes.CreateChannelParams
) {
  const openChannelsNodeA = await getChannelAddresses(nodeA);
  const openChannelsNodeB = await getChannelAddresses(nodeB);

  expect(openChannelsNodeA.has(data.multisigAddress!)).toBeTruthy();
  expect(openChannelsNodeB.has(data.multisigAddress!)).toBeTruthy();
  expect([data.initiatingXpub, data.respondingXpub]).toEqual(
    ownersPublicIdentifiers
  );
}
