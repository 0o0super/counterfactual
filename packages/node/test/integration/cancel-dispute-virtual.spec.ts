import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import { Node as NodeTypes } from "@counterfactual/types";
import { Contract } from "ethers";
import { WeiPerEther } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";
import { bigNumberify } from "ethers/utils";
import { v4 as generateUUID } from "uuid";

import {
  CreateChannelMessage,
  IMessagingService,
  InstallVirtualMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeCancelDisputeMessage
} from "../../src";
import { APP_INSTANCE_STATUS } from "../../src/db-schema";
import { xkeyKthAddress } from "../../src/machine";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedVirtualAppInstanceOnNode,
  generateGetStateRequest,
  generateTakeActionRequest,
  getApps,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeDepositRequest,
  makeInstallVirtualRequest,
  makeTTTVirtualAppInstanceProposalReq
} from "./utils";

describe("Node method follows spec - dispute set state", () => {
  jest.setTimeout(60000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodes: Node[] = [];
  let nodeIni: Node;
  let nodeRes: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  const NUM_NODES = 4;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    nodes = await createNodes(
      NUM_NODES,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
    nodeIni = nodes[0];
    nodeRes = nodes[NUM_NODES - 1];
  });

  afterAll(async () => {
    await firebaseServiceFactory.closeServiceConnections();
  });

  describe(
    "Node A and B install an AppInstance, Node A takes action, " +
      "Node B confirms receipt of state update",
    () => {
      const stateEncoding =
        "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)";
      const actionEncoding =
        "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)";

      it("can take action", async done => {
        const initialState = {
          players: [
            xkeyKthAddress(nodeIni.publicIdentifier, 0), // <-- winner
            xkeyKthAddress(nodeRes.publicIdentifier, 0)
          ],
          turnNum: 0,
          winner: 1,
          board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        };

        const validAction = {
          actionType: 0,
          playX: 1,
          playY: 1,
          winClaim: {
            winClaimType: 0,
            idx: 0
          }
        };

        // Create channel!
        const multiSigAddrs: string[] = [];
        for (let i = 0; i < NUM_NODES - 1; i += 1) {
          getMultisigCreationTransactionHash(nodes[i], [
            nodes[i].publicIdentifier,
            nodes[i + 1].publicIdentifier
          ]);
          const res: CreateChannelMessage = await new Promise(r =>
            nodes[i + 1].once(NODE_EVENTS.CREATE_CHANNEL, r)
          );
          multiSigAddrs.push(res.data.multisigAddress!);
        }

        // Deposit!
        for (let i = 0; i < NUM_NODES - 1; i += 1) {
          const depositReq = makeDepositRequest(
            multiSigAddrs[i],
            WeiPerEther.div(2)
          );
          nodes[i].call(depositReq);
          await new Promise(r =>
            nodes[i + 1].once(NODE_EVENTS.DEPOSIT_CONFIRMED, r)
          );
          nodes[i + 1].call(depositReq);
          await new Promise(r =>
            nodes[i].once(NODE_EVENTS.DEPOSIT_CONFIRMED, r)
          );

          const walletBalance = await provider.getBalance(multiSigAddrs[i]);
          expect(walletBalance.toString()).toEqual(WeiPerEther.toString());
        }

        // propose virtual app install and wait for response
        const installVirtualAppInstanceProposalRequest = makeTTTVirtualAppInstanceProposalReq(
          nodeRes.publicIdentifier,
          global["networkContext"].TicTacToe,
          initialState,
          {
            stateEncoding,
            actionEncoding
          },
          nodes.slice(1, NUM_NODES - 1).map(n => n.publicIdentifier),
          WeiPerEther.div(5),
          WeiPerEther.div(5)
        );

        nodeIni.call(installVirtualAppInstanceProposalRequest);
        await new Promise(r =>
          nodeRes.once(NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL, r)
        );

        // check if node A and D actually stor proposed app
        const proposedAppInstanceA = (await getProposedAppInstances(
          nodeIni
        ))[0];
        const proposedAppInstanceE = (await getProposedAppInstances(
          nodeRes
        ))[0];

        confirmProposedVirtualAppInstanceOnNode(
          installVirtualAppInstanceProposalRequest.params,
          proposedAppInstanceA
        );
        confirmProposedVirtualAppInstanceOnNode(
          installVirtualAppInstanceProposalRequest.params,
          proposedAppInstanceE
        );
        expect(proposedAppInstanceE.proposedByIdentifier).toEqual(
          nodeIni.publicIdentifier
        );
        expect(proposedAppInstanceA.id).toEqual(proposedAppInstanceE.id);

        // Node D initiate install
        const installVirtualReq = makeInstallVirtualRequest(
          proposedAppInstanceA.id,
          proposedAppInstanceA.intermediaries!
        );
        nodeRes.call(installVirtualReq);
        const installVirtualRes: InstallVirtualMessage = await new Promise(r =>
          nodeIni.once(NODE_EVENTS.INSTALL_VIRTUAL, r)
        );

        // check if app actually got installed
        const virtualAppInstanceNodeA = (await getApps(
          nodeIni,
          APP_INSTANCE_STATUS.INSTALLED
        ))[0];
        const virtualAppInstanceNodeE = (await getApps(
          nodeRes,
          APP_INSTANCE_STATUS.INSTALLED
        ))[0];
        expect(virtualAppInstanceNodeA).toEqual(virtualAppInstanceNodeE);

        const installedId = installVirtualRes.data.installedId!;
        // update!!
        const takeActionReq = generateTakeActionRequest(
          installedId,
          validAction
        );
        nodeIni.call(takeActionReq);
        await new Promise(r => nodeRes.once(NODE_EVENTS.UPDATE_STATE, r));

        const getStateReq = generateGetStateRequest(installedId);

        const response = await nodeRes.callStatic(getStateReq);

        const updatedState = (response.result as NodeTypes.GetStateResult)
          .state;

        expect(updatedState.board[1][1]).toEqual(bigNumberify(1));
        expect(updatedState.turnNum).toEqual(bigNumberify(1));

        // dispute!
        await nodeIni.call({
          params: {
            appInstanceId: installedId
          },
          requestId: generateUUID(),
          type: NodeTypes.MethodName.DISPUTE_STATE
        });
        await provider.send("evm_mine", []);
        await provider.send("evm_mine", []);

        const appRegistry = new Contract(
          global["networkContext"].AppRegistry,
          AppRegistryApp.abi,
          provider
        );
        const data1: any = await appRegistry.functions.getAppChallenge(
          installedId
        );
        expect(data1.status).toEqual(1);

        // now nodeB propose cancel dispute and A accept it
        nodeRes.call({
          params: {
            appInstanceId: installedId
          },
          requestId: generateUUID(),
          type: NodeTypes.MethodName.PROPOSE_CANCEL_DISPUTE
        });

        const req: ProposeCancelDisputeMessage = await new Promise(r =>
          nodeIni.on(NODE_EVENTS.PROPOSE_CANCEL_DISPUTE, r)
        );

        // nodeA initiate cancel dispute
        nodeIni.call({
          params: {
            appInstanceId: req.data.appInstanceId,
            proposingState: req.data.proposingState
          },
          requestId: generateUUID(),
          type: NodeTypes.MethodName.DISPUTE_CANCEL
        });
        await new Promise(r => nodeRes.on(NODE_EVENTS.CANCEL_DISPUTE, r));
        const data2: any = await appRegistry.functions.getAppChallenge(
          req.data.appInstanceId
        );
        expect(data2.status).toEqual(0);

        done();
      });
    }
  );
});
