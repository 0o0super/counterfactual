import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import {
  Address,
  AppABIEncodings,
  AssetType,
  Node as NodeTypes,
  SolidityABIEncoderV2Struct
} from "@counterfactual/types";
import { Contract } from "ethers";
import { One, WeiPerEther } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";
import { bigNumberify } from "ethers/utils";
import { v4 as generateUUID } from "uuid";

import {
  CreateChannelMessage,
  IMessagingService,
  InstallMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeCancelDisputeMessage,
  ProposeMessage,
  UpdateStateMessage
} from "../../src";
import { xkeyKthAddress } from "../../src/machine";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  generateGetStateRequest,
  generateTakeActionRequest,
  getMultisigCreationTransactionHash,
  makeDepositRequest,
  makeInstallRequest
} from "./utils";

describe("Node method follows spec - dispute set state", () => {
  jest.setTimeout(60000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(async () => {
    await firebaseServiceFactory.closeServiceConnections();
  });

  describe(
    "Node A and B install an AppInstance, Node A takes action, " +
      "Node B confirms receipt of state update",
    () => {
      const stateEncoding =
        "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)";
      const actionEncoding =
        "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)";

      it("can take action", async done => {
        const initialState = {
          players: [
            xkeyKthAddress(nodeA.publicIdentifier, 0), // <-- winner
            xkeyKthAddress(nodeB.publicIdentifier, 0)
          ],
          turnNum: 0,
          winner: 1,
          board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        };

        const validAction = {
          actionType: 0,
          playX: 1,
          playY: 1,
          winClaim: {
            winClaimType: 0,
            idx: 0
          }
        };

        const tttAppInstanceProposalReq = makeTTTAppInstanceProposalReq(
          nodeB.publicIdentifier,
          global["networkContext"].TicTacToe,
          initialState,
          {
            stateEncoding,
            actionEncoding
          }
        );

        nodeA.on(
          NODE_EVENTS.CREATE_CHANNEL,
          async (data: CreateChannelMessage) => {
            // deposit!!
            const depositReq = makeDepositRequest(
              data.data.multisigAddress!,
              WeiPerEther
            );

            nodeB.on(
              NODE_EVENTS.UPDATE_STATE,
              async (msg: UpdateStateMessage) => {
                const getStateReq = generateGetStateRequest(
                  msg.data.appInstanceId
                );

                const response = await nodeB.callStatic(getStateReq);

                const updatedState = (response.result as NodeTypes.GetStateResult)
                  .state;

                expect(updatedState.board[1][1]).toEqual(bigNumberify(1));
                expect(updatedState.turnNum).toEqual(bigNumberify(1));

                // let's dispute here
                await nodeA.call({
                  params: {
                    appInstanceId: msg.data.appInstanceId
                  },
                  requestId: generateUUID(),
                  type: NodeTypes.MethodName.DISPUTE_STATE
                });

                // now nodeB propose cancel dispute and A accept it
                nodeB.call({
                  params: {
                    appInstanceId: msg.data.appInstanceId
                  },
                  requestId: generateUUID(),
                  type: NodeTypes.MethodName.PROPOSE_CANCEL_DISPUTE
                });

                const req: ProposeCancelDisputeMessage = await new Promise(r =>
                  nodeA.on(NODE_EVENTS.PROPOSE_CANCEL_DISPUTE, r)
                );

                // nodeA initiate cancel dispute
                nodeA.call({
                  params: {
                    appInstanceId: req.data.appInstanceId,
                    proposingState: req.data.proposingState
                  },
                  requestId: generateUUID(),
                  type: NodeTypes.MethodName.DISPUTE_CANCEL
                });
                await new Promise(r => nodeB.on(NODE_EVENTS.CANCEL_DISPUTE, r));
                const appRegistry = new Contract(
                  global["networkContext"].AppRegistry,
                  AppRegistryApp.abi,
                  provider
                );
                const data: any = await appRegistry.functions.getAppChallenge(
                  req.data.appInstanceId
                );
                expect(data.status).toEqual(0);
                done();
              }
            );

            nodeA.on(NODE_EVENTS.INSTALL, async (msg: InstallMessage) => {
              const takeActionReq = generateTakeActionRequest(
                msg.data.installedId!,
                validAction
              );

              await nodeA.call(takeActionReq);
            });

            nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, (msg: ProposeMessage) => {
              const installReq = makeInstallRequest(msg.data.appInstanceId!);
              nodeB.call(installReq);
            });

            nodeB.once(NODE_EVENTS.DEPOSIT_CONFIRMED, async () => {
              nodeA.once(NODE_EVENTS.DEPOSIT_CONFIRMED, async () => {
                const multiSigBalance = await provider.getBalance(
                  data.data.multisigAddress!
                );
                expect(multiSigBalance).toEqual(WeiPerEther.mul(2));
                nodeA.call(tttAppInstanceProposalReq);
              });
              // deposit!!
              await nodeB.call(depositReq);
            });
            await nodeA.call(depositReq);
          }
        );
        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });
    }
  );
});

function makeTTTAppInstanceProposalReq(
  proposedToIdentifier: string,
  appId: Address,
  initialState: SolidityABIEncoderV2Struct,
  abiEncodings: AppABIEncodings
): NodeTypes.MethodRequest {
  return {
    params: {
      proposedToIdentifier,
      appId,
      initialState,
      abiEncodings,
      asset: {
        assetType: AssetType.ETH
      },
      myDeposit: WeiPerEther.div(2),
      peerDeposit: WeiPerEther.div(2),
      timeout: One.mul(5)
    } as NodeTypes.ProposeInstallParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.PROPOSE_INSTALL
  } as NodeTypes.MethodRequest;
}
