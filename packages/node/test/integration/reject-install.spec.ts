import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { NODE_EVENTS, ProposeMessage } from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedAppInstanceOnNode,
  getInstalledAppInstances,
  getMultisigCreationTransactionHash,
  getProposedAppInstanceInfo,
  getProposedAppInstances,
  makeInstallProposalRequest,
  makeRejectInstallRequest
} from "./utils";

describe("Node method follows spec - rejectInstall", () => {
  jest.setTimeout(10000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  describe(
    "Node A gets app install proposal, sends to node B, B approves it, installs it," +
      "sends acks back to A, A installs it, both nodes have the same app instance",
    () => {
      it("sends proposal with non-null initial state", async done => {
        nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
          expect(await getInstalledAppInstances(nodeA)).toEqual([]);
          expect(await getInstalledAppInstances(nodeB)).toEqual([]);

          let appInstanceId;

          // second, an app instance must be proposed to be installed into that channel
          const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
            nodeA.publicIdentifier,
            nodeB.publicIdentifier
          );

          nodeA.on(NODE_EVENTS.REJECT_INSTALL, async () => {
            expect((await getProposedAppInstances(nodeA)).length).toEqual(0);
            done();
          });

          // node B then decides to reject the proposal
          nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, async (msg: ProposeMessage) => {
            appInstanceId = msg.data.appInstanceId;
            expect(appInstanceId).toBeDefined();
            confirmProposedAppInstanceOnNode(
              appInstanceInstallationProposalRequest.params,
              await getProposedAppInstanceInfo(nodeA, appInstanceId)
            );

            const rejectReq = makeRejectInstallRequest(appInstanceId);

            // Node A should have a proposal in place before Node B rejects it
            expect((await getProposedAppInstances(nodeA)).length).toEqual(1);

            await nodeB.call(rejectReq);

            expect((await getProposedAppInstances(nodeB)).length).toEqual(0);
          });

          await nodeA.call(appInstanceInstallationProposalRequest);
        });
        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });
    }
  );
});
