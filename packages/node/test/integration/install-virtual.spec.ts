import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { APP_INSTANCE_STATUS } from "../../src/db-schema";
import { NODE_EVENTS, ProposeVirtualMessage } from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedVirtualAppInstanceOnNode,
  getApps,
  getMultisigCreationTransactionHash,
  getProposedAppInstances,
  makeInstallVirtualProposalRequest,
  makeInstallVirtualRequest
} from "./utils";

describe("Node method follows spec - proposeInstallVirtual", () => {
  jest.setTimeout(120000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC] = await createNodes(
      3,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });
  describe(
    "Node A makes a proposal through an intermediary Node B to install a " +
      "Virtual AppInstance with Node C. All Nodes confirm receipt of proposal",
    () => {
      it("sends proposal with non-null initial state", async done => {
        nodeA.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
          nodeC.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
            const intermediaries = [nodeB.publicIdentifier];
            const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
              nodeA.publicIdentifier,
              nodeC.publicIdentifier,
              intermediaries
            );

            nodeA.once(NODE_EVENTS.INSTALL_VIRTUAL, async () => {
              const virtualAppInstanceNodeA = (await getApps(
                nodeA,
                APP_INSTANCE_STATUS.INSTALLED
              ))[0];

              const virtualAppInstanceNodeC = (await getApps(
                nodeC,
                APP_INSTANCE_STATUS.INSTALLED
              ))[0];

              expect(virtualAppInstanceNodeA).toEqual(virtualAppInstanceNodeC);
              done();
            });

            nodeC.once(
              NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
              async (msg: ProposeVirtualMessage) => {
                const proposedAppInstanceA = (await getProposedAppInstances(
                  nodeA
                ))[0];
                const proposedAppInstanceC = (await getProposedAppInstances(
                  nodeC
                ))[0];

                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceA
                );
                confirmProposedVirtualAppInstanceOnNode(
                  installVirtualAppInstanceProposalRequest.params,
                  proposedAppInstanceC
                );

                expect(proposedAppInstanceC.proposedByIdentifier).toEqual(
                  nodeA.publicIdentifier
                );
                expect(proposedAppInstanceA.id).toEqual(
                  proposedAppInstanceC.id
                );

                expect(msg.data.appInstanceId).toBeDefined();
                const installVirtualReq = makeInstallVirtualRequest(
                  msg.data.appInstanceId!,
                  msg.data.intermediaries
                );
                nodeC.call(installVirtualReq);
              }
            );

            await nodeA.call(installVirtualAppInstanceProposalRequest);
          });
          await getMultisigCreationTransactionHash(nodeB, [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier
          ]);
        });
        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });
    }
  );

  describe(
    "Node A makes a second proposal through an intermediary Node B to install a " +
      "Virtual AppInstance with Node C. All Nodes confirm receipt of proposal",
    () => {
      it("sends proposal with non-null initial state", async done => {
        // fix meeeee
        // await expect(
        //   getMultisigCreationTransactionHash(nodeA, [
        //     nodeA.publicIdentifier,
        //     nodeB.publicIdentifier
        //   ])
        // ).rejects.toBeDefined();

        // await expect(
        //   getMultisigCreationTransactionHash(nodeB, [
        //     nodeB.publicIdentifier,
        //     nodeC.publicIdentifier
        //   ])
        // ).rejects.toBeDefined(); // it's a loose pass

        const intermediaries = [nodeB.publicIdentifier];
        const installVirtualAppInstanceProposalRequest = makeInstallVirtualProposalRequest(
          nodeA.publicIdentifier,
          nodeC.publicIdentifier,
          intermediaries
        );

        nodeA.once(NODE_EVENTS.INSTALL_VIRTUAL, async () => {
          const virtualAppInstanceNodeA = (await getApps(
            nodeA,
            APP_INSTANCE_STATUS.INSTALLED
          ))[0];

          const virtualAppInstanceNodeC = (await getApps(
            nodeC,
            APP_INSTANCE_STATUS.INSTALLED
          ))[0];

          expect(virtualAppInstanceNodeA).toEqual(virtualAppInstanceNodeC);
          done();
        });

        nodeC.once(
          NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
          async (msg: ProposeVirtualMessage) => {
            const proposedAppInstanceA = (await getProposedAppInstances(
              nodeA
            ))[0];
            const proposedAppInstanceC = (await getProposedAppInstances(
              nodeC
            ))[0];

            confirmProposedVirtualAppInstanceOnNode(
              installVirtualAppInstanceProposalRequest.params,
              proposedAppInstanceA
            );
            confirmProposedVirtualAppInstanceOnNode(
              installVirtualAppInstanceProposalRequest.params,
              proposedAppInstanceC
            );

            expect(proposedAppInstanceC.proposedByIdentifier).toEqual(
              nodeA.publicIdentifier
            );
            expect(proposedAppInstanceA.id).toEqual(proposedAppInstanceC.id);

            expect(msg.data.appInstanceId).toBeDefined();

            const installVirtualReq = makeInstallVirtualRequest(
              msg.data.appInstanceId!,
              msg.data.intermediaries
            );
            nodeC.call(installVirtualReq);
          }
        );
        await nodeA.call(installVirtualAppInstanceProposalRequest);
      });
    }
  );
});
