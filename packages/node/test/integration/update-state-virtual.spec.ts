import { Node as NodeTypes } from "@counterfactual/types";
import { AddressZero, Zero } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";

import {
  IMessagingService,
  InstallVirtualMessage,
  Node,
  NODE_EVENTS,
  NodeConfig,
  ProposeVirtualMessage,
  UpdateStateMessage
} from "../../src";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  generateGetStateRequest,
  generateUpdateStateRequest,
  getMultisigCreationTransactionHash,
  makeInstallVirtualRequest,
  makeTTTVirtualAppInstanceProposalReq
} from "./utils";

describe("Node method follows spec - updateState virtual", () => {
  jest.setTimeout(50000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeC: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB, nodeC] = await createNodes(
      3,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  describe(
    "Node A and C install an AppInstance through Node B, Node A takes action, " +
      "Node C confirms receipt of state update",
    () => {
      const stateEncoding =
        "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)";
      const actionEncoding =
        "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)";

      const initialState = {
        players: [AddressZero, AddressZero],
        turnNum: 0,
        winner: 0,
        board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      };

      it("can take action", async done => {
        nodeA.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
          nodeC.once(NODE_EVENTS.CREATE_CHANNEL, async () => {
            const tttAppInstanceProposalReq = makeTTTVirtualAppInstanceProposalReq(
              nodeC.publicIdentifier,
              global["networkContext"].TicTacToe,
              initialState,
              {
                stateEncoding,
                actionEncoding
              },
              [nodeB.publicIdentifier],
              Zero,
              Zero
            );

            nodeC.on(
              NODE_EVENTS.UPDATE_STATE,
              async (msg: UpdateStateMessage) => {
                const getStateReq = generateGetStateRequest(
                  msg.data.appInstanceId
                );
                const response = await nodeC.callStatic(getStateReq);
                const updatedState = (response.result as NodeTypes.GetStateResult)
                  .state;

                expect(updatedState.board[0][0]).toEqual(1);
                expect(updatedState.turnNum).toEqual(1);
                done();
              }
            );

            nodeA.on(
              NODE_EVENTS.INSTALL_VIRTUAL,
              async (msg: InstallVirtualMessage) => {
                // update!!
                const updateStateReq = generateUpdateStateRequest(
                  msg.data.installedId!,
                  {
                    players: [AddressZero, AddressZero],
                    turnNum: 1,
                    winner: 0,
                    board: [[1, 0, 0], [0, 0, 0], [0, 0, 0]]
                  }
                );

                await nodeA.call(updateStateReq);
              }
            );

            nodeC.on(
              NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
              (msg: ProposeVirtualMessage) => {
                const installReq = makeInstallVirtualRequest(
                  msg.data.appInstanceId!,
                  msg.data.intermediaries
                );
                nodeC.call(installReq);
              }
            );

            nodeA.call(tttAppInstanceProposalReq);
          });
          await getMultisigCreationTransactionHash(nodeB, [
            nodeB.publicIdentifier,
            nodeC.publicIdentifier
          ]);
        });
        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });
    }
  );
});
