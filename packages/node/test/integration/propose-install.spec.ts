import { JsonRpcProvider } from "ethers/providers";

import { IMessagingService, Node, NodeConfig } from "../../src";
import { ERRORS } from "../../src/methods/errors";
import { NODE_EVENTS, ProposeMessage } from "../../src/types";
import { LocalFirebaseServiceFactory } from "../services/firebase-server";

import { createNodes } from "./node-creator";
import {
  confirmProposedAppInstanceOnNode,
  getInstalledAppInstanceInfo,
  getInstalledAppInstances,
  getMultisigCreationTransactionHash,
  getProposedAppInstanceInfo,
  makeInstallProposalRequest,
  makeInstallRequest
} from "./utils";

describe("Node method follows spec - proposeInstall", () => {
  jest.setTimeout(15000);

  let firebaseServiceFactory: LocalFirebaseServiceFactory;
  let messagingService: IMessagingService;
  let nodeA: Node;
  let nodeB: Node;
  let nodeConfig: NodeConfig;
  let provider: JsonRpcProvider;

  beforeAll(async () => {
    firebaseServiceFactory = new LocalFirebaseServiceFactory(
      process.env.FIREBASE_DEV_SERVER_HOST!,
      process.env.FIREBASE_DEV_SERVER_PORT!
    );
    messagingService = firebaseServiceFactory.createMessagingService(
      process.env.FIREBASE_MESSAGING_SERVER_KEY!
    );
    nodeConfig = {
      STORE_KEY_PREFIX: process.env.FIREBASE_STORE_PREFIX_KEY!
    };

    provider = new JsonRpcProvider(global["ganacheURL"]);

    [nodeA, nodeB] = await createNodes(
      2,
      messagingService,
      nodeConfig,
      firebaseServiceFactory,
      provider
    );
  });

  afterAll(() => {
    firebaseServiceFactory.closeServiceConnections();
  });

  describe(
    "Node A gets app install proposal, sends to node B, B approves it, installs it," +
      "sends acks back to A, A installs it, both nodes have the same app instance",
    () => {
      it("sends proposal with non-null initial state", async done => {
        let appInstanceId: any;
        nodeA.on(NODE_EVENTS.CREATE_CHANNEL, async () => {
          expect(await getInstalledAppInstances(nodeA)).toEqual([]);
          expect(await getInstalledAppInstances(nodeB)).toEqual([]);

          // second, an app instance must be proposed to be installed into that channel
          const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
            nodeA.publicIdentifier,
            nodeB.publicIdentifier
          );

          // node B then decides to approve the proposal
          nodeB.on(NODE_EVENTS.PROPOSE_INSTALL, async (msg: ProposeMessage) => {
            expect(msg.data.appInstanceId).toBeDefined();
            appInstanceId = msg.data.appInstanceId!;
            confirmProposedAppInstanceOnNode(
              appInstanceInstallationProposalRequest.params,
              await getProposedAppInstanceInfo(nodeA, appInstanceId)
            );

            // some approval logic happens in this callback, we proceed
            // to approve the proposal, and install the app instance
            const installRequest = makeInstallRequest(appInstanceId);
            nodeB.call(installRequest);
          });

          nodeA.on(NODE_EVENTS.INSTALL, async () => {
            const appInstanceNodeA = await getInstalledAppInstanceInfo(
              nodeA,
              appInstanceId
            );
            const appInstanceNodeB = await getInstalledAppInstanceInfo(
              nodeB,
              appInstanceId
            );
            expect(appInstanceNodeA).toEqual(appInstanceNodeB);
            done();
          });
          await nodeA.call(appInstanceInstallationProposalRequest);
        });
        await getMultisigCreationTransactionHash(nodeA, [
          nodeA.publicIdentifier,
          nodeB.publicIdentifier
        ]);
      });

      it("sends proposal with null initial state", async () => {
        const appInstanceInstallationProposalRequest = makeInstallProposalRequest(
          nodeA.publicIdentifier,
          nodeB.publicIdentifier,
          true
        );

        expect(
          nodeA.call(appInstanceInstallationProposalRequest)
        ).rejects.toEqual(ERRORS.NULL_INITIAL_STATE_FOR_PROPOSAL);
      });
    }
  );
});
