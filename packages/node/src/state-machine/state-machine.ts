import { TypeState } from "typestate";

// let's use appinstance for example

enum AppState {
  Initial,
  Propose,
  Install,
  Reject,
  Uninstall,
  Update,
  TakeAction,
  End
}

export default class NodeStateMachine {
  private fsm: TypeState.FiniteStateMachine<AppState>;

  constructor(state: AppState = AppState.Initial) {
    this.fsm = new TypeState.FiniteStateMachine<AppState>(state);
    this.defineTransition();
  }

  private defineTransition() {
    this.fsm.from(AppState.Initial).to(AppState.Propose);
    this.fsm.from(AppState.Propose).to(AppState.Reject);
    this.fsm.from(AppState.Reject).to(AppState.End);
    this.fsm.from(AppState.Propose).to(AppState.Install);

    this.fsm.from(AppState.Install).to(AppState.Update);
    this.fsm.from(AppState.Install).to(AppState.TakeAction);

    this.fsm.from(AppState.Update).to(AppState.Update);
    this.fsm.from(AppState.Update).to(AppState.TakeAction);
    this.fsm.from(AppState.TakeAction).to(AppState.TakeAction);
    this.fsm.from(AppState.TakeAction).to(AppState.Update);

    this.fsm.from(AppState.Update).to(AppState.Uninstall);
    this.fsm.from(AppState.TakeAction).to(AppState.Uninstall);
    this.fsm.from(AppState.Install).to(AppState.Uninstall);

    this.fsm.from(AppState.Uninstall).to(AppState.End);
  }

  public checkState() {}
}
