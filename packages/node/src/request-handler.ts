import { NetworkContext, Node } from "@counterfactual/types";
import { Signer } from "ethers";
import { BaseProvider } from "ethers/providers";
import EventEmitter from "eventemitter3";

import {
  methodToImplementation,
  staticMethodToImplementation
} from "./api-router";
import { InstructionExecutor } from "./machine";
import { NodeController } from "./methods/controller";
import { IMessagingService, IStoreService } from "./services";
import { Store } from "./store";
import { NodeExecutionState } from "./types";

/**
 * This class registers handlers for requests to get or set some information
 * about app instances and channels for this Node and any relevant peer Nodes.
 */

// type InteractiveMethodType = (
//   requestId: string,
//   requestHandler: RequestHandler,
//   params: Node.MethodParams,
//   step: NodeExecutionState,
//   protocolMessage?: ProtocolMessage
// ) => Promise<Node.MethodResult>;

type StaticMethodType = (
  // requestId: string, // do we need requestId for static?
  requestHandler: RequestHandler,
  params: Node.MethodParams
) => Promise<Node.MethodResult>;

// type InteractiveMethodParam = {
//   method: Node.MethodName;
//   req: Node.MethodRequest;
//   step: NodeExecutionState;
//   // status: ExecutionState;
// };

// type OperationStatus = {
//   data: InteractiveMethodParam;
//   multiSigAddrs: string[];
//   firstSeen: number;
//   lastSeen: number;
//   retryCount: number;
// };

export class RequestHandler {
  private interactiveMethods = new Map<string, NodeController>();
  private staticMethods = new Map<string, StaticMethodType>();

  store: Store;

  constructor(
    readonly publicIdentifier: string,
    readonly outgoing: EventEmitter,
    readonly error: EventEmitter,
    readonly storeService: IStoreService,
    readonly messagingService: IMessagingService,
    readonly instructionExecutor: InstructionExecutor,
    readonly networkContext: NetworkContext,
    readonly provider: BaseProvider,
    readonly wallet: Signer,
    storeKeyPrefix: string,
    readonly blocksNeededForConfirmation: number
  ) {
    this.store = new Store(storeService, storeKeyPrefix);
    this.mapPublicApiMethods();
  }

  /**
   * In some use cases, waiting for the response of a method call is easier
   * and cleaner than wrangling through callback hell.
   * @param method
   * @param req
   */
  public async callInteractiveMethod(
    req: Node.MethodRequest,
    step: NodeExecutionState
  ): Promise<Node.MethodResponse> {
    if (!this.interactiveMethods.has(req.type)) {
      throw Error("Interactive Method Not Found");
    }

    const controller = this.interactiveMethods.get(req.type)!;

    return {
      type: req.type,
      requestId: req.requestId,
      result: await controller.execute(
        req.requestId,
        this,
        req.params,
        step,
        req.protocolMessage
      )
    };

    // const executionId = generateUUID();
    // this.pendingOperations.set(executionId, {
    //   data: { method, req, step },
    //   multiSigAddrs: [], // should we get here?
    //   firstSeen: Date.now(),
    //   lastSeen: Date.now(),
    //   retryCount: 1
    // });
    // this.pickPendingAndRun();

    // // wait until event fire
    // // should handle error
    // return new Promise((res, rej) => {
    //   this.internalEmitter.once(req.requestId, (err?: Error) =>
    //     err ? rej(err) : res()
    //   );
    // });
  }

  public async callStaticMethod(
    method: Node.MethodName,
    req: Node.MethodRequest
  ): Promise<Node.MethodResponse> {
    if (!this.staticMethods.has(method)) {
      throw Error("Static Method Not Found");
    }
    return {
      type: req.type,
      requestId: req.requestId,
      result: await this.staticMethods.get(method)!(this, req.params)
    };
  }

  /**
   * This registers all of the methods the Node is expected to have
   * as described at https://github.com/counterfactual/monorepo/blob/master/packages/cf.js/API_REFERENCE.md#public-methods
   */
  private mapPublicApiMethods() {
    for (const methodName in methodToImplementation) {
      this.interactiveMethods.set(
        methodName,
        methodToImplementation[methodName]
      );
    }
    for (const methodName in staticMethodToImplementation) {
      this.staticMethods.set(
        methodName,
        staticMethodToImplementation[methodName]
      );
    }
  }

  // private pickPendingAndRun() {
  //   this.dispatcherQueue.add(async () => {
  //     // sort by retry times
  //     const runningOpSet = new Set<string>([
  //       ...this.runningOperations.values()
  //     ]);
  //     const pendingOperationsCopy = new Set<string>(
  //       [...this.pendingOperations.entries()]
  //         // don't pick op that is already running
  //         .filter(([id]) => !runningOpSet.has(id))
  //         .sort((a, b) => a[1].retryCount - b[1].retryCount)
  //         // .sort(
  //         //   (a, b) =>
  //         //     b[1].firstSeen / (b[1].retryCount + 1) -
  //         //     a[1].firstSeen / (a[1].retryCount + 1)
  //         // )
  //         .map(e => e[0])
  //     );

  //     for (const executionId of pendingOperationsCopy) {
  //       const opStat = this.pendingOperations.get(executionId)!;
  //       const op = opStat.data;

  //       const controller: NodeController = this.interactiveMethods.get(
  //         op.method
  //       )!;

  //       let state: AppInstanceState | undefined;
  //       try {
  //         state = await controller.getTargetState(
  //           op.req.requestId,
  //           this,
  //           op.req.params
  //         );
  //       } catch (error) {
  //         // this should not happen but it did...
  //         /*
  //         REASON: install request arrive earlier than propose,
  //         and propose only get single operation, thus will not
  //         leave trait in "runningRequest" if it arrive later
  //         */
  //         opStat.retryCount += 1;
  //         opStat.lastSeen = Date.now();

  //         this.pendingOperations.delete(executionId);
  //         if (
  //           opStat.retryCount > 5 // ||
  //           // (error as string).includes(
  //           //   "No proposed AppInstance exists for the given appInstanceId"
  //           // ) ||
  //           // error === ERRORS.NO_APP_INSTANCE_FOR_GIVEN_ID
  //         ) {
  //           console.log(`Operation ${op.method}@${executionId} error, abort.`);
  //           this.internalEmitter.emit(op.req.requestId, error);
  //           return;
  //         }
  //         // we add it back after some time penalty!
  //         setTimeout(() => {
  //           this.dispatcherQueue.add(async () => {
  //             this.pendingOperations.set(executionId, opStat);
  //             this.pickPendingAndRun();
  //           });
  //         }, 100 * Math.pow(opStat.retryCount, 2));
  //         return;
  //       }

  //       if (opStat.multiSigAddrs.length === 0) {
  //         opStat.multiSigAddrs = [...state.multiSigAddresses];
  //       }

  //       let canPick = true;
  //       for (const addr of state.multiSigAddresses) {
  //         if (this.runningOperations.has(addr)) {
  //           canPick = false;
  //           break;
  //         }
  //       }
  //       // const ptcMsg: ProtocolMessage | undefined = op.req.protocolMessage;
  //       // if (
  //       //   ptcMsg &&
  //       //   (ptcMsg.messageType !== state.protocolState ||
  //       //     ptcMsg.protocol !== state.protocol)
  //       // ) {
  //       //   // prevent invalid state transition beforehand
  //       //   canPick = false;
  //       // }

  //       if (canPick) {
  //         for (const addr of state.multiSigAddresses) {
  //           this.runningOperations.set(addr, executionId);
  //         }

  //         (async () => {
  //           try {
  //             // console.log(`Op ${executionId}@${op.req.requestId.slice(0, 5)}`);
  //             await controller.execute(
  //               state,
  //               this,
  //               op.req.params,
  //               op.step,
  //               op.req.protocolMessage
  //             );

  //             // if no error, delete it from pending Q
  //             // this process need to be seqential
  //             this.dispatcherQueue.add(async () => {
  //               this.pendingOperations.delete(executionId);
  //               const stateFromStore = await this.store.getAppInstanceProtocolStatus(
  //                 op.req.requestId
  //               );
  //               if (!stateFromStore) {
  //                 // this request done, emit event to caller
  //                 this.internalEmitter.emit(op.req.requestId);
  //               }
  //               for (const addr of state!.multiSigAddresses) {
  //                 this.runningOperations.delete(addr);
  //                 // if (!stateFromStore) {
  //                 //   this.runningRequest.delete(addr);
  //                 // }
  //               }
  //               this.pickPendingAndRun();
  //             });
  //           } catch (error) {
  //             // success or not, we need to pick pending op
  //             this.dispatcherQueue.add(async () => {
  //               for (const addr of state!.multiSigAddresses) {
  //                 this.runningOperations.delete(addr);
  //               }

  //               // add small number to retry count if it's incompatible state
  //               if (error.message !== ERRORS.INCOMPATABLE_STATE) {
  //                 console.error(`WHEEEE ${error}`);
  //                 opStat.retryCount += 1;
  //               } else {
  //                 opStat.retryCount += 0.1;
  //               }
  //               opStat.lastSeen = Date.now();

  //               this.pendingOperations.delete(executionId);
  //               // console.log(
  //               //   `${op.method}@${op.req.requestId}@step${op.step}@pStep${
  //               //     op.req.protocolMessage
  //               //       ? (op.req.protocolMessage as ProtocolMessage).messageType
  //               //       : "none"
  //               //   }*${opStat.retryCount} Error: ${error}`
  //               // );
  //               if (opStat.retryCount > 5) {
  //                 console.log(
  //                   `Operation ${op.method}@${executionId} error, abort.`
  //                 );
  //                 this.internalEmitter.emit(op.req.requestId, error);
  //                 return;
  //               }
  //               // we add it back after some time penalty!
  //               setTimeout(() => {
  //                 this.dispatcherQueue.add(async () => {
  //                   this.pendingOperations.set(executionId, opStat);
  //                   this.pickPendingAndRun();
  //                 });
  //               }, 100 * Math.pow(opStat.retryCount, 2));
  //             });
  //           }
  //         })();
  //       }
  //     }
  //   });
  // }

  public async getSigner(): Promise<Signer> {
    // try {
    //   const signer = await (this.provider as JsonRpcProvider).getSigner();
    //   await signer.getAddress();
    //   return signer;
    // } catch (e) {
    //   if (e.code === "UNSUPPORTED_OPERATION") {
    //     return this.wallet;
    //   }
    //   throw e;
    // }
    return this.wallet;
  }

  public async getSignerAddress(): Promise<string> {
    const signer = await this.getSigner();
    return await signer.getAddress();
  }
}
