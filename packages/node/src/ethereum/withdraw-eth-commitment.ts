import { BigNumber, bigNumberify } from "ethers/utils";

import { MultisigCommitment } from "./multisig-commitment";
import { MultisigOperation, MultisigTransaction } from "./types";

export class WithdrawETHCommitment extends MultisigCommitment {
  public constructor(
    public readonly multisigAddress: string,
    public readonly multisigOwners: string[],
    public readonly to: string,
    public readonly value: BigNumber | number
  ) {
    super(multisigAddress, multisigOwners);
  }

  public toJsonString() {
    return JSON.stringify({
      multisigAddress: this.multisigAddress,
      multisigOwners: this.multisigOwners,
      to: this.to,
      value: this.value
    });
  }

  public static fromJsonString(str: string) {
    const fromJsonInstance = JSON.parse(str);
    return new WithdrawETHCommitment(
      fromJsonInstance.multisigAddress,
      fromJsonInstance.multisigOwners,
      fromJsonInstance.to,
      fromJsonInstance.value
    );
  }

  public getTransactionDetails(): MultisigTransaction {
    return {
      to: this.to,
      value: bigNumberify(this.value),
      data: "0x",
      operation: MultisigOperation.Call
    };
  }
}
