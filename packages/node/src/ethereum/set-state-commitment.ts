import { utils } from "@counterfactual/cf.js";
import AppRegistry from "@counterfactual/contracts/build/AppRegistry.json";
import { AppIdentity, NetworkContext } from "@counterfactual/types";
import { Interface, keccak256, Signature, solidityPack } from "ethers/utils";

import { EthereumCommitment, Transaction } from "./types";
import { appIdentityToHash } from "./utils/app-identity";
const {
  signaturesToBytes,
  sortSignaturesBySignerAddress,
  signaturesToBytesSortedBySignerAddress
} = utils;

const iface = new Interface(AppRegistry.abi);

// hardcoded assumption: all installed virtual apps can go through this many update operations
const NONCE_EXPIRY = 65536;

export class SetStateCommitment extends EthereumCommitment {
  constructor(
    public readonly networkContext: NetworkContext,
    public readonly appIdentity: AppIdentity,
    public readonly timeout: number,
    public readonly hashedAppState?: string,
    public readonly appLocalNonce?: number
  ) {
    super();
  }

  public toJsonString() {
    return JSON.stringify({
      appIdentity: this.appIdentity,
      hashedAppState: this.hashedAppState,
      appLocalNonce: this.appLocalNonce,
      timeout: this.timeout
    });
  }

  public static fromJsonString(str: string, networkContext: NetworkContext) {
    const fromStringInstance = JSON.parse(str);
    return new SetStateCommitment(
      networkContext,
      fromStringInstance.appIdentity,
      fromStringInstance.timeout,
      fromStringInstance.hashedAppState,
      fromStringInstance.appLocalNonce
    );
  }

  public hashToSign(signerIsIntermediary?: boolean): string {
    if (signerIsIntermediary) {
      /// keep in sync with `digest2` definition
      return keccak256(
        solidityPack(
          ["bytes1", "bytes32", "uint256", "uint256", "bytes1"],
          [
            "0x19",
            appIdentityToHash(this.appIdentity),
            NONCE_EXPIRY,
            this.timeout,
            "0x01"
          ]
        )
      );
    }
    const appIdHash = appIdentityToHash(this.appIdentity);
    return keccak256(
      solidityPack(
        ["bytes1", "bytes32", "uint256", "uint256", "bytes32"],
        [
          "0x19",
          appIdHash,
          this.appLocalNonce,
          this.timeout,
          this.hashedAppState
        ]
      )
    );
  }

  public transaction(
    sigs: Signature[],
    intermediariesSignature?: Signature[]
  ): Transaction {
    if (intermediariesSignature) {
      if (intermediariesSignature.length === 0) {
        throw Error("transaction must receive intermediary signature");
      }
      return {
        to: this.networkContext.AppRegistry,
        value: 0,
        data: iface.functions.virtualAppSetState.encode([
          this.appIdentity,
          this.getSignedStateHashUpdate(sigs, intermediariesSignature)
        ])
      };
    }
    return {
      to: this.networkContext.AppRegistry,
      value: 0,
      data: iface.functions.setState.encode([
        this.appIdentity,
        this.getSignedStateHashUpdate(sigs)
      ])
    };
  }

  private getSignedStateHashUpdate(
    signatures: Signature[],
    intermediariesSignature?: Signature[]
  ): any {
    if (intermediariesSignature) {
      return {
        appStateHash: this.hashedAppState!,
        nonce: this.appLocalNonce!,
        timeout: this.timeout,
        signatures: signaturesToBytes(
          ...sortSignaturesBySignerAddress(this.hashToSign(false), signatures),
          ...sortSignaturesBySignerAddress(
            this.hashToSign(true),
            intermediariesSignature
          )
        ),
        nonceExpiry: NONCE_EXPIRY
      };
    }
    return {
      appStateHash: this.hashedAppState!,
      nonce: this.appLocalNonce!,
      timeout: this.timeout,
      signatures: signaturesToBytesSortedBySignerAddress(
        this.hashToSign(),
        ...signatures
      )
    };
  }
}
