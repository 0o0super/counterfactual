import { utils } from "@counterfactual/cf.js";
import AppRegistry from "@counterfactual/contracts/build/AppRegistry.json";
import { AppIdentity, NetworkContext } from "@counterfactual/types";
import { Interface, keccak256, Signature, solidityPack } from "ethers/utils";

import { EthereumCommitment, Transaction } from "./types";
import { appIdentityToHash } from "./utils/app-identity";
const { signaturesToBytes, sortSignaturesBySignerAddress } = utils;

// hardcoded assumption: all installed virtual apps can go through this many update operations
const NONCE_EXPIRY = 65536;

const iface = new Interface(AppRegistry.abi);

export class VirtualAppSetStateCommitment extends EthereumCommitment {
  constructor(
    public readonly networkContext: NetworkContext,
    public readonly appIdentity: AppIdentity,
    public readonly timeout: number,
    // todo(xuanji): the following two are set to null for intermediary. This
    // is bad API design and should be fixed eventually.
    public readonly hashedSolidityABIEncoderV2Struct?: string,
    public readonly appLocalNonce?: number
  ) {
    super();
  }

  public toJsonString() {
    return JSON.stringify({
      appIdentity: this.appIdentity,
      timeout: this.timeout,
      hashedSolidityABIEncoderV2Struct: this.hashedSolidityABIEncoderV2Struct,
      appLocalNonce: this.appLocalNonce
    });
  }

  public static fromJsonSrting(str: string, networkContext: NetworkContext) {
    const fromStringInstance = JSON.parse(str);
    return new VirtualAppSetStateCommitment(
      networkContext,
      fromStringInstance.appIdentity,
      fromStringInstance.timeout,
      fromStringInstance.hashedSolidityABIEncoderV2Struct,
      fromStringInstance.appLocalNonce
    );
  }

  /// overrides EthereumCommitment::hashToSign
  public hashToSign(signerIsIntermediary: boolean): string {
    if (signerIsIntermediary) {
      /// keep in sync with `digest2` definition
      return keccak256(
        solidityPack(
          ["bytes1", "bytes32", "uint256", "uint256", "bytes1"],
          [
            "0x19",
            appIdentityToHash(this.appIdentity),
            NONCE_EXPIRY,
            this.timeout,
            "0x01"
          ]
        )
      );
    }
    /// keep in sync with `digest` definition
    return keccak256(
      solidityPack(
        ["bytes1", "bytes32", "uint256", "uint256", "bytes32"],
        [
          "0x19",
          appIdentityToHash(this.appIdentity),
          this.appLocalNonce!,
          this.timeout,
          this.hashedSolidityABIEncoderV2Struct
        ]
      )
    );
  }

  // overrides EthereumCommitment::Transaction
  public transaction(
    signatures: Signature[],
    intermediariesSignature: Signature[]
  ): Transaction {
    if (!intermediariesSignature || intermediariesSignature.length === 0) {
      throw Error("transaction must receive intermediary signature");
    }
    return {
      to: this.networkContext.AppRegistry,
      value: 0,
      data: iface.functions.virtualAppSetState.encode([
        this.appIdentity,
        this.getSignedStateHashUpdate(signatures, intermediariesSignature)
      ])
    };
  }

  /// keep in sync with virtualAppSetState
  private getSignedStateHashUpdate(
    signatures: Signature[],
    intermediariesSignature: Signature[]
  ): any {
    return {
      appStateHash: this.hashedSolidityABIEncoderV2Struct!,
      nonce: this.appLocalNonce!,
      timeout: this.timeout,
      signatures: signaturesToBytes(
        ...sortSignaturesBySignerAddress(this.hashToSign(false), signatures),
        ...sortSignaturesBySignerAddress(
          this.hashToSign(true),
          intermediariesSignature
        )
      ),
      nonceExpiry: NONCE_EXPIRY
    };
  }
}
