import { utils } from "@counterfactual/cf.js";
import AppRegistry from "@counterfactual/contracts/build/AppRegistry.json";
import { AppIdentity, NetworkContext } from "@counterfactual/types";
import { Interface, keccak256, Signature, solidityPack } from "ethers/utils";

import { EthereumCommitment, Transaction } from "./types";
import { appIdentityToHash } from "./utils/app-identity";
const {
  signaturesToBytes,
  sortSignaturesBySignerAddress,
  signaturesToBytesSortedBySignerAddress
} = utils;

const iface = new Interface(AppRegistry.abi);

// hardcoded assumption: all installed virtual apps can go through this many update operations
const NONCE_EXPIRY = 65536;

export class CancelDisputeCommitment extends EthereumCommitment {
  constructor(
    public readonly networkContext: NetworkContext,
    public readonly appIdentity: AppIdentity,
    public readonly timeout: number,
    public readonly hashedAppState?: string,
    public readonly appLocalNonce?: number
  ) {
    super();
  }

  public toJsonString() {
    return JSON.stringify({
      appIdentity: this.appIdentity,
      hashedAppState: this.hashedAppState,
      appLocalNonce: this.appLocalNonce,
      timeout: this.timeout
    });
  }

  public static fromJsonString(str: string, networkContext: NetworkContext) {
    const fromStringInstance = JSON.parse(str);
    return new CancelDisputeCommitment(
      networkContext,
      fromStringInstance.appIdentity,
      fromStringInstance.timeout,
      fromStringInstance.hashedAppState,
      fromStringInstance.appLocalNonce
    );
  }

  public hashToSign(signerIsIntermediary?: boolean): string {
    if (signerIsIntermediary) {
      /// keep in sync with `digest2` definition
      return keccak256(
        solidityPack(
          ["bytes1", "bytes32", "uint256", "uint256", "bytes1"],
          [
            "0x19",
            appIdentityToHash(this.appIdentity),
            NONCE_EXPIRY,
            this.timeout,
            "0x01"
          ]
        )
      );
    }
    return keccak256(
      solidityPack(
        ["bytes1", "bytes32", "uint256", "uint256", "bytes32", "bytes1"],
        [
          "0x19",
          appIdentityToHash(this.appIdentity),
          this.appLocalNonce,
          this.timeout,
          this.hashedAppState,
          "0xcd" // indicate this is a cancel-dispute commitment, not set-state
        ]
      )
    );
  }

  public transaction(
    sigs: Signature[],
    intermediariesSignature?: Signature[]
  ): Transaction {
    if (intermediariesSignature) {
      if (intermediariesSignature.length === 0) {
        throw Error("transaction must receive intermediary signature");
      }
      return {
        to: this.networkContext.AppRegistry,
        value: 0,
        data: iface.functions.cancelChallengeVirtual.encode([
          this.appIdentity,
          NONCE_EXPIRY,
          signaturesToBytes(
            ...sortSignaturesBySignerAddress(this.hashToSign(false), sigs),
            ...sortSignaturesBySignerAddress(
              this.hashToSign(true),
              intermediariesSignature
            )
          )
        ])
      };
    }
    return {
      to: this.networkContext.AppRegistry,
      value: 0,
      data: iface.functions.cancelChallenge.encode([
        this.appIdentity,
        signaturesToBytesSortedBySignerAddress(this.hashToSign(), ...sigs)
      ])
    };
  }
}
