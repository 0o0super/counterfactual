import {
  AppInterface,
  AssetType,
  SolidityABIEncoderV2Struct
} from "@counterfactual/types";
import { AddressZero } from "ethers/constants";
import { bigNumberify, BigNumberish, Signature } from "ethers/utils";

import { SetStateCommitment } from "../ethereum";
import { ETHVirtualAppAgreementCommitment } from "../ethereum/eth-virtual-app-agreement-commitment";
import {
  CommitmentType,
  InstallVirtualAppState,
  Opcode
} from "../machine/enums";
import {
  Context,
  InstallVirtualAppParams,
  ProtocolMessage,
  ProtocolParameters
} from "../machine/types";
import { virtualChannelKey } from "../machine/virtual-app-key";
import { xkeyKthAddress, xkeysToSortedKthAddresses } from "../machine/xkeys";
import {
  AppInstance,
  ETHVirtualAppAgreementInstance,
  StateChannel
} from "../models";

import { validateSignature } from "./utils/signature-validator";

type InstallVirtualProtocolMessage = ProtocolMessage & {
  params: InstallVirtualAppParams;
};

/**
 * @description This exchange is described at the following URL:
 * https://specs.counterfactual.com/09-install-virtual-app-protocol
 *
 * Commitments Storage Layout:
 * VAA: Virtual App Agreement
 * VASS: Virtual App Set State
 */
export const INSTALL_VIRTUAL_APP_PROTOCOL = async function*(context: Context) {
  const {
    initiatingXpub,
    intermediariesXpub,
    respondingXpub,
    targetAppIdentityHash
  } = context.message.params as InstallVirtualAppParams;
  const participantsXpub = getParticipantsXpub(context);
  const myIndex = participantsXpub.indexOf(context.me);
  const participantsVASSSig = new Map<string, Signature>();

  const key = virtualChannelKey(
    [initiatingXpub, respondingXpub],
    intermediariesXpub
  );

  // lock virtual channel!
  let [virtualSc]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    ``,
    context.message,
    key
  ];
  if (virtualSc.userNeuteredExtendedKeys.length === 0) {
    // this is a placeholder, not a good way...
    virtualSc = StateChannel.createEmptyChannel(key, [
      initiatingXpub,
      respondingXpub
    ]);
  }

  const [
    targetApp,
    virtualAppSetStateCommitment,
    newVirtualSc
  ] = proposeStateTransition(context.message.params, context, virtualSc);

  // save mapping
  yield [
    Opcode.OP_MAP_REALISED_ID_HASH,
    targetAppIdentityHash,
    targetApp.identityHash
  ];

  const amIIntermediary = intermediariesXpub.includes(context.me);
  const vASSSig = yield [
    amIIntermediary ? Opcode.OP_SIGN_AS_INTERMEDIARY : Opcode.OP_SIGN,
    virtualAppSetStateCommitment,
    targetApp.appSeqNo
  ];
  participantsVASSSig.set(context.me, vASSSig);

  if (context.me !== initiatingXpub) {
    // validate message from initiating first
    validateSignature(
      xkeyKthAddress(initiatingXpub, targetApp.appSeqNo),
      virtualAppSetStateCommitment,
      context.message.signatures[0]
    );
    // REMEMBER TO SAVE SIG!!
    participantsVASSSig.set(initiatingXpub, context.message.signatures[0]);
  } else {
    const messageTo = participantsXpub.filter(p => p !== context.me);
    const msg = setStateMessage(context, vASSSig, messageTo, true);
    // send set-state to all participants(except me, ofc)
    yield [Opcode.IO_SEND, context.requestId, msg];
  }

  const middlewareCall: any[] = [];
  const counterpartyOps = new Map<string, AsyncIterableIterator<any>>();
  const genChannelAcquireCall = (cp: string) => [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${cp}${InstallVirtualAppState.CHANNEL_AGREEMENT}`,
    {
      ...context.message,
      signatures: []
    },
    [context.me, cp]
  ];
  if (myIndex !== 0) {
    middlewareCall.push(genChannelAcquireCall(participantsXpub[myIndex - 1]));
  }
  if (myIndex !== participantsXpub.length - 1) {
    middlewareCall.push(genChannelAcquireCall(participantsXpub[myIndex + 1]));
  }

  const promisesToResolve: Promise<any>[] = yield middlewareCall;
  while (promisesToResolve.length > 0) {
    const indexedPromises: Promise<[number, any]>[] = promisesToResolve.map(
      (p, i) =>
        new Promise(r =>
          p.then(ret => {
            r([i, ret]);
          })
        )
    );
    const winningRet = await Promise.race(indexedPromises);
    // delete wining promise
    promisesToResolve.splice(winningRet[0], 1);
    if (winningRet[1][0].protocol) {
      // this return ProtocolMessage[]
      const msg: ProtocolMessage[] = winningRet[1];
      // generator is waiting IO_WAIT!!
      // sould be only one message
      const op = counterpartyOps.get(msg[0].fromXpub)!;
      let lastMiddlewareReturn: any = msg;
      while (true) {
        const { done, value } = await op.next(lastMiddlewareReturn);
        if (done) break;
        const [operation] = value;
        if (operation !== Opcode.IO_WAIT) {
          lastMiddlewareReturn = yield value;
        } else {
          promisesToResolve.push((yield [value])[0]);
          break;
        }
      }
    } else {
      // this return [StateChannel, ProtocolMessage]
      const [channel, message]: [StateChannel, ProtocolMessage] = winningRet[1];
      const sortedInitiating =
        channel.userNeuteredExtendedKeys[channel.numOperationExecuted % 2];
      const sortedResponding =
        channel.userNeuteredExtendedKeys[
          (channel.numOperationExecuted + 1) % 2
        ];

      let op: AsyncIterableIterator<any>;
      let counterparty: string;
      switch (context.me) {
        case sortedInitiating:
          op = sortedInitiatingSteps(
            context,
            sortedResponding,
            targetApp,
            channel
          );
          counterparty = sortedResponding;
          break;
        case sortedResponding:
          op = sortedRespondingSteps(
            context,
            message,
            sortedInitiating,
            targetApp,
            channel
          );
          counterparty = sortedInitiating;
          break;
        default:
          throw Error("Wrong participants in install virtual");
      }
      counterpartyOps.set(counterparty, op);
      let lastMiddlewareReturn: any = undefined;
      while (true) {
        const { done, value } = await op.next(lastMiddlewareReturn);
        if (done) break;
        const [operation] = value;
        if (operation !== Opcode.IO_WAIT) {
          lastMiddlewareReturn = yield value;
        } else {
          promisesToResolve.push((yield [value])[0]);
          break;
        }
      }
    }
  }

  // finishing step! virtual-app set-state!
  if (context.me !== initiatingXpub) {
    // send set-sate to everyone else
    const messagesTo = participantsXpub.filter(p => p !== context.me);
    const msg = setStateMessage(context, vASSSig, messagesTo, false);
    yield [Opcode.IO_SEND, context.requestId, msg];
  }

  // wait everyone's respond except me and initiating
  const waitingKeys = participantsXpub
    .filter(p => p !== context.me && p !== initiatingXpub)
    .map(
      xpub => `${context.requestId}${xpub}${InstallVirtualAppState.SET_STATE}`
    );

  const responds: ProtocolMessage[] = yield [Opcode.IO_WAIT, ...waitingKeys];

  for (const resp of responds) {
    const isIntermediary = intermediariesXpub.includes(resp.fromXpub);
    // validate signature
    validateSignature(
      // 0 or appSeqNo?????
      xkeyKthAddress(resp.fromXpub, targetApp.appSeqNo),
      virtualAppSetStateCommitment,
      resp.signatures[0],
      isIntermediary
    );
    // save validated sig
    participantsVASSSig.set(resp.fromXpub, resp.signatures[0]);
  }
  if (participantsVASSSig.size !== participantsXpub.length) {
    throw Error("Reach finish without enough signatures");
  }

  // unlock virtual channel
  yield [Opcode.LOCK_RELEASE, newVirtualSc];

  // seperate signatures, should have better method
  const iniAndRespSig: Signature[] = [];
  const intermediariesSig: Signature[] = [];
  for (const [addr, sig] of participantsVASSSig) {
    const index = participantsXpub.indexOf(addr);
    if (index !== 0 && index !== participantsXpub.length) {
      // intermediaries
      intermediariesSig.push(sig);
    } else {
      // ini or resp
      iniAndRespSig.push(sig);
    }
  }

  // save commitments
  const finalVirtualAppSetStateCommitment = virtualAppSetStateCommitment.transaction(
    iniAndRespSig,
    intermediariesSig
  );

  // this will work but redundent
  participantsVASSSig.delete(participantsXpub[0]);
  participantsVASSSig.delete(participantsXpub[participantsXpub.length - 1]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.VirtualAppSetState,
    finalVirtualAppSetStateCommitment,
    targetApp.identityHash,
    participantsVASSSig
  ];
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  targetApp: AppInstance,
  sc: StateChannel
) {
  // alter state channel state
  const [commitment, newStateChannel] = prepareChannelAgreementCommitment(
    context.message.params,
    context,
    sc, // has to be defined
    targetApp.identityHash
  );

  const mySig = yield [Opcode.OP_SIGN, commitment];
  // send and wait response
  yield [
    Opcode.IO_SEND,
    context.requestId,
    channelAgreementMessage(context, mySig, sortedResponding)
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${
      InstallVirtualAppState.CHANNEL_AGREEMENT
    }`
  ]) as ProtocolMessage[];

  // validate signature
  validateSignature(
    // 0 or appSeqNo?????
    xkeyKthAddress(sortedResponding, 0),
    commitment,
    theirSig
  );

  // we can unlock this channel now
  yield [Opcode.LOCK_RELEASE, newStateChannel];

  // save commitment!
  const finalChannelAgreement = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.ETHVirtualAppAgreement,
    finalChannelAgreement,
    targetApp.identityHash
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  message: ProtocolMessage,
  sortedInitiating: string,
  targetApp: AppInstance,
  sc: StateChannel
) {
  // alter state channel state
  const [commitment, newStateChannel] = prepareChannelAgreementCommitment(
    context.message.params,
    context,
    sc,
    targetApp.identityHash
  );

  const {
    signatures: [theirSig]
  } = message;
  // validate signature
  validateSignature(
    // 0 or appSeqNo?????
    xkeyKthAddress(sortedInitiating, 0),
    commitment,
    theirSig
  );

  // we can unlock this channel now
  yield [Opcode.LOCK_RELEASE, newStateChannel];

  const mySig = yield [Opcode.OP_SIGN, commitment];
  // send my signature
  yield [
    Opcode.IO_SEND,
    context.requestId,
    channelAgreementMessage(context, mySig, sortedInitiating)
  ];

  // save commitment!
  const finalChannelAgreement = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.ETHVirtualAppAgreement,
    finalChannelAgreement,
    targetApp.identityHash
  ];
}

function getParticipantsXpub(context: Context) {
  const { initiatingXpub, intermediariesXpub, respondingXpub } = context.message
    .params as InstallVirtualAppParams;

  return [initiatingXpub, ...intermediariesXpub, respondingXpub];
}

function setStateMessage(
  context: Context,
  sig: Signature,
  toXpub: string[],
  isInitiating: boolean
) {
  return {
    ...context.message,
    messageType: isInitiating
      ? InstallVirtualAppState.INITIATE
      : InstallVirtualAppState.SET_STATE,
    signatures: [sig],
    toXpub: toXpub,
    fromXpub: context.me // bad, fix me
  } as InstallVirtualProtocolMessage;
}

function channelAgreementMessage(
  context: Context,
  sig: Signature,
  toXpub: string
) {
  return {
    ...context.message,
    messageType: InstallVirtualAppState.CHANNEL_AGREEMENT,
    toXpub: [toXpub],
    fromXpub: context.me, // bad, fix me
    signatures: [sig]
  } as InstallVirtualProtocolMessage;
}

function createAndAddTarget(
  defaultTimeout: number,
  appInterface: AppInterface,
  initialState: SolidityABIEncoderV2Struct,
  initiatingBalanceDecrement: BigNumberish, // FIXME: serialize
  respondingBalanceDecrement: BigNumberish,
  initiatingXpub: string,
  respondingXpub: string,
  intermediariesXpub: string[],
  virtualSc: StateChannel
): [AppInstance, StateChannel] {
  const appSeqNo = virtualSc.numInstalledApps;

  // https://github.com/counterfactual/specs/blob/master/09-install-virtual-app-protocol.md#derived-fields
  const signingKeys = xkeysToSortedKthAddresses(
    [initiatingXpub, respondingXpub],
    appSeqNo
  ).concat(xkeysToSortedKthAddresses(intermediariesXpub, appSeqNo));

  const target = new AppInstance(
    AddressZero,
    signingKeys,
    defaultTimeout,
    appInterface,
    {
      assetType: AssetType.ETH,
      limit: bigNumberify(initiatingBalanceDecrement).add(
        bigNumberify(respondingBalanceDecrement)
      ),
      token: AddressZero // TODO: support tokens
    },
    true, // sets it to be a virtual app
    virtualSc.numInstalledApps, // app seq no
    0, // root nonce value: virtual app instances do not have rootNonceValue
    initialState,
    0, // app nonce
    defaultTimeout
  );

  const newStateChannel = virtualSc.addVirtualAppInstance(target);

  return [target, newStateChannel];
}

function proposeStateTransition(
  params: ProtocolParameters,
  context: Context,
  virtualSC: StateChannel
): [AppInstance, SetStateCommitment, StateChannel] {
  const {
    intermediariesXpub,
    initiatingXpub,
    respondingXpub,
    defaultTimeout,
    appInterface,
    initialState,
    initiatingBalanceDecrement,
    respondingBalanceDecrement
  } = params as InstallVirtualAppParams;

  const [targetAppInstance, newVirtualSc] = createAndAddTarget(
    defaultTimeout,
    appInterface,
    initialState,
    initiatingBalanceDecrement,
    respondingBalanceDecrement,
    initiatingXpub,
    respondingXpub,
    intermediariesXpub,
    virtualSC
  );

  const virtualAppSetStateCommitment = new SetStateCommitment(
    context.network,
    targetAppInstance.identity,
    targetAppInstance.defaultTimeout,
    targetAppInstance.hashOfLatestState,
    0
  );

  return [targetAppInstance, virtualAppSetStateCommitment, newVirtualSc];
}

function prepareChannelAgreementCommitment(
  params: ProtocolParameters,
  context: Context,
  sc: StateChannel,
  targetAppIdentityHash: string
): [ETHVirtualAppAgreementCommitment, StateChannel] {
  const {
    intermediariesXpub,
    initiatingXpub,
    respondingXpub,
    initiatingBalanceDecrement,
    respondingBalanceDecrement
  } = params as InstallVirtualAppParams;

  const participantsXpub = [
    initiatingXpub,
    ...intermediariesXpub,
    respondingXpub
  ];

  const myIndex = participantsXpub.indexOf(context.me);
  const counterPartyIndex = participantsXpub.indexOf(
    sc.userNeuteredExtendedKeys.filter(a => a !== context.me)[0]
  );

  let leftAddress: string;
  let rightAddress: string;
  if (myIndex > counterPartyIndex) {
    // sc is left channel
    leftAddress = xkeyKthAddress(participantsXpub[counterPartyIndex], 0);
    rightAddress = xkeyKthAddress(participantsXpub[myIndex], 0);
  } else {
    // sc is right channel
    leftAddress = xkeyKthAddress(participantsXpub[myIndex], 0);
    rightAddress = xkeyKthAddress(participantsXpub[counterPartyIndex], 0);
  }

  const ethVirtualAppAgreementInstance = new ETHVirtualAppAgreementInstance(
    sc.multisigAddress,
    {
      assetType: AssetType.ETH,
      limit: bigNumberify(initiatingBalanceDecrement).add(
        respondingBalanceDecrement
      ),
      token: AddressZero
    },
    sc.numInstalledApps,
    sc.rootNonceValue,
    100,
    bigNumberify(initiatingBalanceDecrement).add(respondingBalanceDecrement),
    targetAppIdentityHash,
    // reversed! because install is in reverse direction of propose
    rightAddress,
    leftAddress
  );

  // S6
  const newchannel = sc.installETHVirtualAppAgreementInstance(
    ethVirtualAppAgreementInstance,
    targetAppIdentityHash,
    {
      [leftAddress]: initiatingBalanceDecrement,
      [rightAddress]: respondingBalanceDecrement
    }
  );
  const freeBalance = sc.getFreeBalanceFor(AssetType.ETH);

  const commitment = new ETHVirtualAppAgreementCommitment(
    context.network,
    sc.multisigAddress,
    sc.multisigOwners,
    targetAppIdentityHash,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.hashOfLatestState,
    freeBalance.nonce,
    freeBalance.timeout,
    freeBalance.appSeqNo,
    freeBalance.rootNonceValue,
    bigNumberify(ethVirtualAppAgreementInstance.expiry),
    bigNumberify(ethVirtualAppAgreementInstance.capitalProvided),
    [
      ethVirtualAppAgreementInstance.beneficiary1,
      ethVirtualAppAgreementInstance.beneficiary2
    ],
    ethVirtualAppAgreementInstance.uninstallKey
  );

  return [commitment, newchannel];
}
