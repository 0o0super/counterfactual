import { NetworkContext } from "@counterfactual/types";
import { Signature } from "ethers/utils";

import { SetStateCommitment } from "../ethereum";
import { CommitmentType, Opcode, UpdateState } from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  Transaction,
  UpdateParams
} from "../machine/types";
import { xkeyKthAddress } from "../machine/xkeys";
import { StateChannel } from "../models/state-channel";

import { validateSignature } from "./utils/signature-validator";

/**
 * @description This exchange is described at the following URL:
 *
 * specs.counterfactual.com/07-update-protocol#messages
 *
 */

export const UPDATE_PROTOCOL = async function*(context: Context) {
  const { multisigAddress, initiatingXpub, respondingXpub } = context.message
    .params as UpdateParams;

  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${UpdateState.AGREEMENT}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const { appIdentityHash, respondingXpub } = message.params as UpdateParams;

  const [commitment, newSC] = await proposeStateTransition(
    message.params as UpdateParams,
    context.network,
    sc
  );
  const { appSeqNo, isVirtualApp } = newSC.getAppInstance(appIdentityHash);

  const mySig = yield [Opcode.OP_SIGN, commitment, appSeqNo];
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: UpdateState.AGREEMENT,
      toXpub: [sortedResponding],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${UpdateState.AGREEMENT}`
  ]) as ProtocolMessage[];

  validateSignature(
    xkeyKthAddress(sortedResponding, appSeqNo),
    commitment,
    theirSig
  );

  // save channel and release
  yield [Opcode.LOCK_RELEASE, newSC];

  let finalCommitment: Transaction;
  if (isVirtualApp) {
    // get intermediaries signature from install-virtual!
    const sigs: Map<string, Signature> = yield [
      Opcode.OP_GET_INTERMEDIARY_SIG,
      appIdentityHash
    ];
    finalCommitment = commitment.transaction(
      [mySig, theirSig],
      [...sigs.values()]
    );
  } else {
    finalCommitment = commitment.transaction([mySig, theirSig]);
  }

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.SetState,
    finalCommitment,
    appIdentityHash
  ];

  if (context.me === respondingXpub) {
    // send extra ack message!
    yield [
      Opcode.IO_SEND,
      context.requestId,
      {
        ...message,
        messageType: 2,
        toXpub: [sortedResponding],
        signatures: []
      } as ProtocolMessage
    ];
  }
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  // construct commitment and validate message from counterparty
  const { appIdentityHash, initiatingXpub } = message.params as UpdateParams;

  const [commitment, newSC] = await proposeStateTransition(
    message.params as UpdateParams,
    context.network,
    sc
  );
  const { appSeqNo, isVirtualApp } = newSC.getAppInstance(appIdentityHash);

  // validate message from initiating
  const {
    signatures: [theirSig]
  } = message;
  validateSignature(
    xkeyKthAddress(sortedInitiating, appSeqNo),
    commitment,
    theirSig
  );

  // release and save state-channel
  yield [Opcode.LOCK_RELEASE, newSC];

  const mySig = yield [Opcode.OP_SIGN, commitment, appSeqNo];

  // send mysig to counterparty!
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: UpdateState.AGREEMENT,
      toXpub: [sortedInitiating],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  let finalCommitment: Transaction;
  if (isVirtualApp) {
    // get intermediaries signature from install-virtual!
    const sigs: Map<string, Signature> = yield [
      Opcode.OP_GET_INTERMEDIARY_SIG,
      appIdentityHash
    ];
    finalCommitment = commitment.transaction(
      [mySig, theirSig],
      [...sigs.values()]
    );
  } else {
    finalCommitment = commitment.transaction([mySig, theirSig]);
  }

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.SetState,
    finalCommitment,
    appIdentityHash
  ];

  if (context.me === initiatingXpub) {
    // send extra ack message!
    yield [Opcode.IO_WAIT, `${context.requestId}${sortedInitiating}2`];
  }
}

function proposeStateTransition(
  params: UpdateParams,
  network: NetworkContext,
  sc: StateChannel
): [SetStateCommitment, StateChannel] {
  const { appIdentityHash, newState } = params;
  const newStateChannel = sc.setState(appIdentityHash, newState);

  const setStateCommitment = constructUpdateOp(
    network,
    newStateChannel,
    appIdentityHash
  );

  return [setStateCommitment, newStateChannel];
}

function constructUpdateOp(
  network: NetworkContext,
  stateChannel: StateChannel,
  appIdentityHash: string
) {
  const app = stateChannel.getAppInstance(appIdentityHash);

  return new SetStateCommitment(
    network,
    app.identity,
    app.timeout,
    app.hashOfLatestState,
    app.nonce
  );
}
