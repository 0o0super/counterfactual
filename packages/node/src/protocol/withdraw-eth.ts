import {
  AssetType,
  ETHBucketAppState,
  NetworkContext
} from "@counterfactual/types";
import { AddressZero, Zero } from "ethers/constants";

import {
  InstallCommitment,
  UninstallCommitment,
  WithdrawETHCommitment
} from "../ethereum";
import { xkeyKthAddress } from "../machine";
import { CommitmentType, Opcode, WithdrawState } from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  ProtocolParameters,
  WithdrawParams
} from "../machine/types";
import { AppInstance, StateChannel } from "../models";

import { validateSignature } from "./utils/signature-validator";

/**
 * @description This exchange is described at the following URL:
 * https://specs.counterfactual.com/11-withdraw-protocol *
 */

export const WITHDRAW_ETH_PROTOCOL = async function*(context: Context) {
  const { multisigAddress, initiatingXpub, respondingXpub } = context.message
    .params as WithdrawParams;

  // first acquire statechannel
  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${WithdrawState.INSTALL_WITHDRAW}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const { multisigAddress } = context.message.params as WithdrawParams;
  const sortedRespondingAddress = xkeyKthAddress(sortedResponding, 0);

  const [
    installRefundCommitment,
    newSC,
    refundAppIdentityHash
  ] = addInstallRefundAppCommitmentToContext(
    context.message.params,
    context,
    sc
  );
  const withdrawETHCommitment = addMultisigSendCommitmentToContext(
    context.message,
    newSC
  );

  const s1 = yield [Opcode.OP_SIGN, installRefundCommitment];
  const s3 = yield [Opcode.OP_SIGN, withdrawETHCommitment];
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: WithdrawState.INSTALL_WITHDRAW,
      toXpub: [sortedResponding],
      signatures: [s1, s3]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [s2, s4, s6]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${WithdrawState.INSTALL_WITHDRAW}`
  ]) as ProtocolMessage[];

  // counterparty will send uninstall!
  const [
    uninstallRefundCommitment,
    newSC2
  ] = addUninstallRefundAppCommitmentToContext(
    context,
    newSC,
    refundAppIdentityHash
  );

  validateSignature(sortedRespondingAddress, installRefundCommitment, s2);
  validateSignature(sortedRespondingAddress, withdrawETHCommitment, s4);
  validateSignature(sortedRespondingAddress, uninstallRefundCommitment, s6);

  // release state-channel
  yield [Opcode.LOCK_RELEASE, newSC2];

  const s5 = yield [Opcode.OP_SIGN, uninstallRefundCommitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: WithdrawState.UNINSTALL_WITHDRAW,
      toXpub: [sortedResponding],
      signatures: [s5]
    } as ProtocolMessage
  ];

  const finalCommitment = withdrawETHCommitment.transaction([s3, s4]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.WithdrawETH,
    finalCommitment,
    multisigAddress
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  const { multisigAddress } = context.message.params as WithdrawParams;
  const sortedInitiatingAddress = xkeyKthAddress(sortedInitiating, 0);

  const [
    installRefundCommitment,
    newSC,
    refundAppIdentityHash
  ] = addInstallRefundAppCommitmentToContext(
    context.message.params,
    context,
    sc
  );
  const withdrawETHCommitment = addMultisigSendCommitmentToContext(
    context.message,
    newSC
  );

  const {
    signatures: [s1, s3]
  } = message;

  validateSignature(sortedInitiatingAddress, installRefundCommitment, s1);
  validateSignature(sortedInitiatingAddress, withdrawETHCommitment, s3);

  const [
    uninstallRefundCommitment,
    newSC2
  ] = addUninstallRefundAppCommitmentToContext(
    context,
    newSC,
    refundAppIdentityHash
  );

  const s2 = yield [Opcode.OP_SIGN, installRefundCommitment];
  const s4 = yield [Opcode.OP_SIGN, withdrawETHCommitment];
  const s6 = yield [Opcode.OP_SIGN, uninstallRefundCommitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: WithdrawState.INSTALL_WITHDRAW,
      toXpub: [sortedInitiating],
      signatures: [s2, s4, s6]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [s5]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedInitiating}${WithdrawState.UNINSTALL_WITHDRAW}`
  ]) as ProtocolMessage[];

  validateSignature(sortedInitiatingAddress, uninstallRefundCommitment, s5);

  // unlock channel
  yield [Opcode.LOCK_RELEASE, newSC2];

  // save commitment
  const finalCommitment = withdrawETHCommitment.transaction([s3, s4]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.WithdrawETH,
    finalCommitment,
    multisigAddress
  ];
}

function addInstallRefundAppCommitmentToContext(
  params: ProtocolParameters,
  context: Context,
  sc: StateChannel
): [InstallCommitment, StateChannel, string] {
  const {
    recipient,
    amount,
    multisigAddress,
    initiatingXpub
  } = params as WithdrawParams;

  const appInstance = new AppInstance(
    multisigAddress,
    sc.getNextSigningKeys(),
    1008,
    {
      addr: context.network.ETHBalanceRefundApp,
      stateEncoding:
        "tuple(address recipient, address multisig,  uint256 threshold)",
      actionEncoding: undefined
    },
    {
      assetType: AssetType.ETH,
      limit: amount,
      token: AddressZero
    },
    false,
    sc.numInstalledApps,
    sc.rootNonceValue,
    {
      recipient,
      multisig: multisigAddress,
      threshold: amount
    },
    0,
    1008
  );

  let aliceBalanceDecrement = Zero;
  let bobBalanceDecrement = Zero;

  if (
    sc.getFreeBalanceAddrOf(initiatingXpub, AssetType.ETH) ===
    sc.multisigOwners[0]
  ) {
    aliceBalanceDecrement = amount;
  } else {
    bobBalanceDecrement = amount;
  }

  const newStateChannel = sc.installApp(
    appInstance,
    aliceBalanceDecrement,
    bobBalanceDecrement
  );

  const installRefundCommitment = constructInstallOp(
    context.network,
    newStateChannel,
    appInstance.identityHash
  );

  return [installRefundCommitment, newStateChannel, appInstance.identityHash];
}

function addUninstallRefundAppCommitmentToContext(
  context: Context,
  sc: StateChannel,
  appIdentityHash: string
): [UninstallCommitment, StateChannel] {
  const newStateChannel = sc.uninstallApp(appIdentityHash, Zero, Zero);

  const freeBalance = newStateChannel.getFreeBalanceFor(AssetType.ETH);

  const uninstallCommitment = new UninstallCommitment(
    context.network,
    newStateChannel.multisigAddress,
    newStateChannel.multisigOwners,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.state as ETHBucketAppState,
    freeBalance.nonce,
    freeBalance.timeout,
    freeBalance.appSeqNo
  );

  return [uninstallCommitment, newStateChannel];
}

function addMultisigSendCommitmentToContext(
  message: ProtocolMessage,
  sc: StateChannel
) {
  const { recipient, amount } = message.params as WithdrawParams;

  return new WithdrawETHCommitment(
    sc.multisigAddress,
    sc.multisigOwners,
    recipient,
    amount
  );
}

function constructInstallOp(
  network: NetworkContext,
  stateChannel: StateChannel,
  appIdentityHash: string
) {
  const app = stateChannel.getAppInstance(appIdentityHash);

  const freeBalance = stateChannel.getFreeBalanceFor(AssetType.ETH);

  return new InstallCommitment(
    network,
    stateChannel.multisigAddress,
    stateChannel.multisigOwners,
    app.identity,
    app.terms,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.hashOfLatestState,
    freeBalance.nonce,
    freeBalance.timeout,
    app.appSeqNo,
    freeBalance.rootNonceValue
  );
}
