import { AssetType, ETHBucketAppState } from "@counterfactual/types";
import { Signature } from "ethers/utils/bytes";

import { SetStateCommitment, UninstallCommitment } from "../ethereum";
import {
  CommitmentType,
  Opcode,
  UninstallVirtualAppState
} from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  ProtocolParameters,
  UninstallVirtualAppParams
} from "../machine/types";
import { virtualChannelKey } from "../machine/virtual-app-key";
import { xkeyKthAddress } from "../machine/xkeys";
import { StateChannel } from "../models";

import { computeFreeBalanceIncrements } from "./utils/get-resolution-increments";
import { validateSignature } from "./utils/signature-validator";

type UninstallVirtualProtocolMessage = ProtocolMessage & {
  params: UninstallVirtualAppParams;
};

export const UNINSTALL_VIRTUAL_APP_PROTOCOL = async function*(
  context: Context
) {
  const {
    initiatingXpub,
    intermediariesXpub,
    respondingXpub,
    targetAppIdentityHash
  } = context.message.params as UninstallVirtualAppParams;
  const participantsXpub = getParticipantsXpub(context);
  const myIndex = participantsXpub.indexOf(context.me);
  const isIntermediary = intermediariesXpub.includes(context.me);

  const key = virtualChannelKey(
    [initiatingXpub, respondingXpub],
    intermediariesXpub
  );

  // lock virtual channel!
  const [virtualSc]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    ``,
    context.message,
    key
  ];

  const [lockCommitment, newVirtualSc] = addVirtualAppStateTransitionToContext(
    context.message.params,
    context,
    isIntermediary,
    virtualSc
  );

  const lockCommitmentSigs = new Map<string, Signature>();
  const lockCommitSig = yield [
    isIntermediary ? Opcode.OP_SIGN_AS_INTERMEDIARY : Opcode.OP_SIGN,
    lockCommitment
  ];
  lockCommitmentSigs.set(context.me, lockCommitSig);

  if (context.me !== initiatingXpub) {
    // validate message from initiating first
    validateSignature(
      xkeyKthAddress(initiatingXpub, 0),
      lockCommitment,
      context.message.signatures[0]
    );
    lockCommitmentSigs.set(initiatingXpub, context.message.signatures[0]);
  } else {
    // initiate send lock commitment to everyone
    const messageTo = participantsXpub.filter(p => p !== context.me);
    const msg = setStateMessage(context, lockCommitSig, messageTo, true);
    // send set-state to all participants(except me, ofc)
    yield [Opcode.IO_SEND, context.requestId, msg];
  }

  const middlewareCall: any[] = [];
  const counterpartyOps = new Map<string, AsyncIterableIterator<any>>();
  const genChannelAcquireCall = (cp: string) => [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${cp}${UninstallVirtualAppState.CHANNEL_AGREEMENT}`,
    {
      ...context.message,
      signatures: []
    },
    [context.me, cp]
  ];
  if (myIndex !== 0) {
    middlewareCall.push(genChannelAcquireCall(participantsXpub[myIndex - 1]));
  }
  if (myIndex !== participantsXpub.length - 1) {
    middlewareCall.push(genChannelAcquireCall(participantsXpub[myIndex + 1]));
  }

  const promisesToResolve: Promise<any>[] = yield middlewareCall;
  while (promisesToResolve.length > 0) {
    const indexedPromises: Promise<[number, any]>[] = promisesToResolve.map(
      (p, i) =>
        new Promise(r =>
          p.then(ret => {
            r([i, ret]);
          })
        )
    );
    const winningRet = await Promise.race(indexedPromises);
    // delete wining promise
    promisesToResolve.splice(winningRet[0], 1);
    if (winningRet[1][0].protocol) {
      // this return ProtocolMessage[]
      const msg: ProtocolMessage[] = winningRet[1];
      // generator is waiting IO_WAIT!!
      // sould be only one message
      const op = counterpartyOps.get(msg[0].fromXpub)!;
      let lastMiddlewareReturn: any = msg;
      while (true) {
        const { done, value } = await op.next(lastMiddlewareReturn);
        if (done) break;
        const [operation] = value;
        if (operation !== Opcode.IO_WAIT) {
          lastMiddlewareReturn = yield value;
        } else {
          promisesToResolve.push((yield [value])[0]);
          break;
        }
      }
    } else {
      // this return [StateChannel, ProtocolMessage]
      const [channel, message]: [StateChannel, ProtocolMessage] = winningRet[1];
      const sortedInitiating =
        channel.userNeuteredExtendedKeys[channel.numOperationExecuted % 2];
      const sortedResponding =
        channel.userNeuteredExtendedKeys[
          (channel.numOperationExecuted + 1) % 2
        ];

      let op: AsyncIterableIterator<any>;
      let counterparty: string;
      switch (context.me) {
        case sortedInitiating:
          op = sortedInitiatingSteps(
            context,
            sortedResponding,
            newVirtualSc,
            channel
          );
          counterparty = sortedResponding;
          break;
        case sortedResponding:
          op = sortedRespondingSteps(
            context,
            message,
            sortedInitiating,
            newVirtualSc,
            channel
          );
          counterparty = sortedInitiating;
          break;
        default:
          throw Error("Wrong participants in install virtual");
      }
      counterpartyOps.set(counterparty, op);
      let lastMiddlewareReturn: any = undefined;
      while (true) {
        const { done, value } = await op.next(lastMiddlewareReturn);
        if (done) break;
        const [operation] = value;
        if (operation !== Opcode.IO_WAIT) {
          lastMiddlewareReturn = yield value;
        } else {
          promisesToResolve.push((yield [value])[0]);
          break;
        }
      }
    }
  }

  // finishing step! virtual-app set-state!
  if (context.me !== initiatingXpub) {
    // send set-sate to everyone else
    const messagesTo = participantsXpub.filter(p => p !== context.me);
    const msg = setStateMessage(context, lockCommitSig, messagesTo, false);
    yield [Opcode.IO_SEND, context.requestId, msg];
  }

  // send to everyone else
  // wait everyone's respond except me and initiating
  const waitingKeys = participantsXpub
    .filter(p => p !== context.me && p !== initiatingXpub)
    .map(
      xpub => `${context.requestId}${xpub}${UninstallVirtualAppState.SET_STATE}`
    );

  const resps: ProtocolMessage[] = yield [Opcode.IO_WAIT, ...waitingKeys];
  for (const resp of resps) {
    validateSignature(
      xkeyKthAddress(resp.fromXpub, 0),
      lockCommitment,
      resp.signatures[0],
      intermediariesXpub.includes(resp.fromXpub)
    );
    // SAVE SIG!!
    lockCommitmentSigs.set(resp.fromXpub, resp.signatures[0]);
  }

  // remove app in virtual channel
  const finalVirtualSc = newVirtualSc.removeVirtualApp(targetAppIdentityHash);

  // unlock virtual channel
  yield [Opcode.LOCK_RELEASE, finalVirtualSc];

  // write commitment for virtual channel
  // seperate signatures, should have better method
  const iniAndRespSig: Signature[] = [];
  const intermediariesSig: Signature[] = [];
  for (const [addr, sig] of lockCommitmentSigs) {
    const index = participantsXpub.indexOf(addr);
    if (index !== 0 && index !== participantsXpub.length) {
      // intermediaries
      intermediariesSig.push(sig);
    } else {
      // ini or resp
      iniAndRespSig.push(sig);
    }
  }

  // save commitments
  const finalVirtualAppSetStateCommitment = lockCommitment.transaction(
    iniAndRespSig,
    intermediariesSig
  );

  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.VirtualAppSetState,
    finalVirtualAppSetStateCommitment,
    targetAppIdentityHash
  ];

  // // because channel agreement is last step, so we need to make sure everyone is done
  // const ackMessagesTo = participantsXpub.filter(p => p !== context.me);
  // const ackMsg: ProtocolMessage = {
  //   ...context.message,
  //   toXpub: ackMessagesTo,
  //   messageType: UninstallVirtualAppState.ACK,
  //   signatures: []
  // };

  // const ackWaitingKeys = participantsXpub
  //   .filter(p => p !== context.me)
  //   .map(xpub => `${context.requestId}${xpub}${UninstallVirtualAppState.ACK}`);

  // yield [Opcode.IO_SEND, context.requestId, ackMsg];
  // yield [Opcode.IO_WAIT, ...ackWaitingKeys];
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  virtualSc: StateChannel,
  ledgerSc: StateChannel
) {
  const { targetAppIdentityHash } = context.message
    .params as UninstallVirtualAppParams;
  // alter state channel state
  const [commitment, newStateChannel] = await addUninstallAgreementToContext(
    context,
    virtualSc,
    ledgerSc
  );

  const mySig = yield [Opcode.OP_SIGN, commitment];
  // send and wait response
  yield [
    Opcode.IO_SEND,
    context.requestId,
    channelAgreementMessage(context, mySig, sortedResponding)
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${
      UninstallVirtualAppState.CHANNEL_AGREEMENT
    }`
  ]) as ProtocolMessage[];

  // validate signature
  validateSignature(
    // 0 or appSeqNo?????
    xkeyKthAddress(sortedResponding, 0),
    commitment,
    theirSig
  );

  // we can unlock this channel now
  yield [Opcode.LOCK_RELEASE, newStateChannel];

  // save commitment!
  const finalChannelAgreement = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Uninstall,
    finalChannelAgreement,
    targetAppIdentityHash
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  message: ProtocolMessage,
  sortedInitiating: string,
  virtualSc: StateChannel,
  ledgerSc: StateChannel
) {
  const {
    signatures: [theirSig]
  } = message;

  const { targetAppIdentityHash } = context.message
    .params as UninstallVirtualAppParams;
  // alter state channel state
  const [commitment, newStateChannel] = await addUninstallAgreementToContext(
    context,
    virtualSc,
    ledgerSc
  );

  // validate signature
  validateSignature(
    // 0 or appSeqNo?????
    xkeyKthAddress(sortedInitiating, 0),
    commitment,
    theirSig
  );

  // we can unlock this channel now
  yield [Opcode.LOCK_RELEASE, newStateChannel];

  const mySig = yield [Opcode.OP_SIGN, commitment];
  // send my signature
  yield [
    Opcode.IO_SEND,
    context.requestId,
    channelAgreementMessage(context, mySig, sortedInitiating)
  ];

  // save commitment!
  const finalChannelAgreement = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.ETHVirtualAppAgreement,
    finalChannelAgreement,
    targetAppIdentityHash
  ];
}

function getParticipantsXpub(context: Context) {
  const { initiatingXpub, intermediariesXpub, respondingXpub } = context.message
    .params as UninstallVirtualAppParams;

  return [initiatingXpub, ...intermediariesXpub, respondingXpub];
}

function setStateMessage(
  context: Context,
  sig: Signature,
  toXpub: string[],
  isInitiating: boolean
) {
  return {
    ...context.message,
    messageType: isInitiating
      ? UninstallVirtualAppState.INITIATE
      : UninstallVirtualAppState.SET_STATE,
    signatures: [sig],
    toXpub: toXpub,
    fromXpub: context.me // bad, fix me
  } as UninstallVirtualProtocolMessage;
}

function channelAgreementMessage(
  context: Context,
  sig: Signature,
  toXpub: string
) {
  return {
    ...context.message,
    messageType: UninstallVirtualAppState.CHANNEL_AGREEMENT,
    toXpub: [toXpub],
    fromXpub: context.me, // bad, fix me
    signatures: [sig]
  } as ProtocolMessage;
}

function addVirtualAppStateTransitionToContext(
  params: ProtocolParameters,
  context: Context,
  isIntermediary: boolean,
  virtualSc: StateChannel
): [SetStateCommitment, StateChannel] {
  const {
    targetAppIdentityHash,
    targetAppState
  } = params as UninstallVirtualAppParams;

  let newVirtualSc = virtualSc;
  if (isIntermediary) {
    newVirtualSc = virtualSc.setState(targetAppIdentityHash, targetAppState);
  }

  newVirtualSc = newVirtualSc.lockAppInstance(targetAppIdentityHash);
  const targetAppInstance = newVirtualSc.getAppInstance(targetAppIdentityHash);

  // post-expiry lock commitment
  const commitment = new SetStateCommitment(
    context.network,
    targetAppInstance.identity,
    targetAppInstance.defaultTimeout,
    targetAppInstance.hashOfLatestState,
    targetAppInstance.appSeqNo
  );

  return [commitment, newVirtualSc];
}

async function addUninstallAgreementToContext(
  context: Context,
  virtualSc: StateChannel,
  ledgerSc: StateChannel
): Promise<[UninstallCommitment, StateChannel]> {
  // uninstall agreement
  const { provider, message, network } = context;
  const {
    initiatingXpub,
    respondingXpub,
    targetAppIdentityHash
  } = message.params as UninstallVirtualAppParams;

  const increments = await computeFreeBalanceIncrements(
    virtualSc,
    targetAppIdentityHash,
    provider
  );

  const participantsXpub = getParticipantsXpub(context);
  const myIndex = participantsXpub.indexOf(context.me);
  const counterPartyIndex = participantsXpub.indexOf(
    ledgerSc.userNeuteredExtendedKeys.filter(a => a !== context.me)[0]
  );

  let leftParty: string;
  let rightParty: string;
  if (myIndex > counterPartyIndex) {
    // sc is left channel
    leftParty = participantsXpub[counterPartyIndex];
    rightParty = participantsXpub[myIndex];
  } else {
    // sc is right channel
    leftParty = participantsXpub[myIndex];
    rightParty = participantsXpub[counterPartyIndex];
  }

  const agreementInstance = ledgerSc.getETHVirtualAppAgreementInstanceFromTarget(
    targetAppIdentityHash
  );

  const newStateChannel = ledgerSc.uninstallETHVirtualAppAgreementInstance(
    targetAppIdentityHash,
    {
      [leftParty]: increments[xkeyKthAddress(initiatingXpub, 0)],
      [rightParty]: increments[xkeyKthAddress(respondingXpub, 0)]
    }
  );

  const freeBalance = newStateChannel.getFreeBalanceFor(AssetType.ETH);

  const commitment = new UninstallCommitment(
    network,
    newStateChannel.multisigAddress,
    newStateChannel.multisigOwners,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.state as ETHBucketAppState,
    freeBalance.nonce,
    freeBalance.timeout,
    agreementInstance.appSeqNo
  );

  return [commitment, newStateChannel];
}
