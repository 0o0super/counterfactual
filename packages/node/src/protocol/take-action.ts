import { Signature } from "ethers/utils";

import { SetStateCommitment } from "../ethereum";
import { CommitmentType, Opcode, TakeActionState } from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  TakeActionParams,
  Transaction
} from "../machine/types";
import { xkeyKthAddress } from "../machine/xkeys";
import { StateChannel } from "../models/state-channel";

import { validateSignature } from "./utils/signature-validator";

type TakeActionProtocolMessage = ProtocolMessage & { params: TakeActionParams };

/**
 * @description This exchange is described at the following URL:
 *
 * TODO:
 *
 */
export const TAKE_ACTION_PROTOCOL = async function*(context: Context) {
  const {
    multisigAddress,
    initiatingXpub,
    respondingXpub
    // action,
    // appIdentityHash
  } = context.message.params as TakeActionParams;

  // // check state valid
  // const app = await sc.getAppInstance(appIdentityHash);
  // await app.computeStateTransition(action, context.provider);

  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${TakeActionState.AGREEMENT}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const { appIdentityHash } = message.params as TakeActionParams;

  const [commitment, newSC] = await addStateTransitionAndCommitmentToContext(
    context,
    message as TakeActionProtocolMessage,
    sc
  );
  const { appSeqNo, isVirtualApp } = newSC.getAppInstance(appIdentityHash);

  const mySig = yield [Opcode.OP_SIGN, commitment, appSeqNo];
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: TakeActionState.AGREEMENT,
      toXpub: [sortedResponding],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${TakeActionState.AGREEMENT}`
  ]) as ProtocolMessage[];

  validateSignature(
    xkeyKthAddress(sortedResponding, appSeqNo),
    commitment,
    theirSig
  );

  // save channel and release
  yield [Opcode.LOCK_RELEASE, newSC];

  let finalCommitment: Transaction;
  if (isVirtualApp) {
    // get intermediaries signature from install-virtual!
    const sigs: Map<string, Signature> = yield [
      [[Opcode.OP_GET_INTERMEDIARY_SIG, appIdentityHash]]
    ];
    finalCommitment = commitment.transaction(
      [mySig, theirSig],
      [...sigs.values()]
    );
  } else {
    finalCommitment = commitment.transaction([mySig, theirSig]);
  }

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.SetState,
    finalCommitment,
    appIdentityHash
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  // construct commitment and validate message from counterparty
  const { appIdentityHash } = message.params as TakeActionParams;

  const [commitment, newSC] = await addStateTransitionAndCommitmentToContext(
    context,
    message as TakeActionProtocolMessage,
    sc
  );
  const { appSeqNo, isVirtualApp } = newSC.getAppInstance(appIdentityHash);

  // validate message from initiating
  const {
    signatures: [theirSig]
  } = message;
  validateSignature(
    xkeyKthAddress(sortedInitiating, appSeqNo),
    commitment,
    theirSig
  );

  // release and save state-channel
  yield [Opcode.LOCK_RELEASE, newSC];

  const mySig = yield [Opcode.OP_SIGN, commitment, appSeqNo];

  // send mysig to counterparty!
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: TakeActionState.AGREEMENT,
      toXpub: [sortedInitiating],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  let finalCommitment: Transaction;
  if (isVirtualApp) {
    // get intermediaries signature from install-virtual!
    const sigs: Map<string, Signature> = yield [
      Opcode.OP_GET_INTERMEDIARY_SIG,
      appIdentityHash
    ];
    finalCommitment = commitment.transaction(
      [mySig, theirSig],
      [...sigs.values()]
    );
  } else {
    finalCommitment = commitment.transaction([mySig, theirSig]);
  }

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.SetState,
    finalCommitment,
    appIdentityHash
  ];
}

async function addStateTransitionAndCommitmentToContext(
  context: Context,
  message: TakeActionProtocolMessage,
  sc: StateChannel
): Promise<[SetStateCommitment, StateChannel]> {
  const { network, provider } = context;
  const { appIdentityHash, action } = message.params;

  const appInstance = sc.getAppInstance(appIdentityHash);

  const newChannel = sc.setState(
    appIdentityHash,
    await appInstance.computeStateTransition(action, provider)
  );

  const updatedAppInstance = newChannel.getAppInstance(appIdentityHash);

  const setStateCommitment = new SetStateCommitment(
    network,
    updatedAppInstance.identity,
    updatedAppInstance.timeout,
    updatedAppInstance.hashOfLatestState,
    updatedAppInstance.nonce
  );

  return [setStateCommitment, newChannel];
}
