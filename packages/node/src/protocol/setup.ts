import { AssetType, NetworkContext } from "@counterfactual/types";

import { SetupCommitment } from "../ethereum";
import { CommitmentType, Opcode, SetupState } from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  ProtocolParameters,
  SetupParams
} from "../machine/types";
import { xkeyKthAddress } from "../machine/xkeys";
import { StateChannel } from "../models/state-channel";

import { validateSignature } from "./utils/signature-validator";

export const SETUP_PROTOCOL = async function*(context: Context) {
  const { initiatingXpub, respondingXpub } = context.message
    .params as SetupParams;

  // will "not locking new channel" cause problem?
  const [setupCommitment, newSC] = proposeStateTransition(
    context.message.params,
    context
  );

  switch (context.me) {
    case initiatingXpub:
      yield* initiatingSteps(context, setupCommitment);
      break;
    case respondingXpub:
      yield* respondingSteps(context, setupCommitment);
      break;
    default:
      throw Error("Wrong participants in install");
  }

  yield [Opcode.LOCK_RELEASE, newSC];
};

async function* initiatingSteps(context: Context, commitment: SetupCommitment) {
  const { respondingXpub, multisigAddress } = context.message
    .params as SetupParams;
  const mySig = yield [Opcode.OP_SIGN, commitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: SetupState.INITIATE,
      toXpub: [respondingXpub],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${respondingXpub}${SetupState.AGREEMENT}`
  ]) as ProtocolMessage[];

  validateSignature(xkeyKthAddress(respondingXpub, 0), commitment, theirSig);

  const finalCommitment = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Setup,
    finalCommitment,
    multisigAddress
  ];
}

async function* respondingSteps(context: Context, commitment: SetupCommitment) {
  const { initiatingXpub, multisigAddress } = context.message
    .params as SetupParams;

  const {
    signatures: [theirSig]
  } = context.message;
  validateSignature(xkeyKthAddress(initiatingXpub, 0), commitment, theirSig);
  const mySig = yield [Opcode.OP_SIGN, commitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: SetupState.AGREEMENT,
      toXpub: [initiatingXpub],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const finalCommitment = commitment.transaction([mySig, theirSig]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Setup,
    finalCommitment,
    multisigAddress
  ];
}

function proposeStateTransition(
  params: ProtocolParameters,
  context: Context
): [SetupCommitment, StateChannel] {
  const {
    multisigAddress,
    initiatingXpub,
    respondingXpub
  } = params as SetupParams;

  // FIX MEEEEE
  // if (context.stateChannelsMap.has(multisigAddress)) {
  //   throw Error(`Found an already-setup channel at ${multisigAddress}`);
  // }

  const newStateChannel = StateChannel.setupChannel(
    context.network.ETHBucket,
    multisigAddress,
    [initiatingXpub, respondingXpub]
  );

  const setupCommitment = constructSetupCommitment(
    context.network,
    newStateChannel
  );

  return [setupCommitment, newStateChannel];
}

export function constructSetupCommitment(
  network: NetworkContext,
  stateChannel: StateChannel
) {
  const freeBalance = stateChannel.getFreeBalanceFor(AssetType.ETH);

  return new SetupCommitment(
    network,
    stateChannel.multisigAddress,
    stateChannel.multisigOwners,
    freeBalance.identity,
    freeBalance.terms
  );
}
