import { AssetType, ETHBucketAppState } from "@counterfactual/types";

import { UninstallCommitment } from "../ethereum";
import { CommitmentType, Opcode, UninstallState } from "../machine/enums";
import {
  Context,
  ProtocolMessage,
  ProtocolParameters,
  UninstallParams
} from "../machine/types";
import { xkeyKthAddress } from "../machine/xkeys";
import { StateChannel } from "../models";

import {
  computeFreeBalanceIncrements,
  getAliceBobMap
} from "./utils/get-resolution-increments";
import { validateSignature } from "./utils/signature-validator";

/**
 * @description This exchange is described at the following URL:
 *
 * specs.counterfactual.com/06-uninstall-protocol#messages
 */

export const UNINSTALL_PROTOCOL = async function*(context: Context) {
  const { multisigAddress, initiatingXpub, respondingXpub } = context.message
    .params as UninstallParams;

  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${UninstallState.AGREEMENT}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const [appIdentityHash, commitment, newSC] = await proposeStateTransition(
    message.params,
    context,
    sc
  );
  const mySig = yield [Opcode.OP_SIGN, commitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: UninstallState.AGREEMENT,
      toXpub: [sortedResponding],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${UninstallState.AGREEMENT}`
  ]) as ProtocolMessage[];

  validateSignature(xkeyKthAddress(sortedResponding, 0), commitment, theirSig);

  // save channel and release
  yield [Opcode.LOCK_RELEASE, newSC];

  const finalCommitment = commitment.transaction([mySig, theirSig]);

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Uninstall,
    finalCommitment,
    appIdentityHash
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  // construct commitment and validate message from counterparty
  const [appIdentityHash, commitment, newSC] = await proposeStateTransition(
    message.params,
    context,
    sc
  );

  // validate message from initiating
  const {
    signatures: [theirSig]
  } = message;
  validateSignature(xkeyKthAddress(sortedInitiating, 0), commitment, theirSig);

  // release and save state-channel
  yield [Opcode.LOCK_RELEASE, newSC];

  const mySig = yield [Opcode.OP_SIGN, commitment];

  // send mysig to counterparty!
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: UninstallState.AGREEMENT,
      toXpub: [sortedInitiating],
      signatures: [mySig]
    } as ProtocolMessage
  ];
  const finalCommitment = commitment.transaction([mySig, theirSig]);

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Uninstall,
    finalCommitment,
    appIdentityHash
  ];
}

async function proposeStateTransition(
  params: ProtocolParameters,
  context: Context,
  sc: StateChannel
): Promise<[string, UninstallCommitment, StateChannel]> {
  const { appIdentityHash } = params as UninstallParams;
  const { network, provider } = context;

  const sequenceNo = sc.getAppInstance(appIdentityHash).appSeqNo;

  const increments = await computeFreeBalanceIncrements(
    sc,
    appIdentityHash,
    provider
  );

  const aliceBobMap = getAliceBobMap(sc);

  const newStateChannel = sc.uninstallApp(
    appIdentityHash,
    increments[aliceBobMap.alice],
    increments[aliceBobMap.bob]
  );

  const freeBalance = newStateChannel.getFreeBalanceFor(AssetType.ETH);

  const uninstallCommitment = new UninstallCommitment(
    network,
    newStateChannel.multisigAddress,
    newStateChannel.multisigOwners,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.state as ETHBucketAppState,
    freeBalance.nonce,
    freeBalance.timeout,
    sequenceNo
  );

  return [appIdentityHash, uninstallCommitment, newStateChannel];
}
