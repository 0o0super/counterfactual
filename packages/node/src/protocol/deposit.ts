import {
  AssetType,
  ETHBucketAppState,
  NetworkContext
} from "@counterfactual/types";
import { Signer } from "ethers";
import { AddressZero, Zero } from "ethers/constants";
import { TransactionRequest, TransactionResponse } from "ethers/providers";
import { bigNumberify } from "ethers/utils";

import { InstallCommitment, UninstallCommitment } from "../ethereum";
import { xkeyKthAddress } from "../machine";
import { CommitmentType, DepositState, Opcode } from "../machine/enums";
import {
  Context,
  DepositParams,
  ProtocolMessage,
  ProtocolParameters
} from "../machine/types";
import { AppInstance, StateChannel } from "../models";

import { getAliceBobMap } from "./utils/get-resolution-increments";
import { validateSignature } from "./utils/signature-validator";

/**
 * @description This exchange is described at the following URL:
 * https://specs.counterfactual.com/11-withdraw-protocol *
 */

export const DEPOSIT_PROTOCOL = async function*(context: Context) {
  const { multisigAddress, initiatingXpub, respondingXpub } = context.message
    .params as DepositParams;

  // first acquire statechannel
  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${DepositState.INSTALL_REFUND}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const { initiatingXpub } = context.message.params as DepositParams;
  const sortedRespondingAddress = xkeyKthAddress(sortedResponding, 0);

  const [
    installRefundCommitment,
    newSC,
    refundAppIdentityHash
  ] = addInstallRefundAppCommitmentToContext(
    context.message.params,
    context,
    sc
  );

  const s1 = yield [Opcode.OP_SIGN, installRefundCommitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: DepositState.INSTALL_REFUND,
      toXpub: [sortedResponding],
      signatures: [s1]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [s2, s4]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${DepositState.INSTALL_REFUND}`
  ]) as ProtocolMessage[];

  validateSignature(sortedRespondingAddress, installRefundCommitment, s2);
  const finalInstallCommitment = installRefundCommitment.transaction([s1, s2]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Install,
    finalInstallCommitment,
    refundAppIdentityHash
  ];

  if (context.me === initiatingXpub) {
    yield* deposit(context.message.params, context);
  }

  // counterparty will send uninstall!
  const [
    uninstallRefundCommitment,
    newSC2
  ] = await addUninstallRefundAppCommitmentToContext(
    context,
    newSC,
    refundAppIdentityHash
  );
  validateSignature(sortedRespondingAddress, uninstallRefundCommitment, s4);

  // release state-channel
  yield [Opcode.LOCK_RELEASE, newSC2];

  const s5 = yield [Opcode.OP_SIGN, uninstallRefundCommitment];
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: DepositState.UNINSTALL_REFUND,
      toXpub: [sortedResponding],
      signatures: [s5]
    } as ProtocolMessage
  ];

  const finalUninstallCommitment = uninstallRefundCommitment.transaction([
    s5,
    s4
  ]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Uninstall,
    finalUninstallCommitment,
    refundAppIdentityHash
  ];
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  const { initiatingXpub } = context.message.params as DepositParams;
  const sortedInitiatingAddress = xkeyKthAddress(sortedInitiating, 0);

  const [
    installRefundCommitment,
    newSC,
    refundAppIdentityHash
  ] = addInstallRefundAppCommitmentToContext(
    context.message.params,
    context,
    sc
  );
  const {
    signatures: [s1]
  } = message;

  validateSignature(sortedInitiatingAddress, installRefundCommitment, s1);
  const s2 = yield [Opcode.OP_SIGN, installRefundCommitment];
  const finalInstallCommitment = installRefundCommitment.transaction([s1, s2]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Install,
    finalInstallCommitment,
    refundAppIdentityHash
  ];

  if (context.me === initiatingXpub) {
    yield* deposit(context.message.params, context);
  }

  const [
    uninstallRefundCommitment,
    newSC2
  ] = await addUninstallRefundAppCommitmentToContext(
    context,
    newSC,
    refundAppIdentityHash
  );

  const s4 = yield [Opcode.OP_SIGN, uninstallRefundCommitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...context.message,
      messageType: DepositState.INSTALL_REFUND,
      toXpub: [sortedInitiating],
      signatures: [s2, s4]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [s5]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedInitiating}${DepositState.UNINSTALL_REFUND}`
  ]) as ProtocolMessage[];

  validateSignature(sortedInitiatingAddress, uninstallRefundCommitment, s5);

  // unlock channel
  yield [Opcode.LOCK_RELEASE, newSC2];

  // save commitment
  const finalUninstallCommitment = uninstallRefundCommitment.transaction([
    s5,
    s4
  ]);
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Uninstall,
    finalUninstallCommitment,
    refundAppIdentityHash
  ];
}

async function* deposit(params: ProtocolParameters, context: Context) {
  const { amount, multisigAddress } = params as DepositParams;
  const { provider } = context;

  const tx: TransactionRequest = {
    to: multisigAddress,
    value: bigNumberify(amount),
    gasLimit: 30000,
    gasPrice: await provider.getGasPrice()
  };

  const singer: Signer = yield [Opcode.OP_GET_SINGER];

  let txResponse: TransactionResponse;

  let retryCount = 3;
  while (retryCount > 0) {
    try {
      txResponse = await singer.sendTransaction(tx);
      break;
    } catch (e) {
      if (e.toString().includes("reject") || e.toString().includes("denied")) {
        console.error(`${e}`);
      }

      retryCount -= 1;

      if (retryCount === 0) {
        throw new Error(`${e}`);
      }
    }
  }

  await txResponse!.wait(1);
}

function addInstallRefundAppCommitmentToContext(
  params: ProtocolParameters,
  context: Context,
  sc: StateChannel
): [InstallCommitment, StateChannel, string] {
  const { recipient, amount, multisigAddress } = params as DepositParams;

  const appInstance = new AppInstance(
    multisigAddress,
    sc.getNextSigningKeys(),
    1008,
    {
      addr: context.network.ETHBalanceRefundApp,
      stateEncoding:
        "tuple(address recipient, address multisig,  uint256 threshold)",
      actionEncoding: undefined
    },
    {
      assetType: AssetType.ETH,
      limit: amount,
      token: AddressZero
    },
    false,
    sc.numInstalledApps,
    sc.rootNonceValue,
    {
      recipient,
      multisig: multisigAddress,
      threshold: amount
    },
    0,
    1008
  );

  const newStateChannel = sc.installApp(appInstance, Zero, Zero);

  const installRefundCommitment = constructInstallOp(
    context.network,
    newStateChannel,
    appInstance.identityHash
  );

  return [installRefundCommitment, newStateChannel, appInstance.identityHash];
}

async function addUninstallRefundAppCommitmentToContext(
  context: Context,
  sc: StateChannel,
  appIdentityHash: string
): Promise<[UninstallCommitment, StateChannel]> {
  const { initiatingXpub, respondingXpub, amount } = context.message
    .params as DepositParams;

  const increments = {
    [xkeyKthAddress(initiatingXpub, 0)]: amount,
    [xkeyKthAddress(respondingXpub, 0)]: Zero
  };

  const aliceBobMap = getAliceBobMap(sc);

  const newStateChannel = sc.uninstallApp(
    appIdentityHash,
    increments[aliceBobMap.alice],
    increments[aliceBobMap.bob]
  );

  const freeBalance = newStateChannel.getFreeBalanceFor(AssetType.ETH);

  const uninstallCommitment = new UninstallCommitment(
    context.network,
    newStateChannel.multisigAddress,
    newStateChannel.multisigOwners,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.state as ETHBucketAppState,
    freeBalance.nonce,
    freeBalance.timeout,
    freeBalance.appSeqNo
  );

  return [uninstallCommitment, newStateChannel];
}

function constructInstallOp(
  network: NetworkContext,
  stateChannel: StateChannel,
  appIdentityHash: string
) {
  const app = stateChannel.getAppInstance(appIdentityHash);

  const freeBalance = stateChannel.getFreeBalanceFor(AssetType.ETH);

  return new InstallCommitment(
    network,
    stateChannel.multisigAddress,
    stateChannel.multisigOwners,
    app.identity,
    app.terms,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.hashOfLatestState,
    freeBalance.nonce,
    freeBalance.timeout,
    app.appSeqNo,
    freeBalance.rootNonceValue
  );
}
