import { AssetType, NetworkContext } from "@counterfactual/types";

import { InstallCommitment } from "../ethereum";
import { CommitmentType, InstallState, Opcode } from "../machine/enums";
import {
  Context,
  InstallParams,
  ProtocolMessage,
  ProtocolParameters
} from "../machine/types";
import { xkeyKthAddress } from "../machine/xkeys";
import { AppInstance, StateChannel } from "../models";

import { validateSignature } from "./utils/signature-validator";

/**
 * @description This exchange is described at the following URL:
 *
 * specs.counterfactual.com/05-install-protocol#messages
 */

export const INSTALL_PROTOCOL = async function*(context: Context) {
  const { multisigAddress, initiatingXpub, respondingXpub } = context.message
    .params as InstallParams;

  // if I am initiating, send message to counterparty
  if (context.me === initiatingXpub) {
    yield [
      Opcode.IO_SEND,
      context.requestId,
      { ...context.message, toXpub: [respondingXpub] } as ProtocolMessage
    ];
  }

  const counterparty =
    context.me === initiatingXpub ? respondingXpub : initiatingXpub;
  // first acquire statechannel
  const [sc, message]: [StateChannel, ProtocolMessage] = yield [
    Opcode.LOCK_ACQUIRE,
    `${context.requestId}${counterparty}${InstallState.AGREEMENT}`,
    context.message,
    multisigAddress
  ];
  const sortedInitiating =
    sc.userNeuteredExtendedKeys[sc.numOperationExecuted % 2];
  const sortedResponding =
    sc.userNeuteredExtendedKeys[(sc.numOperationExecuted + 1) % 2];

  switch (context.me) {
    case sortedInitiating:
      yield* sortedInitiatingSteps(context, sortedResponding, message, sc);
      break;
    case sortedResponding:
      yield* sortedRespondingSteps(context, sortedInitiating, message, sc);
      break;
    default:
      throw Error("Wrong participants in install");
  }
};

async function* sortedInitiatingSteps(
  context: Context,
  sortedResponding: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  if (message.signatures && message.signatures[0]) {
    // problem in execution flow!
    throw Error("Execution flow error in install");
  }

  const [appIdentityHash, commitment, newSC] = proposeStateTransition(
    message.params,
    context.network,
    sc
  );
  const mySig = yield [Opcode.OP_SIGN, commitment];

  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: InstallState.AGREEMENT,
      toXpub: [sortedResponding],
      signatures: [mySig]
    } as ProtocolMessage
  ];

  const [
    {
      signatures: [theirSig]
    }
  ] = (yield [
    Opcode.IO_WAIT,
    `${context.requestId}${sortedResponding}${InstallState.AGREEMENT}`
  ]) as ProtocolMessage[];

  validateSignature(xkeyKthAddress(sortedResponding, 0), commitment, theirSig);

  // save channel and release
  yield [Opcode.LOCK_RELEASE, newSC];

  const finalCommitment = commitment.transaction([mySig, theirSig]);

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Install,
    finalCommitment,
    appIdentityHash
  ];

  // save mapping
  yield [
    Opcode.OP_MAP_REALISED_ID_HASH,
    (context.message.params as InstallParams).appIdentityHash,
    appIdentityHash
  ];

  const { respondingXpub } = message.params as InstallParams;
  if (context.me === respondingXpub) {
    // send extra ack message!
    yield [
      Opcode.IO_SEND,
      context.requestId,
      {
        ...message,
        messageType: 2,
        toXpub: [sortedResponding],
        signatures: []
      } as ProtocolMessage
    ];
  }
}

async function* sortedRespondingSteps(
  context: Context,
  sortedInitiating: string,
  message: ProtocolMessage,
  sc: StateChannel
) {
  // construct commitment and validate message from counterparty
  const [appIdentityHash, commitment, newSC] = proposeStateTransition(
    message.params,
    context.network,
    sc
  );

  // validate message from initiating
  const {
    signatures: [theirSig]
  } = message;
  validateSignature(xkeyKthAddress(sortedInitiating, 0), commitment, theirSig);

  // release and save state-channel
  yield [Opcode.LOCK_RELEASE, newSC];

  const mySig = yield [Opcode.OP_SIGN, commitment];

  // send mysig to counterparty!
  yield [
    Opcode.IO_SEND,
    context.requestId,
    {
      ...message,
      messageType: InstallState.AGREEMENT,
      toXpub: [sortedInitiating],
      signatures: [mySig]
    } as ProtocolMessage
  ];
  const finalCommitment = commitment.transaction([mySig, theirSig]);

  // save commitment
  yield [
    Opcode.WRITE_COMMITMENT,
    CommitmentType.Install,
    finalCommitment,
    appIdentityHash
  ];

  // save mapping
  yield [
    Opcode.OP_MAP_REALISED_ID_HASH,
    (context.message.params as InstallParams).appIdentityHash,
    appIdentityHash
  ];

  const { initiatingXpub } = message.params as InstallParams;
  if (context.me === initiatingXpub) {
    // wait extra ack message!
    yield [Opcode.IO_WAIT, `${context.requestId}${sortedInitiating}2`];
  }
}

function proposeStateTransition(
  params: ProtocolParameters,
  network: NetworkContext,
  sc: StateChannel
): [string, InstallCommitment, StateChannel] {
  const {
    aliceBalanceDecrement,
    bobBalanceDecrement,
    initialState,
    terms,
    appInterface,
    defaultTimeout,
    multisigAddress
  } = params as InstallParams;

  const appInstance = new AppInstance(
    multisigAddress,
    sc.getNextSigningKeys(),
    defaultTimeout,
    appInterface,
    terms,
    // KEY: Sets it to NOT be a virtual app
    false,
    // KEY: The app sequence number
    sc.numInstalledApps,
    sc.rootNonceValue,
    initialState,
    // KEY: Set the nonce to be 0
    0,
    defaultTimeout
  );

  const newStateChannel = sc.installApp(
    appInstance,
    aliceBalanceDecrement,
    bobBalanceDecrement
  );

  const appIdentityHash = appInstance.identityHash;

  const commitment = constructInstallOp(
    network,
    newStateChannel,
    appIdentityHash
  );

  return [appIdentityHash, commitment, newStateChannel];
}

function constructInstallOp(
  network: NetworkContext,
  stateChannel: StateChannel,
  appIdentityHash: string
) {
  const app = stateChannel.getAppInstance(appIdentityHash);

  const freeBalance = stateChannel.getFreeBalanceFor(AssetType.ETH);

  return new InstallCommitment(
    network,
    stateChannel.multisigAddress,
    stateChannel.multisigOwners,
    app.identity,
    app.terms,
    freeBalance.identity,
    freeBalance.terms,
    freeBalance.hashOfLatestState,
    freeBalance.nonce,
    freeBalance.timeout,
    app.appSeqNo,
    freeBalance.rootNonceValue
  );
}
