import { NetworkContext } from "@counterfactual/types";
import { BaseProvider } from "ethers/providers";

import { getProtocolFromName } from "../protocol";

import {
  DepositState,
  InstallState,
  InstallVirtualAppState,
  Opcode,
  Protocol,
  SetupState,
  TakeActionState,
  UninstallState,
  UninstallVirtualAppState,
  UpdateState,
  WithdrawState
} from "./enums";
import { MiddlewareContainer } from "./middleware";
import {
  CancelDisputeParams,
  Context,
  DepositParams,
  InstallParams,
  InstallVirtualAppParams,
  Middleware,
  ProtocolMessage,
  SetupParams,
  TakeActionParams,
  UninstallParams,
  UninstallVirtualAppParams,
  UpdateParams,
  WithdrawParams
} from "./types";

export class InstructionExecutor {
  public middlewares: MiddlewareContainer;

  constructor(
    public readonly network: NetworkContext,
    public readonly provider: BaseProvider,
    public readonly me: string
  ) {
    this.middlewares = new MiddlewareContainer();
  }

  public register(scope: Opcode, method: Middleware) {
    this.middlewares.add(scope, method);
  }

  /// Starts executing a protocol in response to a message received. This
  /// function should not be called with messages that are waited for by
  /// `IO_SEND_AND_WAIT`
  public async runProtocolWithMessage(msg: ProtocolMessage, requestId: string) {
    const protocol = getProtocolFromName(msg.protocol);
    const step = protocol;
    if (step === undefined) {
      throw Error(
        `Received invalid type ${msg.messageType} for protocol ${msg.protocol}`
      );
    }
    return this.runProtocol(step, msg, requestId);
  }

  public async runUpdateProtocol(params: UpdateParams, requestId: string) {
    const protocol = Protocol.Update;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: UpdateState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runCancelDisputeProtocol(
    params: CancelDisputeParams,
    requestId: string
  ) {
    const protocol = Protocol.CancelDispute;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: UpdateState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runTakeActionProtocol(
    params: TakeActionParams,
    requestId: string
  ) {
    const protocol = Protocol.TakeAction;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: TakeActionState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runUninstallProtocol(
    params: UninstallParams,
    requestId: string
  ) {
    const protocol = Protocol.Uninstall;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: UninstallState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runInstallProtocol(params: InstallParams, requestId: string) {
    const protocol = Protocol.Install;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: InstallState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runSetupProtocol(params: SetupParams, requestId: string) {
    const protocol = Protocol.Setup;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        protocol,
        params,
        messageType: SetupState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runDepositProtocol(params: DepositParams, requestId: string) {
    const protocol = Protocol.Deposit;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        protocol,
        params,
        messageType: DepositState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runWithdrawProtocol(params: WithdrawParams, requestId: string) {
    const protocol = Protocol.Withdraw;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        protocol,
        params,
        messageType: WithdrawState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.respondingXpub],
        signatures: []
      },
      requestId
    );
  }

  public async runInstallVirtualAppProtocol(
    params: InstallVirtualAppParams,
    requestId: string
  ) {
    const protocol = Protocol.InstallVirtualApp;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: InstallVirtualAppState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.intermediariesXpub[0]],
        signatures: []
      },
      requestId
    );
  }

  public async runUninstallVirtualAppProtocol(
    params: UninstallVirtualAppParams,
    requestId: string
  ) {
    const protocol = Protocol.UninstallVirtualApp;
    return this.runProtocol(
      getProtocolFromName(protocol),
      {
        params,
        protocol,
        messageType: UninstallVirtualAppState.INITIATE,
        fromXpub: params.initiatingXpub,
        toXpub: [params.intermediariesXpub[0]],
        signatures: []
      },
      requestId
    );
  }

  private async runProtocol(
    instruction: (context: Context) => AsyncIterableIterator<any>,
    message: ProtocolMessage,
    requestId: string
  ) {
    const context: Context = {
      message,
      requestId,
      network: this.network,
      provider: this.provider,
      me: this.me
    };

    let lastMiddlewareRet: any = undefined;
    const it = instruction(context);
    while (true) {
      const ret = await it.next(lastMiddlewareRet);
      if (ret.done) {
        return ret.value;
      }
      if (ret.value[0] instanceof Array) {
        // two dimentional array, intend to return pending promise
        const middlewareRequest: any[] = ret.value;
        lastMiddlewareRet = middlewareRequest.map(op => {
          const [opcode, ...args] = op;
          return this.middlewares.run(opcode, args);
        });
      } else {
        // one dimentional array, intend to await and return result
        const [opcode, ...args] = ret.value;
        lastMiddlewareRet = await this.middlewares.run(opcode, args);
      }

      // const [opcode, ...args] = ret.value;
      // lastMiddlewareRet = await this.middlewares.run(opcode, args);
    }
  }
}
