import { Opcode } from "./enums";
import { Middleware } from "./types";

export class MiddlewareContainer {
  public readonly middlewares: { [I in Opcode]: Middleware[] } = {
    [Opcode.IO_SEND]: [],
    // [Opcode.IO_SEND_AND_WAIT]: [],
    [Opcode.OP_SIGN]: [],
    [Opcode.OP_SIGN_AS_INTERMEDIARY]: [],
    [Opcode.WRITE_COMMITMENT]: [],
    [Opcode.LOCK_ACQUIRE]: [],
    [Opcode.LOCK_RELEASE]: [],
    [Opcode.IO_WAIT]: [],
    [Opcode.OP_GET_INTERMEDIARY_SIG]: [],
    [Opcode.OP_MAP_REALISED_ID_HASH]: [],
    [Opcode.OP_GET_SINGER]: []
  };

  public add(scope: Opcode, method: Middleware) {
    this.middlewares[scope].push(method);
  }

  public async run(opCode: Opcode, args: any[]) {
    const middleware = this.middlewares[opCode][0];

    if (middleware === undefined) {
      throw Error(
        `Attempted to run middleware for opcode ${opCode} but none existed`
      );
    }

    return middleware(args);
  }
}
