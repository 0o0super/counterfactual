export enum Opcode {
  /**
   * Called at the end of execution before the return value to store a commitment
   */
  WRITE_COMMITMENT,

  /**
   * Requests a signature on the hash of previously generated EthereumCommitments.
   */
  OP_SIGN,

  /**
   * Same as above, but pass isIntermediary=true to EthereumCommitments.hashToSign
   * todo(xuanji): think of a better design to get rid of this
   */
  OP_SIGN_AS_INTERMEDIARY,

  /**
   * Virtual app set-state commitment need intermediary signature from install
   */
  OP_GET_INTERMEDIARY_SIG,

  /**
   * GET THE SIGNER
   */
  OP_GET_SINGER,

  /**
   * Proposed instance have different identity hash from installed one
   */
  OP_MAP_REALISED_ID_HASH,

  /**
   * Middleware hook to send a ProtocolMessage to a peer.
   */
  IO_SEND,

  /**
   * Middleware hook to send wait a message.
   */
  IO_WAIT,

  // /**
  //  * Middleware hook to both send and wait for a response from a ProtocolMessage
  //  */
  // IO_SEND_AND_WAIT,

  /**
   * Middleware hook acquire state-channel lock
   */
  LOCK_ACQUIRE,

  /**
   * Middleware hook release state-channel lock
   */
  LOCK_RELEASE
}

export enum Protocol {
  Install = "install",
  InstallVirtualApp = "install-virtual-app",
  Setup = "setup",
  TakeAction = "takeAction",
  Uninstall = "uninstall",
  UninstallVirtualApp = "uninstall-virtual-app",
  Update = "update",
  Withdraw = "withdraw",
  Deposit = "deposit",
  CancelDispute = "cancelDispute"
}

export enum CommitmentType {
  CancelDispute = "cancelDisputeCommitment",
  ETHVirtualAppAgreement = "ethVirtualAppAgreementCommitment",
  Install = "installCommitment",
  SetState = "setStateCommitment",
  Setup = "setupCommitment",
  Uninstall = "uninstallCommitment",
  VirtualAppSetState = "virtualAppSetStateCommitment",
  WithdrawETH = "withdrawETHCommitment"
}

export enum ProtocolExecutionState {
  INACTIVE,
  ACTIVE,
  WAITING,
  RETRYING
}

export enum SetupState {
  INITIATE,
  AGREEMENT
}

export enum UpdateState {
  INITIATE,
  AGREEMENT
}

export enum TakeActionState {
  INITIATE,
  AGREEMENT
}

export enum WithdrawState {
  INITIATE,
  INSTALL_WITHDRAW,
  UNINSTALL_WITHDRAW
}

export enum DepositState {
  INITIATE,
  INSTALL_REFUND,
  UNINSTALL_REFUND
}

export enum InstallState {
  INITIATE,
  AGREEMENT
}

export enum UninstallState {
  INITIATE,
  AGREEMENT
}

export enum CancelDisputeState {
  INITIATE,
  AGREEMENT
}

export enum InstallVirtualAppState {
  INITIATE,
  CHANNEL_AGREEMENT,
  SET_STATE
}

export enum UninstallVirtualAppState {
  INITIATE,
  CHANNEL_AGREEMENT,
  SET_STATE
}

export type ProtocolState =
  | SetupState
  | UpdateState
  | WithdrawState
  | DepositState
  | TakeActionState
  | InstallState
  | UninstallState
  | InstallVirtualAppState
  | UninstallVirtualAppState
  | CancelDisputeState;
