import { Node } from "@counterfactual/types";

import { RequestHandler } from "../request-handler";

export abstract class NodeStaticMethodController {
  public static readonly methodName: Node.MethodName;

  // public async execute(
  //   requestHandler: RequestHandler,
  //   params: Node.MethodParams
  // ) {
  //   const allChannels = await requestHandler.store.getAllChannels();
  //   const queues = [...Object.keys(allChannels)].map(addr =>
  //     requestHandler.getShardedQueue(addr)
  //   );

  //   let promise: any;
  //   let ret: Node.MethodResult = {};

  //   const runPromise = async () => {
  //     if (!promise) {
  //       promise = this.executeMethod(requestHandler, params);
  //     }
  //     return await promise;
  //   };

  //   for (const q of queues) ret = await q.add(runPromise);
  //   return ret;
  // }

  public abstract async execute(
    requestHandler: RequestHandler,
    params: Node.MethodParams
  ): Promise<Node.MethodResult>;
}
