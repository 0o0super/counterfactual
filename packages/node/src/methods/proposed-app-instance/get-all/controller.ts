import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { NodeStaticMethodController } from "../../static-method-controller";

export default class GetProposedAppInstancesController extends NodeStaticMethodController {
  public static readonly methodName =
    Node.MethodName.GET_PROPOSED_APP_INSTANCES;

  public async execute(
    requestHandler: RequestHandler,
    params: Node.MethodParams
  ): Promise<Node.GetProposedAppInstancesResult> {
    return {
      appInstances: await requestHandler.store.getProposedAppInstances()
    };
  }
}
