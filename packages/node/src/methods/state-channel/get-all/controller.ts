import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { NodeStaticMethodController } from "../../static-method-controller";

export default class GetAllChannelAddressesController extends NodeStaticMethodController {
  public static readonly methodName = Node.MethodName.GET_CHANNEL_ADDRESSES;

  public async execute(
    requestHandler: RequestHandler,
    params: Node.MethodParams
  ): Promise<Node.GetChannelAddressesResult> {
    return {
      multisigAddresses: Object.keys(
        await requestHandler.store.getAllChannels()
      )
    };
  }
}
