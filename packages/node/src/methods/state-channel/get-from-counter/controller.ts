import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { NodeStaticMethodController } from "../../static-method-controller";

export default class GetChannelAddressFromCounterController extends NodeStaticMethodController {
  public static readonly methodName =
    Node.MethodName.GET_CHANNEL_ADDRESS_FROM_COUNTER;

  public async execute(
    requestHandler: RequestHandler,
    params: Node.GetChannelAddressFromCounterParams
  ): Promise<Node.GetChannelAddressFromCounterResult> {
    const allChannels = await requestHandler.store.getAllChannels();
    const targetChannel = [...Object.values(allChannels)].filter(sc => {
      return (
        sc.userNeuteredExtendedKeys.length === 2 &&
        sc.userNeuteredExtendedKeys.includes(params.counterpartyXpub)
      );
    });
    if (targetChannel.length === 0) {
      return {
        multisigAddress: undefined
      };
    }
    return {
      multisigAddress: targetChannel[0].multisigAddress
    };
  }
}
