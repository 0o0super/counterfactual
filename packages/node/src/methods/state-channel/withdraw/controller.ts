import { Node } from "@counterfactual/types";
import { JsonRpcProvider, TransactionResponse } from "ethers/providers";

import { ProtocolMessage, xkeyKthAddress } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  WithdrawMessage
} from "../../../types";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

import { runWithdrawProtocol } from "./operation";

export default class WithdrawController extends NodeController {
  public static readonly methodName = Node.MethodName.WITHDRAW;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.WithdrawParams
  ): Promise<Node.WithdrawResult> {
    const {
      publicIdentifier,
      store,
      networkContext,
      provider,
      wallet,
      blocksNeededForConfirmation,
      outgoing,
      messagingService
    } = requestHandler;
    const { recipient, multisigAddress, amount } = params;

    const channel = await store.getStateChannel(multisigAddress);

    if (channel.hasAppInstanceOfKind(networkContext.ETHBalanceRefundApp)) {
      return Promise.reject(ERRORS.CANNOT_WITHDRAW);
    }

    params.recipient = recipient || xkeyKthAddress(publicIdentifier, 0);

    await runWithdrawProtocol(requestHandler, requestId, params);

    // actually withdraw
    const commitment = await store.getWithdrawalCommitment(multisigAddress);

    if (!commitment) {
      throw Error("no commitment found");
    }

    const tx = {
      ...commitment,
      gasPrice: await provider.getGasPrice(),
      gasLimit: 300000
    };

    try {
      let txResponse: TransactionResponse;

      if (provider instanceof JsonRpcProvider) {
        const signer = await provider.getSigner();
        txResponse = await signer.sendTransaction(tx);
      } else {
        txResponse = await wallet.sendTransaction(tx);
      }

      outgoing.emit(NODE_EVENTS.WITHDRAWAL_STARTED, {
        value: amount,
        txHash: txResponse.hash
      });

      await provider.waitForTransaction(
        txResponse.hash as string,
        blocksNeededForConfirmation
      );
    } catch (e) {
      outgoing.emit(NODE_EVENTS.WITHDRAWAL_FAILED, e);
      throw new Error(`${ERRORS.WITHDRAWAL_FAILED}: ${e}`);
    }

    // withdraw success!
    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );
    const counterparty = channel.userNeuteredExtendedKeys.filter(
      a => a !== publicIdentifier
    )[0];

    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([counterparty], ackMsg);

    return { amount, recipient: params.recipient };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message provided for ptotocol step");
    }
    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.WithdrawParams
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;

    // withdraw confirm message!
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.WITHDRAWAL_CONFIRMED,
      state: NodeExecutionState.FINISHING,
      method: Node.MethodName.WITHDRAW,
      data: params
    } as WithdrawMessage;
  }
}
