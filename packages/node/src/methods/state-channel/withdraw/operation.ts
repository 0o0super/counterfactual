import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { getPeersAddressFromChannel } from "../../../utils";

export async function runWithdrawProtocol(
  requestHandler: RequestHandler,
  requestId: string,
  params: Node.WithdrawParams
) {
  const { publicIdentifier, instructionExecutor, store } = requestHandler;
  const { multisigAddress, amount } = params;

  const [peerAddress] = await getPeersAddressFromChannel(
    publicIdentifier,
    store,
    multisigAddress
  );

  await instructionExecutor.runWithdrawProtocol(
    {
      amount,
      multisigAddress,
      recipient: params.recipient as string,
      initiatingXpub: publicIdentifier,
      respondingXpub: peerAddress
    },
    requestId
  );
}
