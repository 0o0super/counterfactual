import { Node } from "@counterfactual/types";

import { ProtocolMessage, xkeyKthAddress } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  DepositConfirmationMessage,
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage
} from "../../../types";
import { getPeersAddressFromChannel } from "../../../utils";
// import { getPeersAddressFromChannel } from "../../../utils";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

export default class DepositController extends NodeController {
  public static readonly methodName = Node.MethodName.DEPOSIT;

  protected async beforeExecution(
    requestHandler: RequestHandler,
    params: Node.DepositParams
  ): Promise<void> {
    const { provider } = requestHandler;
    const { amount } = params;

    // two deposit should not conflict as we lock channel at machine level
    // const channel = await store.getStateChannel(multisigAddress);

    // if (
    //   channel.hasAppInstanceOfKind(
    //     requestHandler.networkContext.ETHBalanceRefundApp
    //   )
    // ) {
    //   return Promise.reject(ERRORS.CANNOT_DEPOSIT);
    // }

    const address = await requestHandler.getSignerAddress();

    const balanceOfSigner = await provider.getBalance(address);

    if (balanceOfSigner.lt(amount)) {
      return Promise.reject(`${ERRORS.INSUFFICIENT_FUNDS}: ${address}`);
    }
  }

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.DepositParams
  ): Promise<Node.DepositResult> {
    const {
      provider,
      store,
      publicIdentifier,
      outgoing,
      messagingService,
      instructionExecutor
    } = requestHandler;
    const { multisigAddress, amount } = params;
    const counterparty = (await getPeersAddressFromChannel(
      publicIdentifier,
      store,
      multisigAddress
    ))[0];

    await this.beforeExecution(requestHandler, params);
    await instructionExecutor.runDepositProtocol(
      {
        multisigAddress,
        amount,
        initiatingXpub: publicIdentifier,
        respondingXpub: counterparty,
        recipient: xkeyKthAddress(publicIdentifier, 0)
      },
      requestId
    );

    // await installBalanceRefundApp(requestHandler, requestId, params);
    // const success = await makeDeposit(requestHandler, params);
    // await uninstallBalanceRefundApp(requestHandler, `${requestId}u`, params);

    // if (!success) {
    //   throw Error("Deposit GG");
    // }

    // const sc = await store.getStateChannel(multisigAddress);
    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );

    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([counterparty], ackMsg);

    return {
      multisigBalance: await provider.getBalance(multisigAddress)
    };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    const { instructionExecutor } = requestHandler;
    await instructionExecutor.runProtocolWithMessage(
      protocolMessage!,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.DepositParams
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.DEPOSIT_CONFIRMED,
      method: Node.MethodName.DEPOSIT,
      state: NodeExecutionState.FINISHING,
      data: {
        ...params,
        // This party shouldn't get notified by the peer node
        notifyCounterparty: false
      }
    } as DepositConfirmationMessage;
  }
}
