import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../machine";
import { RequestHandler } from "../request-handler";
import { NodeExecutionState, NodeMessage } from "../types";

export abstract class NodeController {
  public static readonly methodName: Node.MethodName;

  public async execute(
    requestId: string,
    requestHandler: RequestHandler,
    params: Node.MethodParams,
    step: NodeExecutionState,
    protocolMessage?: ProtocolMessage
  ): Promise<Node.MethodResult> {
    const { outgoing } = requestHandler;

    switch (step) {
      case NodeExecutionState.INITIATING:
        return await this.initiatingStep(requestHandler, requestId, params);
      case NodeExecutionState.RUNNING_PROTOCOL:
        if (!protocolMessage) {
          throw Error("No protocol message found!");
        }
        await this.protocolStep(requestHandler, requestId, protocolMessage);
        return {};
      case NodeExecutionState.FINISHING:
        const msg = await this.finishingMessage(
          requestHandler,
          requestId,
          params
        );
        outgoing.emit(msg.type, msg);
        return {};
    }
  }

  protected abstract async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.MethodParams
  ): Promise<Node.MethodResult>;

  protected abstract async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void>;

  protected abstract async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.MethodParams
  ): Promise<NodeMessage>;
}
