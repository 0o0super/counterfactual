import { Node } from "@counterfactual/types";

import { ProposedAppInstanceInfo } from "../../../models";
import { Store } from "../../../store";
import { getChannelFromPeerAddress } from "../../../utils";

/**
 * Creates a ProposedAppInstanceInfo to reflect the proposal received from
 * the client.
 * @param myIdentifier
 * @param store
 * @param params
 */
export async function createProposedAppInstance(
  store: Store,
  params: Node.ProposeInstallParams
): Promise<string> {
  const channel = await getChannelFromPeerAddress(
    params.proposedByIdentifier,
    params.proposedToIdentifier,
    store
  );

  const proposedAppInstanceInfo = new ProposedAppInstanceInfo(
    {
      ...params,
      multisigAddress: channel.multisigAddress
    },
    channel.multisigAddress
  );

  await store.addAppInstanceProposal(channel, proposedAppInstanceInfo);

  return proposedAppInstanceInfo.id;
}

// export async function setAppInstanceIDForProposeInstall(
//   myIdentifier: string,
//   store: Store,
//   params: Node.ProposeInstallParams
// ) {
//   const channel = await getChannelFromPeerAddress(
//     myIdentifier,
//     params.proposedByIdentifier,
//     store
//   );

//   const proposedAppInstanceInfo = new ProposedAppInstanceInfo(
//     {
//       ...params,
//       proposedByIdentifier: params.proposedByIdentifier
//     },
//     channel
//   );

//   await store.addAppInstanceProposal(channel, proposedAppInstanceInfo);
// }
