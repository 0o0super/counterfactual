import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  ProposeMessage
} from "../../../types";
import { NodeController } from "../../controller";

import { createProposedAppInstance } from "./operation";

/**
 * This creates an entry of a proposed AppInstance while sending the proposal
 * to the peer with whom this AppInstance is specified to be installed.
 * @param params
 * @returns The AppInstanceId for the proposed AppInstance
 */
export default class ProposeInstallController extends NodeController {
  public static readonly methodName = Node.MethodName.PROPOSE_INSTALL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeInstallParams
  ): Promise<Node.ProposeInstallResult> {
    const { messagingService, publicIdentifier } = requestHandler;

    params.proposedByIdentifier = publicIdentifier;
    const msg = await this.finishingMessage(requestHandler, requestId, params);
    messagingService.send([params.proposedToIdentifier], msg);

    return { appInstanceId: (msg as ProposeMessage).data.appInstanceId! };
  }

  protected protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw new Error("Method not implemented.");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeInstallParams
  ): Promise<NodeMessage> {
    const id = await createProposedAppInstance(requestHandler.store, params);
    // FOR DEBUG
    if (params.appInstanceId) {
      if (params.appInstanceId !== id) {
        throw Error(
          `SOMETHINGS GOING WRONG!!!!! ${params.appInstanceId} vs ${id}`
        );
      }
    }
    params.appInstanceId = id;
    return {
      requestId,
      from: params.proposedByIdentifier,
      method: Node.MethodName.PROPOSE_INSTALL,
      type: NODE_EVENTS.PROPOSE_INSTALL,
      state: NodeExecutionState.FINISHING,
      data: params
    } as ProposeMessage;
  }
}
