import { InstructionExecutor } from "../../../machine";

export async function uninstallAppInstanceFromChannel(
  requestId: string,
  multisigAddress: string,
  instructionExecutor: InstructionExecutor,
  initiatingXpub: string,
  respondingXpub: string,
  appInstanceId: string
): Promise<void> {
  await instructionExecutor.runUninstallProtocol(
    {
      initiatingXpub,
      respondingXpub,
      multisigAddress: multisigAddress,
      appIdentityHash: appInstanceId
    },
    requestId
  );
}
