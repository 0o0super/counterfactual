import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UninstallMessage
} from "../../../types";
import { getCounterpartyAddress } from "../../../utils";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

import { uninstallAppInstanceFromChannel } from "./operation";

export default class UninstallController extends NodeController {
  public static readonly methodName = Node.MethodName.UNINSTALL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UninstallParams
  ): Promise<Node.UninstallResult> {
    const {
      store,
      instructionExecutor,
      publicIdentifier,
      outgoing,
      messagingService
    } = requestHandler;
    const { appInstanceId } = params;

    const stateChannel = await store.getChannelFromAppInstanceID(appInstanceId);

    if (!stateChannel.hasAppInstance(appInstanceId)) {
      throw new Error(ERRORS.APP_ALREADY_UNINSTALLED(appInstanceId));
    }

    const to = getCounterpartyAddress(
      publicIdentifier,
      stateChannel.userNeuteredExtendedKeys
    );

    await uninstallAppInstanceFromChannel(
      requestId,
      stateChannel.multisigAddress,
      instructionExecutor,
      publicIdentifier,
      to,
      appInstanceId
    );

    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );
    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([to], ackMsg);

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UninstallParams
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;
    return {
      requestId,
      from: publicIdentifier,
      state: NodeExecutionState.FINISHING,
      type: NODE_EVENTS.UNINSTALL,
      method: Node.MethodName.UNINSTALL,
      data: params
    } as UninstallMessage;
  }
}
