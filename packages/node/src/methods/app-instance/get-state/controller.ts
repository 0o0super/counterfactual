import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { ERRORS } from "../../errors";
import { NodeStaticMethodController } from "../../static-method-controller";

/**
 * Handles the retrieval of an AppInstance's state.
 * @param this
 * @param params
 */
export default class GetStateController extends NodeStaticMethodController {
  public static readonly methodName = Node.MethodName.GET_STATE;

  public async execute(
    requestHandler: RequestHandler,
    params: Node.GetStateParams
  ): Promise<Node.GetStateResult> {
    const { store } = requestHandler;
    const { appInstanceId } = params;

    if (!appInstanceId) {
      Promise.reject(ERRORS.NO_APP_INSTANCE_ID_FOR_GET_STATE);
    }

    const appInstance = await store.getAppInstance(appInstanceId);

    return {
      state: appInstance.state
    };
  }
}
