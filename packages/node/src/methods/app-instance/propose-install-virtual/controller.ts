import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  ProposeVirtualMessage
} from "../../../types";
import { NodeController } from "../../controller";

import { setAppInstanceIDForProposeInstallVirtual } from "./operation";

/**
 * This creates an entry of a proposed Virtual AppInstance while sending the
 * proposal to the intermediaries and the responding Node.
 * @param params
 * @returns The AppInstanceId for the proposed AppInstance
 */
export default class ProposeInstallVirtualController extends NodeController {
  public static readonly methodName = Node.MethodName.PROPOSE_INSTALL_VIRTUAL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeInstallVirtualParams
  ): Promise<Node.ProposeInstallVirtualResult> {
    const { publicIdentifier, messagingService } = requestHandler;

    // TODO: check if channel is open with the first intermediary
    // and that there are sufficient funds

    // TODO: Also create the proposed eth virtual app agreement
    params.proposedByIdentifier = publicIdentifier;
    const msg = await this.finishingMessage(requestHandler, requestId, params);

    // send to all at once
    const sendTo = params.intermediaries.concat([params.proposedToIdentifier]);
    await messagingService.send(sendTo, msg); // should await ?

    return {
      appInstanceId: (msg as ProposeVirtualMessage).data.appInstanceId!
    };
  }

  protected protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw new Error("Method not implemented.");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeInstallVirtualParams
  ): Promise<NodeMessage> {
    const { publicIdentifier, store } = requestHandler;

    const id = await setAppInstanceIDForProposeInstallVirtual(store, params);
    params.appInstanceId = id;

    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
      state: NodeExecutionState.FINISHING,
      method: Node.MethodName.PROPOSE_INSTALL_VIRTUAL,
      data: params
    } as ProposeVirtualMessage;
  }
}
