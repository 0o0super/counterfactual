import { Node } from "@counterfactual/types";

import { StateChannel, virtualChannelKey } from "../../../machine";
import { ProposedAppInstanceInfo } from "../../../models";
import { Store } from "../../../store";
import { getChannelFromPeerAddress } from "../../../utils";
import { ERRORS } from "../../errors";

export async function setAppInstanceIDForProposeInstallVirtual(
  store: Store,
  params: Node.ProposeInstallVirtualParams
) {
  const { intermediaries, proposedByIdentifier, proposedToIdentifier } = params;

  const channel = await getOrCreateVirtualChannel(
    proposedByIdentifier,
    proposedToIdentifier,
    intermediaries,
    store
  );

  const proposedAppInstanceInfo = new ProposedAppInstanceInfo(
    {
      ...params,
      proposedByIdentifier,
      multisigAddress: channel.multisigAddress
    },
    channel.multisigAddress
  );

  await store.addVirtualAppInstanceProposal(proposedAppInstanceInfo);
  return proposedAppInstanceInfo.id;
}

export async function getOrCreateVirtualChannel(
  initiatorIdentifier: string,
  respondingIdentifier: string,
  intermediaries: string[],
  store: Store
): Promise<StateChannel> {
  let channel: StateChannel;
  try {
    channel = await getChannelFromPeerAddress(
      initiatorIdentifier,
      respondingIdentifier,
      store
    );
  } catch (e) {
    if (
      e.includes(
        ERRORS.NO_CHANNEL_BETWEEN_NODES(
          initiatorIdentifier,
          respondingIdentifier
        )
      ) &&
      intermediaries !== undefined
    ) {
      const key = virtualChannelKey(
        [initiatorIdentifier, respondingIdentifier],
        intermediaries
      );
      channel = StateChannel.createEmptyChannel(key, [
        initiatorIdentifier,
        respondingIdentifier
      ]);

      await store.saveStateChannel(channel);
    } else {
      return Promise.reject(e);
    }
  }
  return channel;
}
