import { Node } from "@counterfactual/types";

import { NODE_EVENTS } from "../../..";
import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NodeExecutionState,
  NodeMessage,
  UninstallVirtualMessage
} from "../../../types";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

import { uninstallAppInstanceFromChannel } from "./operation";

export default class UninstallVirtualController extends NodeController {
  public static readonly methodName = Node.MethodName.UNINSTALL_VIRTUAL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UninstallVirtualParams
  ): Promise<Node.UninstallVirtualResult> {
    const {
      store,
      instructionExecutor,
      publicIdentifier,
      outgoing,
      messagingService
    } = requestHandler;
    const { appInstanceId } = params;

    const stateChannel = await store.getChannelFromAppInstanceID(appInstanceId);

    if (!stateChannel.hasAppInstance(appInstanceId)) {
      throw new Error(ERRORS.APP_ALREADY_UNINSTALLED(appInstanceId));
    }

    // who initiate uninstall matter! intermediaries can be in reverse
    const {
      proposedByIdentifier,
      intermediaries,
      proposedToIdentifier
    } = await store.getAppInstanceInfo(appInstanceId);
    if (!intermediaries) {
      throw Error("Target is not a virtual app");
    }

    if (
      ![proposedByIdentifier, proposedToIdentifier].includes(publicIdentifier)
    ) {
      throw Error("Intermediaries cannot initiate uninstall");
    }

    let intermediariesWithCorrectOrder = intermediaries;
    let initiating = proposedByIdentifier;
    let responding = proposedToIdentifier;
    if (proposedToIdentifier === publicIdentifier) {
      // reverse!!
      intermediariesWithCorrectOrder = intermediaries.reverse();
      initiating = proposedToIdentifier;
      responding = proposedByIdentifier;
    }

    await uninstallAppInstanceFromChannel(
      store,
      requestId,
      instructionExecutor,
      initiating,
      responding,
      intermediariesWithCorrectOrder,
      appInstanceId
    );

    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );
    const counterparties = [
      proposedByIdentifier,
      ...intermediaries!,
      proposedToIdentifier
    ].filter(a => a !== publicIdentifier);
    outgoing.emit(ackMsg.type, ackMsg);
    // send to all except me
    messagingService.send(counterparties, ackMsg);

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UninstallVirtualParams
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UNINSTALL_VIRTUAL,
      state: NodeExecutionState.FINISHING,
      method: Node.MethodName.UNINSTALL_VIRTUAL,
      data: params
    } as UninstallVirtualMessage;
  }
}
