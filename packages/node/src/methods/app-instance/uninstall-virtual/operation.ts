import { InstructionExecutor } from "../../../machine";
import { Store } from "../../../store";

export async function uninstallAppInstanceFromChannel(
  store: Store,
  requestId: string,
  instructionExecutor: InstructionExecutor,
  initiatingXpub: string,
  respondingXpub: string,
  intermediariesXpub: string[],
  appInstanceId: string
): Promise<void> {
  const stateChannel = await store.getChannelFromAppInstanceID(appInstanceId);

  const appInstance = stateChannel.getAppInstance(appInstanceId);

  await instructionExecutor.runUninstallVirtualAppProtocol(
    {
      initiatingXpub,
      respondingXpub,
      intermediariesXpub,
      targetAppState: appInstance.state, // FIX ME, this can cause problem!
      targetAppIdentityHash: appInstanceId
    },
    requestId
  );
}
