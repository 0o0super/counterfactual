import { Node } from "@counterfactual/types";

import { RequestHandler } from "../../../request-handler";
import { ERRORS } from "../../errors";
import { NodeStaticMethodController } from "../../static-method-controller";

/**
 * Handles the retrieval of an AppInstance.
 * @param this
 * @param params
 */
export default class GetAppInstanceController extends NodeStaticMethodController {
  public static readonly methodName = Node.MethodName.GET_APP_INSTANCE;

  public async execute(
    requestHandler: RequestHandler,
    params: Node.GetAppInstanceDetailsParams
  ): Promise<Node.GetAppInstanceResult> {
    const { store } = requestHandler;
    const { appInstanceId } = params;

    if (!appInstanceId) {
      Promise.reject(ERRORS.NO_APP_INSTANCE_ID_TO_GET_DETAILS);
    }

    return {
      appInstance: await store.getAppInstance(appInstanceId)
    };
  }
}
