import { Node, SolidityABIEncoderV2Struct } from "@counterfactual/types";
import { INVALID_ARGUMENT } from "ethers/errors";

import { ERRORS } from "../../..";
import { InstructionExecutor, ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UpdateStateMessage
} from "../../../types";
import { getCounterpartyAddress } from "../../../utils";
import { NodeController } from "../../controller";

export default class UpdateStateController extends NodeController {
  public static readonly methodName = Node.MethodName.UPDATE_STATE;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UpdateStateParams
  ): Promise<Node.UpdateStateResult> {
    const {
      store,
      publicIdentifier,
      instructionExecutor,
      outgoing,
      messagingService
    } = requestHandler;
    const { appInstanceId, newState } = params;

    if (!appInstanceId) {
      return Promise.reject(ERRORS.NO_APP_INSTANCE_FOR_TAKE_ACTION);
    }

    const appInstance = await store.getAppInstance(appInstanceId);

    try {
      appInstance.encodeState(newState);
    } catch (e) {
      if (e.code === INVALID_ARGUMENT) {
        return Promise.reject(`${ERRORS.IMPROPERLY_FORMATTED_STRUCT}: ${e}`);
      }
      return Promise.reject(ERRORS.STATE_OBJECT_NOT_ENCODABLE);
    }

    const sc = await store.getChannelFromAppInstanceID(appInstanceId);

    const respondingXpub = getCounterpartyAddress(
      publicIdentifier,
      sc.userNeuteredExtendedKeys
    );

    await runUpdateStateProtocol(
      appInstanceId,
      requestId,
      sc.multisigAddress,
      instructionExecutor,
      publicIdentifier,
      respondingXpub,
      newState
    );

    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );

    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([respondingXpub], ackMsg);
    return { newState: (ackMsg as UpdateStateMessage).data.newState };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.UpdateStateParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store, publicIdentifier } = requestHandler;
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);
    const appInstance = sc.getAppInstance(appInstanceId);
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UPDATE_STATE,
      method: Node.MethodName.UPDATE_STATE,
      state: NodeExecutionState.FINISHING,
      data: { appInstanceId, newState: appInstance.state }
    } as UpdateStateMessage;
  }
}

async function runUpdateStateProtocol(
  appIdentityHash: string,
  requestId: string,
  multisigAddress: string,
  instructionExecutor: InstructionExecutor,
  initiatingXpub: string,
  respondingXpub: string,
  newState: SolidityABIEncoderV2Struct
) {
  await instructionExecutor.runUpdateProtocol(
    {
      initiatingXpub,
      respondingXpub,
      appIdentityHash,
      newState,
      multisigAddress
    },
    requestId
  );
}
