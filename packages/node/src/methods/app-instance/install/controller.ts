import { Node } from "@counterfactual/types";

import { ERRORS } from "../../..";
import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  InstallMessage,
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage
} from "../../../types";
import { NodeController } from "../../controller";

import { install } from "./operation";

/**
 * This converts a proposed app instance to an installed app instance while
 * sending an approved ack to the proposer.
 * @param params
 */
export default class InstallController extends NodeController {
  public static readonly methodName = Node.MethodName.INSTALL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.InstallParams
  ): Promise<Node.InstallResult> {
    const {
      store,
      instructionExecutor,
      outgoing,
      messagingService,
      publicIdentifier
    } = requestHandler;
    const { proposedId } = params;

    const sc = await store.getChannelFromAppInstanceID(proposedId);
    const counterparty = sc.userNeuteredExtendedKeys.filter(
      a => a !== publicIdentifier
    )[0];

    const appInstanceInfo = await install(
      store,
      requestId,
      instructionExecutor,
      params
    );
    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );

    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([counterparty], ackMsg);
    return { appInstance: appInstanceInfo };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }
    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.InstallParams
  ): Promise<NodeMessage> {
    const { store, publicIdentifier } = requestHandler;

    const { proposedId } = params;

    if (!proposedId || !proposedId.trim()) {
      throw new Error(ERRORS.NO_APP_INSTANCE_ID_TO_INSTALL);
    }

    const appInstanceInfo = await store.getProposedAppInstanceInfo(proposedId);

    const installedId = await store.saveRealizedProposedAppInstance(
      appInstanceInfo
    );
    return {
      requestId,
      from: publicIdentifier,
      method: Node.MethodName.INSTALL,
      type: NODE_EVENTS.INSTALL,
      state: NodeExecutionState.FINISHING,
      data: {
        ...params,
        installedId
      }
    } as InstallMessage;
  }
}
