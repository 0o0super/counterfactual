import { Node } from "@counterfactual/types";
import { AddressZero } from "ethers/constants";

import { InstructionExecutor } from "../../../machine";
import { ProposedAppInstanceInfo } from "../../../models";
import { Store } from "../../../store";
import { ERRORS } from "../../errors";

export async function install(
  store: Store,
  requestId: string,
  instructionExecutor: InstructionExecutor,
  params: Node.InstallParams
): Promise<ProposedAppInstanceInfo> {
  const { proposedId } = params;

  if (!proposedId || !proposedId.trim()) {
    return Promise.reject(ERRORS.NO_APP_INSTANCE_ID_TO_INSTALL);
  }

  const appInstanceInfo = await store.getProposedAppInstanceInfo(proposedId);

  const stateChannel = await store.getChannelFromAppInstanceID(proposedId);

  await instructionExecutor.runInstallProtocol(
    {
      initiatingXpub: appInstanceInfo.proposedToIdentifier,
      respondingXpub: appInstanceInfo.proposedByIdentifier,
      appIdentityHash: proposedId,
      multisigAddress: stateChannel.multisigAddress,
      aliceBalanceDecrement: appInstanceInfo.myDeposit,
      bobBalanceDecrement: appInstanceInfo.peerDeposit,
      initialState: appInstanceInfo.initialState,
      terms: {
        assetType: appInstanceInfo.asset.assetType,
        limit: appInstanceInfo.myDeposit.add(appInstanceInfo.peerDeposit),
        token: appInstanceInfo.asset.token || AddressZero
      },
      appInterface: {
        ...appInstanceInfo.abiEncodings,
        addr: appInstanceInfo.appId
      },
      defaultTimeout: appInstanceInfo.timeout.toNumber()
    },
    requestId
  );

  return appInstanceInfo;
}
