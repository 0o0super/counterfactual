import { AppInstanceInfo, Node } from "@counterfactual/types";

import { InstructionExecutor } from "../../../machine";
import { Store } from "../../../store";
import { ERRORS } from "../../errors";

export async function installVirtual(
  store: Store,
  requestId: string,
  instructionExecutor: InstructionExecutor,
  params: Node.InstallParams
): Promise<AppInstanceInfo> {
  const { proposedId } = params;

  if (!proposedId || !proposedId.trim()) {
    return Promise.reject(ERRORS.NO_APP_INSTANCE_ID_TO_INSTALL);
  }

  const appInstanceInfo = await store.getProposedAppInstanceInfo(proposedId);

  try {
    await instructionExecutor.runInstallVirtualAppProtocol(
      {
        initiatingXpub: appInstanceInfo.proposedToIdentifier,
        respondingXpub: appInstanceInfo.proposedByIdentifier,
        targetAppIdentityHash: proposedId,
        intermediariesXpub: [...appInstanceInfo.intermediaries!].reverse(),
        defaultTimeout: appInstanceInfo.timeout.toNumber(),
        appInterface: {
          addr: appInstanceInfo.appId,
          ...appInstanceInfo.abiEncodings
        },
        initialState: appInstanceInfo.initialState,
        // this is reversed
        initiatingBalanceDecrement: appInstanceInfo.peerDeposit,
        respondingBalanceDecrement: appInstanceInfo.myDeposit
      },
      requestId
    );
  } catch (e) {
    return Promise.reject(`${ERRORS.VIRTUAL_APP_INSTALLATION_FAIL}: ${e}`);
  }

  return appInstanceInfo;
}
