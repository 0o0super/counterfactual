import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  InstallVirtualMessage,
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage
} from "../../../types";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

import { installVirtual } from "./operation";

export default class InstallVirtualController extends NodeController {
  public static readonly methodName = Node.MethodName.INSTALL_VIRTUAL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.InstallVirtualParams
  ): Promise<Node.InstallVirtualResult> {
    const {
      store,
      instructionExecutor,
      publicIdentifier,
      outgoing,
      messagingService
    } = requestHandler;
    const { proposedId } = params;

    const appInfo = await store.getProposedAppInstanceInfo(proposedId);
    const counterparties = [
      appInfo.proposedByIdentifier,
      ...appInfo.intermediaries!,
      appInfo.proposedToIdentifier
    ].filter(a => a !== publicIdentifier);

    const appInstanceInfo = await installVirtual(
      requestHandler.store,
      requestId,
      instructionExecutor,
      params
    );

    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );

    outgoing.emit(ackMsg.type, ackMsg);
    // send to all except me
    messagingService.send(counterparties, ackMsg);
    return { appInstance: appInstanceInfo };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.InstallVirtualParams
  ): Promise<NodeMessage> {
    const store = requestHandler.store;

    const { proposedId } = params;

    if (!proposedId || !proposedId.trim()) {
      throw new Error(ERRORS.NO_APP_INSTANCE_ID_TO_INSTALL);
    }

    const appInstanceInfo = await store.getProposedAppInstanceInfo(proposedId);

    const installedId = await store.saveRealizedProposedAppInstance(
      appInstanceInfo
    );

    return {
      requestId,
      from: requestHandler.publicIdentifier,
      type: NODE_EVENTS.INSTALL_VIRTUAL,
      state: NodeExecutionState.FINISHING,
      method: Node.MethodName.INSTALL_VIRTUAL,
      data: { ...params, installedId }
    } as InstallVirtualMessage;
  }
}
