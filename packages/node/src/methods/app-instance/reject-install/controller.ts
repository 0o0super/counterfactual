import { Node } from "@counterfactual/types";

import { ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  RejectProposalMessage
} from "../../../types";
import { NodeController } from "../../controller";
// import rejectInstallVirtualController from "../reject-install-virtual/controller";

export default class RejectInstallController extends NodeController {
  public static readonly methodName = Node.MethodName.REJECT_INSTALL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.RejectInstallParams
  ): Promise<Node.RejectInstallResult> {
    const { appInstanceId } = params;
    const { messagingService, store } = requestHandler;

    const proposedAppInstanceInfo = await store.getProposedAppInstanceInfo(
      appInstanceId
    );

    const msg = await this.finishingMessage(requestHandler, requestId, params);

    messagingService.send([proposedAppInstanceInfo.proposedByIdentifier], msg);
    if (proposedAppInstanceInfo.intermediaries) {
      messagingService.send(proposedAppInstanceInfo.intermediaries, msg);
    }

    return {};
  }

  protected protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw new Error("Method not implemented.");
  }

  protected async finishingMessage(
    // reject-install-virtual may not be here!!
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.RejectInstallParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store } = requestHandler;
    // duplicated!
    const proposedAppInstanceInfo = await store.getProposedAppInstanceInfo(
      appInstanceId
    );
    const isVirtual = !!proposedAppInstanceInfo.intermediaries;
    await store.removeAppInstanceProposal(appInstanceId);

    return {
      requestId,
      from: requestHandler.publicIdentifier,
      type: isVirtual
        ? NODE_EVENTS.REJECT_INSTALL_VIRTUAL
        : NODE_EVENTS.REJECT_INSTALL,
      method: Node.MethodName.REJECT_INSTALL,
      state: NodeExecutionState.FINISHING,
      data: params
    } as RejectProposalMessage;
  }
}
