import { Node, SolidityABIEncoderV2Struct } from "@counterfactual/types";
import { INVALID_ARGUMENT } from "ethers/errors";

import { InstructionExecutor, ProtocolMessage } from "../../../machine";
import { RequestHandler } from "../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UpdateStateMessage
} from "../../../types";
import { getCounterpartyAddress } from "../../../utils";
import { NodeController } from "../../controller";
import { ERRORS } from "../../errors";

export default class TakeActionController extends NodeController {
  public static readonly methodName = Node.MethodName.TAKE_ACTION;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<Node.TakeActionResult> {
    const {
      store,
      publicIdentifier,
      instructionExecutor,
      outgoing,
      messagingService
    } = requestHandler;
    const { appInstanceId, action } = params;

    if (!appInstanceId) {
      return Promise.reject(ERRORS.NO_APP_INSTANCE_FOR_TAKE_ACTION);
    }

    const appInstance = await store.getAppInstance(appInstanceId);
    try {
      appInstance.encodeAction(action);
    } catch (e) {
      if (e.code === INVALID_ARGUMENT) {
        return Promise.reject(`${ERRORS.IMPROPERLY_FORMATTED_STRUCT}: ${e}`);
      }
      return Promise.reject(ERRORS.STATE_OBJECT_NOT_ENCODABLE);
    }

    const sc = await store.getChannelFromAppInstanceID(appInstanceId);

    const respondingXpub = getCounterpartyAddress(
      publicIdentifier,
      sc.userNeuteredExtendedKeys
    );

    await runTakeActionProtocol(
      appInstanceId,
      sc.multisigAddress,
      requestId,
      instructionExecutor,
      publicIdentifier,
      respondingXpub,
      action
    );

    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );

    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([respondingXpub], ackMsg);

    return { newState: (ackMsg as UpdateStateMessage).data.newState };
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;
    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store, publicIdentifier } = requestHandler;
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);
    const appInstance = sc.getAppInstance(appInstanceId);
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UPDATE_STATE,
      method: Node.MethodName.TAKE_ACTION,
      state: NodeExecutionState.FINISHING,
      data: { appInstanceId, newState: appInstance.state }
    } as UpdateStateMessage;
  }
}

async function runTakeActionProtocol(
  appIdentityHash: string,
  multisigAddress: string,
  requestId: string,
  instructionExecutor: InstructionExecutor,
  initiatingXpub: string,
  respondingXpub: string,
  action: SolidityABIEncoderV2Struct
) {
  try {
    await instructionExecutor.runTakeActionProtocol(
      {
        initiatingXpub,
        respondingXpub,
        appIdentityHash,
        action,
        multisigAddress
      },
      requestId
    );
  } catch (e) {
    if (e.toString().indexOf("VM Exception") !== -1) {
      // TODO: Fetch the revert reason
      throw new Error(`${ERRORS.INVALID_ACTION}`);
    }
    throw e;
  }
}
