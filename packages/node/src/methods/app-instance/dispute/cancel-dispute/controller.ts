import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import { Node, SolidityABIEncoderV2Struct } from "@counterfactual/types";
import { Contract } from "ethers/ethers";
import { TransactionResponse } from "ethers/providers";
import { keccak256 } from "ethers/utils";

import { ProtocolMessage } from "../../../../machine";
import { CommitmentType } from "../../../../machine/enums";
import { RequestHandler } from "../../../../request-handler";
import {
  CancelDisputeMessage,
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage
} from "../../../../types";
import { NodeController } from "../../../controller";

type AppChallenge = {
  // Fix me! Should not be here
  status: number; // Should be enum!
  latestSubmitter: string;
  appStateHash: string;
  disputeCounter: string;
  disputeNonce: string;
  finalizesAt: string;
  nonce: number;
};

export default class DisputeCancelController extends NodeController {
  public static readonly methodName = Node.MethodName.DISPUTE_CANCEL;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.CancelDisputeParams
  ): Promise<Node.MethodResult> {
    const {
      store,
      provider,
      networkContext,
      instructionExecutor,
      publicIdentifier,
      wallet,
      blocksNeededForConfirmation,
      outgoing,
      messagingService
    } = requestHandler;
    const { appInstanceId, proposingState } = params;

    const appInstance = await store.getAppInstance(appInstanceId);
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);

    const appInstanceInfo = await store.getAppInstanceInfo(appInstanceId);
    const counterpartyAddr =
      appInstanceInfo.proposedByIdentifier === publicIdentifier
        ? appInstanceInfo.proposedToIdentifier
        : appInstanceInfo.proposedByIdentifier;

    try {
      const appRegistry = new Contract(
        networkContext.AppRegistry,
        AppRegistryApp.abi,
        provider
      );
      const data: AppChallenge = await appRegistry.functions.getAppChallenge(
        appInstanceId
      );

      if (data.status !== 1) {
        throw new Error("Cannot cancel dispute when app not in dispute state.");
      }

      // here we accept on-chain state and agree it
      let stateToAgree: SolidityABIEncoderV2Struct | undefined;

      if (proposingState) {
        const hashOfProposingState = keccak256(
          appInstance.encodeState(proposingState)
        );
        if (hashOfProposingState === data.appStateHash) {
          // ok! the state proposed by counterparty is valid!
          stateToAgree = proposingState;
        }
      }

      if (appInstance.hashOfLatestState === data.appStateHash) {
        stateToAgree = appInstance.state;
      }

      if (!stateToAgree) {
        throw new Error("Unknown state");
      }

      // run machine!
      await instructionExecutor.runCancelDisputeProtocol(
        {
          stateToAgree,
          initiatingXpub: publicIdentifier,
          respondingXpub: counterpartyAddr,
          multisigAddress: sc.multisigAddress,
          appIdentityHash: appInstanceId
        },
        requestId
      );
    } catch (e) {
      throw new Error(e);
    }

    // actually run transaction
    const commitment = (await store.getCommitments(
      CommitmentType.CancelDispute,
      appInstanceId
    ))[0];

    if (!commitment) {
      throw Error("no commitment found");
    }

    const tx = {
      ...commitment,
      gasPrice: await provider.getGasPrice(),
      gasLimit: 300000
    };

    try {
      const txResponse: TransactionResponse = await wallet.sendTransaction(tx);

      await provider.waitForTransaction(
        txResponse.hash as string,
        blocksNeededForConfirmation
      );
    } catch (e) {
      throw new Error(e);
    }

    // send ack message
    const ackMsg = await this.finishingMessage(
      requestHandler,
      requestId,
      params
    );
    outgoing.emit(ackMsg.type, ackMsg);
    messagingService.send([counterpartyAddr], ackMsg);

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    if (!protocolMessage) {
      throw Error("No protocol message found!");
    }

    const { instructionExecutor } = requestHandler;
    // const { appInstanceId, proposingState } = params;

    // if (!proposingState) {
    //   throw Error("No proposing found!");
    // }

    // const appInstance = await store.getAppInstance(appInstanceId);

    // const appRegistry = new Contract(
    //   networkContext.AppRegistry,
    //   AppRegistryApp.abi,
    //   provider
    // );
    // const data: AppChallenge = await appRegistry.functions.getAppChallenge(
    //   appInstanceId
    // );

    // if (data.status !== 1) {
    //   throw new Error("Cannot cancel dispute when app not in dispute state.");
    // }

    // if (
    //   data.appStateHash !== keccak256(appInstance.encodeState(proposingState))
    // ) {
    //   throw new Error("Counterparty proposing an invalid state");
    // }

    // // append intermediaries signature if this is a virtual app
    // if (appInstance.isVirtualApp) {
    //   const sigs = await store.getIntermidiariesSignatureForApp(appInstanceId);
    //   state.saveCommitment("virtualAppSetState", "_placeholder");
    //   sigs.forEach((v, k) => state.saveSignature("virtualAppSetState", k, v));
    // }

    await instructionExecutor.runProtocolWithMessage(
      protocolMessage,
      requestId
    );
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.CancelDisputeParams
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;

    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.CANCEL_DISPUTE,
      method: Node.MethodName.DISPUTE_CANCEL,
      state: NodeExecutionState.FINISHING,
      data: params
    } as CancelDisputeMessage;
  }
}
