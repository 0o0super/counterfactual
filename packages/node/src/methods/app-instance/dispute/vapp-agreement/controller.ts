import { Node } from "@counterfactual/types";
import { TransactionResponse } from "ethers/providers";

import { ProtocolMessage } from "../../../../machine";
import { CommitmentType } from "../../../../machine/enums";
import { RequestHandler } from "../../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UpdateStateMessage
} from "../../../../types";
import { NodeController } from "../../../controller";

export default class DisputeVirtualAgreementController extends NodeController {
  public static readonly methodName = Node.MethodName.DISPUTE_VIRTUAL_AGREEMENT;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<Node.MethodResult> {
    const {
      store,
      wallet,
      provider,
      blocksNeededForConfirmation
    } = requestHandler;
    const { appInstanceId } = params;

    // const latestState = appInstance.state;
    // TODO: find latest state
    const commitments = await store.getCommitments(
      CommitmentType.ETHVirtualAppAgreement,
      appInstanceId
    );

    for (const commitment of commitments) {
      const tx = {
        ...commitment,
        gasPrice: await provider.getGasPrice(),
        gasLimit: 300000
      };

      try {
        const txResponse: TransactionResponse = await wallet.sendTransaction(
          tx
        );

        await provider.waitForTransaction(
          txResponse.hash as string,
          blocksNeededForConfirmation
        );
      } catch (e) {
        if (
          (e.message as string).includes("This transaction exectued before")
        ) {
          // This is not a fatal error
          continue;
        }
        throw new Error(e);
      }
    }

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw Error("Method not implemented");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store, publicIdentifier } = requestHandler;
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);
    const appInstance = sc.getAppInstance(appInstanceId);
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UPDATE_STATE,
      method: Node.MethodName.TAKE_ACTION,
      state: NodeExecutionState.FINISHING,
      data: { appInstanceId, newState: appInstance.state }
    } as UpdateStateMessage;
  }
}
