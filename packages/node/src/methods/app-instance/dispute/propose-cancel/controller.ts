import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import { Node, SolidityABIEncoderV2Struct } from "@counterfactual/types";
import { Contract } from "ethers/ethers";

import { ProtocolMessage } from "../../../../machine";
import { RequestHandler } from "../../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  ProposeCancelDisputeMessage
} from "../../../../types";
import { NodeController } from "../../../controller";

type AppChallenge = {
  // Fix me! Should not be here
  status: number; // Should be enum!
  latestSubmitter: string;
  appStateHash: string;
  disputeCounter: string;
  disputeNonce: string;
  finalizesAt: string;
  nonce: number;
};

export default class ProposeCancelDisputeController extends NodeController {
  public static readonly methodName = Node.MethodName.PROPOSE_CANCEL_DISPUTE;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeCancelDisputeParams
  ): Promise<Node.MethodResult> {
    const {
      store,
      provider,
      networkContext,
      publicIdentifier,
      messagingService
    } = requestHandler;
    const { appInstanceId } = params;

    const appInstance = await store.getAppInstance(appInstanceId);

    try {
      const appRegistry = new Contract(
        networkContext.AppRegistry,
        AppRegistryApp.abi,
        provider
      );
      const data: AppChallenge = await appRegistry.functions.getAppChallenge(
        appInstanceId
      );

      if (data.status !== 1) {
        throw new Error("Cannot cancel dispute when app not in dispute state.");
      }

      let proposingState: SolidityABIEncoderV2Struct | undefined;
      if (appInstance.hashOfLatestState !== data.appStateHash) {
        if (appInstance.nonce > data.nonce) {
          // I have later state, hopefully counterparty will accept it
          proposingState = appInstance.state;
        } else if (appInstance.nonce < data.nonce) {
          // On-chain state later than mine, leave proposing state empty
          proposingState = undefined;
        } else {
          // this shold never happen, when it do, THROW!
          throw new Error("Got different state with same nonce, abort.");
        }
      } else {
        proposingState = appInstance.state;
      }
      const msg = await this.finishingMessage(requestHandler, requestId, {
        appInstanceId,
        proposingState
      });

      const appInstanceInfo = await store.getAppInstanceInfo(appInstanceId);
      const counterparty =
        appInstanceInfo.proposedByIdentifier === publicIdentifier
          ? appInstanceInfo.proposedToIdentifier
          : appInstanceInfo.proposedByIdentifier;
      await messagingService.send([counterparty], msg);
    } catch (e) {
      throw new Error(e);
    }

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw Error("Method not implemented");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.ProposeCancelDisputeParams,
    protocolMessage?: ProtocolMessage
  ): Promise<NodeMessage> {
    const { publicIdentifier } = requestHandler;
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.PROPOSE_CANCEL_DISPUTE,
      method: Node.MethodName.PROPOSE_CANCEL_DISPUTE,
      state: NodeExecutionState.FINISHING,
      data: params
    } as ProposeCancelDisputeMessage;
  }
}
