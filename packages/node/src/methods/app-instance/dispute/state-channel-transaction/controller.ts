import { Node } from "@counterfactual/types";
import { TransactionResponse } from "ethers/providers";

import { ProtocolMessage } from "../../../../machine";
import { CommitmentType } from "../../../../machine/enums";
import { RequestHandler } from "../../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UpdateStateMessage
} from "../../../../types";
import { NodeController } from "../../../controller";

export default class DisputeTransactionController extends NodeController {
  public static readonly methodName = Node.MethodName.DISPUTE_TRANSACTION;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams,
    protocolMessage?: ProtocolMessage
  ): Promise<Node.MethodResult> {
    const {
      store,
      wallet,
      provider,
      blocksNeededForConfirmation
    } = requestHandler;
    const { appInstanceId } = params;

    // TODO: find latest state
    const commitment = (await store.getCommitments(
      CommitmentType.Install,
      appInstanceId
    ))[0];

    const tx = {
      ...commitment,
      gasPrice: await provider.getGasPrice(),
      gasLimit: 300000
    };

    let txResponse: TransactionResponse;
    try {
      txResponse = await wallet.sendTransaction(tx);
      await provider.waitForTransaction(
        txResponse.hash as string,
        blocksNeededForConfirmation
      );
    } catch (e) {
      throw new Error(e);
    }

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw Error("Method not implemented");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store, publicIdentifier } = requestHandler;
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);
    const appInstance = sc.getAppInstance(appInstanceId);

    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UPDATE_STATE,
      method: Node.MethodName.TAKE_ACTION,
      state: NodeExecutionState.FINISHING,
      data: { appInstanceId, newState: appInstance.state }
    } as UpdateStateMessage;
  }
}
