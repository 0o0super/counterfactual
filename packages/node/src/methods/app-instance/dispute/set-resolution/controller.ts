import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import { Node } from "@counterfactual/types";
import { TransactionResponse } from "ethers/providers";
import { Interface } from "ethers/utils";

import { ProtocolMessage } from "../../../../machine";
import { RequestHandler } from "../../../../request-handler";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  UpdateStateMessage
} from "../../../../types";
import { NodeController } from "../../../controller";

export default class DisputeResolutionController extends NodeController {
  public static readonly methodName = Node.MethodName.DISPUTE_RESOLUTION;

  protected async initiatingStep(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<Node.MethodResult> {
    const {
      store,
      wallet,
      provider,
      networkContext,
      blocksNeededForConfirmation
    } = requestHandler;
    const { appInstanceId } = params;

    const appInstance = await store.getAppInstance(appInstanceId);

    try {
      const appRegistry = new Interface(AppRegistryApp.abi);
      const data = appRegistry.functions.setResolution.encode([
        appInstance.identity,
        appInstance.encodedLatestState,
        appInstance.encodedTerms
      ]);

      const tx = {
        data,
        to: networkContext.AppRegistry,
        value: 0,
        gasPrice: await provider.getGasPrice(),
        gasLimit: 300000
      };

      const txResponse: TransactionResponse = await wallet.sendTransaction(tx);

      await provider.waitForTransaction(
        txResponse.hash as string,
        blocksNeededForConfirmation
      );
    } catch (e) {
      throw new Error(e);
    }

    return {};
  }

  protected async protocolStep(
    requestHandler: RequestHandler,
    requestId: string,
    protocolMessage?: ProtocolMessage
  ): Promise<void> {
    throw Error("Method not implemented");
  }

  protected async finishingMessage(
    requestHandler: RequestHandler,
    requestId: string,
    params: Node.TakeActionParams
  ): Promise<NodeMessage> {
    const { appInstanceId } = params;
    const { store, publicIdentifier } = requestHandler;
    const sc = await store.getChannelFromAppInstanceID(appInstanceId);
    const appInstance = sc.getAppInstance(appInstanceId);
    return {
      requestId,
      from: publicIdentifier,
      type: NODE_EVENTS.UPDATE_STATE,
      method: Node.MethodName.TAKE_ACTION,
      state: NodeExecutionState.FINISHING,
      data: { appInstanceId, newState: appInstance.state }
    } as UpdateStateMessage;
  }
}
