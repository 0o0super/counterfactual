// import AppRegistryApp from "@counterfactual/contracts/build/AppRegistry.json";
import { NetworkContext, Node as NodeTypes } from "@counterfactual/types";
// import { Contract } from "ethers";
import { BaseProvider } from "ethers/providers";
import { Signature, SigningKey } from "ethers/utils";
import { HDNode } from "ethers/utils/hdnode";
import EventEmitter from "eventemitter3";
import Queue from "p-queue";
import { Memoize } from "typescript-memoize";

import AutoNonceWallet from "./auto-nonce-wallet";
import {
  InstructionExecutor,
  Opcode,
  ProtocolMessage,
  StateChannel
} from "./machine";
import { CommitmentType, Protocol } from "./machine/enums";
import { configureNetworkContext } from "./network-configuration";
import { RequestHandler } from "./request-handler";
import { IMessagingService, IStoreService } from "./services";
import { getHDNode } from "./signer";
import {
  NODE_EVENTS,
  NodeExecutionState,
  NodeMessage,
  NodeMessageWrappedProtocolMessage
} from "./types";
import { hashOfOrderedPublicIdentifiers, protocolToNodeMethod } from "./utils";

export interface NodeConfig {
  // The prefix for any keys used in the store by this Node depends on the
  // execution environment.
  STORE_KEY_PREFIX: string;
}

const REASONABLE_NUM_BLOCKS_TO_WAIT = 1;

export class Node {
  /**
   * Because the Node receives and sends out messages based on Event type
   * https://github.com/counterfactual/monorepo/blob/master/packages/cf.js/API_REFERENCE.md#events
   * incoming and outgoing emitters need to be used.
   **/
  private readonly outgoing: EventEmitter;
  private readonly error: EventEmitter;
  // for channel lock specific event
  private readonly internal: EventEmitter;

  private instructionExecutor!: InstructionExecutor; // not a good idea
  private readonly networkContext: NetworkContext;

  // These properties don't have initializers in the constructor and get
  // initialized in the `asynchronouslySetupUsingRemoteServices` function
  private signer!: HDNode;
  protected requestHandler!: RequestHandler;

  private lockerQueue = new Queue({ concurrency: 1 });
  private lockedChannels = new Set<string>();
  // this map keeps waiting key to correspinding promise resolve
  private waitingKeys = new Map<string, (msg: ProtocolMessage) => void>();
  // this map keeps protocol message arrive before wait <= waiting key
  private pendingProtocolMessage = new Map<string, ProtocolMessage>();
  // private protocolMessageStore = new Map<string, ProtocolMessage>();

  static async create(
    messagingService: IMessagingService,
    storeService: IStoreService,
    nodeConfig: NodeConfig,
    provider: BaseProvider,
    networkOrNetworkContext: string | NetworkContext,
    blocksNeededForConfirmation?: number
  ): Promise<Node> {
    const node = new Node(
      messagingService,
      storeService,
      nodeConfig,
      provider,
      networkOrNetworkContext,
      blocksNeededForConfirmation
    );

    return await node.asynchronouslySetupUsingRemoteServices();
  }

  private constructor(
    private readonly messagingService: IMessagingService,
    private readonly storeService: IStoreService,
    private readonly nodeConfig: NodeConfig,
    private readonly provider: BaseProvider,
    networkContext: string | NetworkContext,
    readonly blocksNeededForConfirmation?: number
  ) {
    this.outgoing = new EventEmitter();
    this.error = new EventEmitter();
    this.internal = new EventEmitter();
    this.blocksNeededForConfirmation = REASONABLE_NUM_BLOCKS_TO_WAIT;
    if (typeof networkContext === "string") {
      this.networkContext = configureNetworkContext(networkContext);

      if (
        blocksNeededForConfirmation &&
        blocksNeededForConfirmation > REASONABLE_NUM_BLOCKS_TO_WAIT
      ) {
        this.blocksNeededForConfirmation = blocksNeededForConfirmation;
      }
    } else {
      // Used for testing / ganache
      this.networkContext = networkContext;
    }

    console.log(
      `Waiting for ${this.blocksNeededForConfirmation} block confirmations`
    );

    // for debug!
    // setInterval(() => {
    //   // const pendingMsgStr = [...this.protocolMessageStore.keys()]
    //   //   .map(k => k.slice(0, 6))
    //   //   .join(", ");
    //   const lockedChStr = [...this.lockedChannels.keys()]
    //     .map(k => `${k.slice(3, 9)}(${k.length})`)
    //     .join(", ");
    //   // if (pendingMsgStr) {
    //   //   console.error(`PMSG: ${pendingMsgStr}`);
    //   // }
    //   if (lockedChStr) {
    //     console.error(`LOCKED: ${lockedChStr}`);
    //   }
    // }, 5000);

    // blockchain listener *for test*
    // this better be somewhere else
    // provider.on("block", async (blockNum: number) => {
    //   const appRegistry = new Contract(
    //     this.networkContext.AppRegistry,
    //     AppRegistryApp.abi,
    //     this.provider
    //   );
    //   const allAppID = await this.requestHandler.store.getAllAppInstanceID();
    //   for (const id of allAppID) {
    //     const result = await appRegistry.functions.getAppChallenge(id);
    //     if (result[0] !== 0) {
    //       // this app is in dispute!
    //     }
    //   }
    // });
  }

  private async asynchronouslySetupUsingRemoteServices(): Promise<Node> {
    this.signer = await getHDNode(this.storeService);
    this.instructionExecutor = this.buildInstructionExecutor();
    console.log(`Node signer address: ${this.signer.address}`);
    console.log(`Node public identifier: ${this.publicIdentifier}`);
    this.requestHandler = new RequestHandler(
      this.publicIdentifier,
      this.outgoing,
      this.error,
      this.storeService,
      this.messagingService,
      this.instructionExecutor,
      this.networkContext,
      this.provider,
      new AutoNonceWallet(this.signer.privateKey, this.provider),
      `${this.nodeConfig.STORE_KEY_PREFIX}/${this.publicIdentifier}`,
      this.blocksNeededForConfirmation!
    );
    this.registerMessagingConnection();
    return this;
  }

  @Memoize()
  get publicIdentifier(): string {
    return this.signer.neuter().extendedKey;
  }

  /**
   * Instantiates a new _InstructionExecutor_ object and attaches middleware
   * for the OP_SIGN, IO_SEND, and IO_SEND_AND_WAIT opcodes.
   */
  private buildInstructionExecutor(): InstructionExecutor {
    const instructionExecutor = new InstructionExecutor(
      this.networkContext,
      this.provider,
      this.publicIdentifier
    );

    // todo(xuanji): remove special cases
    const makeSigner = (asIntermediary: boolean) => {
      return async (args: any[]) => {
        if (args.length !== 1 && args.length !== 2) {
          throw Error("OP_SIGN middleware received wrong number of arguments.");
        }

        const [commitment, overrideKeyIndex] = args;
        const keyIndex = overrideKeyIndex || 0;

        const signingKey = new SigningKey(
          this.signer.derivePath(`${keyIndex}`).privateKey
        );

        return signingKey.signDigest(commitment.hashToSign(asIntermediary));
      };
    };

    instructionExecutor.register(Opcode.OP_SIGN, makeSigner(false));

    instructionExecutor.register(
      Opcode.OP_SIGN_AS_INTERMEDIARY,
      makeSigner(true)
    );

    instructionExecutor.register(Opcode.OP_GET_SINGER, async () => {
      return await this.requestHandler.getSigner();
    });

    instructionExecutor.register(Opcode.IO_SEND, async (args: any[]) => {
      const fromXpub = this.publicIdentifier;
      const requestId = args[0] as string;
      const messages = args.slice(1) as ProtocolMessage[];
      if (messages.length === 0) {
        throw Error("No message to send!");
      }
      for (const msg of messages) {
        await this.messagingService.send(msg.toXpub, {
          from: fromXpub,
          type: NODE_EVENTS.PROTOCOL_MESSAGE_EVENT,
          method: protocolToNodeMethod(msg.protocol),
          state: NodeExecutionState.RUNNING_PROTOCOL,
          requestId: requestId,
          data: {
            ...msg,
            fromXpub
          }
        } as NodeMessageWrappedProtocolMessage);
      }
    });

    instructionExecutor.register(
      Opcode.IO_WAIT,
      async (waitingKeys: string[]) => {
        const promisesToWait: Promise<ProtocolMessage>[] = [];
        for (const waitingKey of waitingKeys) {
          if (this.pendingProtocolMessage.has(waitingKey)) {
            promisesToWait.push(
              Promise.resolve(this.pendingProtocolMessage.get(waitingKey)!)
            );
            this.pendingProtocolMessage.delete(waitingKey);
          } else {
            promisesToWait.push(
              new Promise(r => this.waitingKeys.set(waitingKey, r))
            );
          }
        }

        return await Promise.all(promisesToWait);
        // // for debug!!
        // let tempTimer = 0;
        // const fromResp = Promise.all(promisesToWait);
        // while (true) {
        //   const fromTime: Promise<number> = new Promise(r =>
        //     setTimeout(() => r(-1), 10000)
        //   );
        //   const resp = await Promise.race([fromTime, fromResp]);
        //   if (resp === -1) {
        //     const preetyStr = waitingKeys.join(", ");
        //     tempTimer += 10;
        //     console.error(
        //       `IO_WAIT ${preetyStr} already wait for ${tempTimer}s`
        //     );
        //   } else {
        //     return resp as ProtocolMessage[];
        //   }
        // }
      }
    );

    instructionExecutor.register(
      Opcode.WRITE_COMMITMENT,
      async (args: any[]) => {
        const [commitType, commitment, id, intermidiarySigs] = args;

        if (commitType === CommitmentType.WithdrawETH) {
          const multisigAddress = id;
          await this.requestHandler.store.storeWithdrawalCommitment(
            multisigAddress,
            commitment
          );
        } else {
          if (intermidiarySigs) {
            await this.requestHandler.store.setIntermidiariesSignatureForApp(
              id,
              intermidiarySigs as Map<string, Signature>
            );
          }
          await this.requestHandler.store.setCommitment(
            [commitType, id],
            commitment
          );
        }
      }
    );

    instructionExecutor.register(Opcode.LOCK_ACQUIRE, async (args: any[]) => {
      const waitingKey: string = args[0];
      let message: ProtocolMessage = args[1];
      const multi: any = args[2];
      let multisigAddress: string;

      if (multi instanceof Array) {
        multisigAddress = await this.requestHandler.store.getMultisigAddressFromOwnersHash(
          hashOfOrderedPublicIdentifiers(multi)
        );
      } else {
        multisigAddress = multi;
      }

      // initiating should send message to counterparty anyway, without signature(done in machine)
      // CODITION: i am initiator OR receive signature'd message
      while (true) {
        const got: {
          s?: StateChannel;
          m?: ProtocolMessage;
        } = await this.lockerQueue.add(async () => {
          if (!this.lockedChannels.has(multisigAddress)) {
            let channel: StateChannel;
            try {
              channel = await this.requestHandler.store.getStateChannel(
                multisigAddress
              );
            } catch (error) {
              // no channel! return empty channel
              // this act as placeholder
              channel = StateChannel.createEmptyChannel(multisigAddress, []);
            }
            const initiator =
              channel.userNeuteredExtendedKeys[
                channel.numOperationExecuted % 2
              ];

            if (
              channel.multisigAddress.length === 66 &&
              (message.protocol === Protocol.InstallVirtualApp ||
                message.protocol === Protocol.UninstallVirtualApp)
            ) {
              // this is a virtual channel! No extra condition
              // it's a dirty "if"
              this.lockedChannels.add(multisigAddress);
              return { s: channel, m: message };
            }
            if (
              !channel.userNeuteredExtendedKeys.includes(this.publicIdentifier)
            ) {
              throw Error(`Getting channels not belongs to me...!`);
            }
            if (this.publicIdentifier === initiator) {
              // no extra condition for initiator!
              this.lockedChannels.add(multisigAddress);
              return { s: channel, m: message };
            }
            if ((message.signatures || []).length > 0) {
              this.lockedChannels.add(multisigAddress);
              return { s: channel, m: message };
            }
            if (this.pendingProtocolMessage.has(waitingKey)) {
              const newMsg = this.pendingProtocolMessage.get(waitingKey)!;
              if ((newMsg.signatures || []).length > 0) {
                // consume!!
                this.pendingProtocolMessage.delete(waitingKey);
                this.lockedChannels.add(multisigAddress);
                return { s: channel, m: newMsg };
              }
              throw Error("SHOULD NOT BE HERE");
            }
            return {};
          }
          return {};
        });

        if (got.s && got.m) {
          return [got.s, got.m];
        }

        // if (isUnlocked) {
        //   // else
        //   await this.lockerQueue.add(async () => {
        //     this.lockedChannels.delete(multisigAddr);
        //     this.internal.emit(multisigAddr, false);
        //   });
        // }

        // fail to acquire both channel if you are here
        const promisesToRace: Promise<any>[] = [
          // new Promise(r => setTimeout(() => r([-1, null]), 10000))
        ];

        // wait message or wait channel state changed
        promisesToRace.push(
          new Promise(r =>
            this.internal.once(multisigAddress, () => {
              r(null);
            })
          )
        );

        // or receive message from initiator
        promisesToRace.push(
          new Promise(r => {
            if (this.pendingProtocolMessage.has(waitingKey)) {
              const msg = this.pendingProtocolMessage.get(waitingKey)!;
              this.pendingProtocolMessage.delete(waitingKey);
              r(msg);
            } else {
              promisesToRace.push(
                new Promise(r => this.waitingKeys.set(waitingKey, r))
              );
            }
          })
        );

        // const firstRet: [number, any] = await Promise.race(promisesToRace);
        // if (firstRet[1] && firstRet[1].fromXpub) {
        //   // messages[firstRet[0]] = firstRet[1];
        //   this.pendingProtocolMessage.set(
        //     waitingKeys[firstRet[0]],
        //     firstRet[1]
        //   );
        //   // this.waitingKeys.delete(waitingKeys[firstRet[0] === 0 ? 1 : 0]); // may boom
        // }
        // // let's delete both waiting key
        // for (const k of waitingKeys) {
        //   this.waitingKeys.delete(k);
        // }

        // // hopefully prevent some op continuously reject
        // await new Promise(r => setTimeout(r, Math.floor(Math.random() * 100)));

        while (true) {
          const firstRet: any = await Promise.race(promisesToRace);
          if (firstRet && firstRet.fromXpub) {
            message = firstRet;
          }
          // let's delete waiting key
          this.waitingKeys.delete(waitingKey);
          // for (const k of waitingKeys) {
          //   this.waitingKeys.delete(k);
          // }
          break;
        }
      }
    });

    instructionExecutor.register(Opcode.LOCK_RELEASE, async (args: any[]) => {
      const sc: StateChannel = args[0];
      this.lockerQueue.add(async () => {
        await this.requestHandler.store.saveStateChannel(sc);
        this.lockedChannels.delete(sc.multisigAddress);
        this.internal.emit(sc.multisigAddress);
      });
    });

    instructionExecutor.register(
      Opcode.OP_GET_INTERMEDIARY_SIG,
      async (args: any[]) => {
        const appIdentityHash: string = args[0];
        return await this.requestHandler.store.getIntermidiariesSignatureForApp(
          appIdentityHash
        );
      }
    );

    instructionExecutor.register(
      Opcode.OP_MAP_REALISED_ID_HASH,
      async (args: any[]) => {
        const proposedAppIdentityHash: string = args[0];
        const installedAppIdentityHash: string = args[1];
        return await this.requestHandler.store.saveProposedAndIntsalledMapping(
          proposedAppIdentityHash,
          installedAppIdentityHash
        );
      }
    );

    return instructionExecutor;
  }

  /**
   * This is the entrypoint to listening for messages from other Nodes.
   * Delegates setting up a listener to the Node's outgoing EventEmitter.
   * @param event
   * @param callback
   */
  on(event: string, callback: (res: any) => void) {
    this.outgoing.on(event, callback);
  }

  /**
   * This is the entrypoint to listening for messages from other Nodes.
   * Delegates setting up a listener to the Node's outgoing EventEmitter.
   * @param event
   * @param callback
   */
  onError(event: string, callback: (res: any) => void) {
    this.error.on(event, callback);
  }

  /**
   * Stops listening for a given message from other Nodes. If no callback is passed,
   * all callbacks are removed.
   *
   * @param event
   * @param [callback]
   */
  off(event: string, callback?: (res: any) => void) {
    this.outgoing.off(event, callback);
  }

  /**
   * This is the entrypoint to listening for messages from other Nodes.
   * Delegates setting up a listener to the Node's outgoing EventEmitter.
   * It'll run the callback *only* once.
   *
   * @param event
   * @param [callback]
   */
  once(event: string, callback: (res: any) => void) {
    this.outgoing.once(event, callback);
  }

  /**
   * Makes a direct call to the Node for a specific method.
   * @param method
   * @param req
   */
  async call(req: NodeTypes.MethodRequest): Promise<NodeTypes.MethodResponse> {
    // // FOR DEBUG ONLY!
    // const fromOp = this.requestHandler.callInteractiveMethod(
    //   req,
    //   NodeExecutionState.INITIATING
    // );
    // const fromTimeout: Promise<number> = new Promise(r =>
    //   setTimeout(() => r(-1), 45000)
    // );
    // const result = await Promise.race([fromOp, fromTimeout]);
    // if (result === -1) {
    //   throw Error(`Operation ${req.requestId}@${req.type} timeout after 45s`);
    // }
    // return result as NodeTypes.MethodResponse;
    return this.requestHandler.callInteractiveMethod(
      req,
      NodeExecutionState.INITIATING
    );
  }

  async callStatic(
    req: NodeTypes.MethodRequest
  ): Promise<NodeTypes.MethodResponse> {
    return this.requestHandler.callStaticMethod(req.type, req);
  }

  /**
   * When a Node is first instantiated, it establishes a connection
   * with the messaging service. When it receives a message, it emits
   * the message to its registered subscribers, usually external
   * subscribed (i.e. consumers of the Node).
   */
  // private retryMessage: Map<NodeMessage, number> = new Map();
  private registerMessagingConnection() {
    this.messagingService.onReceive(
      this.publicIdentifier,
      async (msg: NodeMessage) => {
        await this.handleReceivedMessage(
          msg as NodeMessageWrappedProtocolMessage
        );
        // this.outgoing.emit(msg.type, msg);
      }
    );
  }

  /**
   * Messages received by the Node fit into one of three categories:
   *
   * (a) A NodeMessage which is _not_ a NodeMessageWrappedProtocolMessage;
   *     this is a standard received message which is handled by a named
   *     controller in the _events_ folder.
   *
   * (b) A NodeMessage which is a NodeMessageWrappedProtocolMessage _and_
   *     has no registered _ioSendDeferral_ callback. In this case, it means
   *     it will be sent to the protocol message event controller to dispatch
   *     the received message to the instruction executor.
   *
   * (c) A NodeMessage which is a NodeMessageWrappedProtocolMessage _and_
   *     _does have_ an _ioSendDeferral_, in which case the message is dispatched
   *     solely to the deffered promise's resolve callback.
   */
  private async handleReceivedMessage(msg: NodeMessage) {
    if (!Object.values(NODE_EVENTS).includes(msg.type)) {
      console.error(`Received message with unknown event type: ${msg.type}`);
    }
    if (!Object.values(NodeTypes.MethodName).includes(msg.method)) {
      console.error(`Received message with unknown method type: ${msg.method}`);
    }

    switch (msg.state) {
      case NodeExecutionState.RUNNING_PROTOCOL:
        const protocolMsg: ProtocolMessage = (msg as NodeMessageWrappedProtocolMessage)
          .data;
        const waitingKey = `${msg.requestId}${msg.from}${
          protocolMsg.messageType
        }`;
        if (this.waitingKeys.has(waitingKey)) {
          // someone is waiting this message!
          // pass it to resolve
          this.waitingKeys.get(waitingKey)!(protocolMsg);
          this.waitingKeys.delete(waitingKey);
        } else if (protocolMsg.messageType !== 0) {
          // this message intend to reply, but arrive before wait
          this.pendingProtocolMessage.set(waitingKey, protocolMsg);
        } else {
          try {
            await this.requestHandler.callInteractiveMethod(
              {
                type: msg.method,
                requestId: msg.requestId,
                params: {},
                protocolMessage: protocolMsg
              },
              msg.state
            );
          } catch (e) {
            // this is for passing test, should send error message to counterparty instead
            throw e;
            // console.error(e.message);
          }
        }
        break;
      case NodeExecutionState.FINISHING:
        const param: NodeTypes.MethodParams = (msg as any).data;
        await this.requestHandler.callInteractiveMethod(
          {
            type: msg.method,
            requestId: msg.requestId,
            params: param
          },
          msg.state
        );
        break;
      default:
        throw Error("Wrong execution flow");
    }
  }
}

const isBrowser =
  typeof window !== "undefined" &&
  {}.toString.call(window) === "[object Window]";

export function debugLog(...messages: any[]) {
  try {
    const logPrefix = "NodeDebugLog";
    if (isBrowser) {
      if (localStorage.getItem("LOG_LEVEL") === "DEBUG") {
        // for some reason `debug` doesn't actually log in the browser
        console.info(logPrefix, messages);
        console.trace();
      }
      // node.js side
    } else if (
      process.env.LOG_LEVEL !== undefined &&
      process.env.LOG_LEVEL === "DEBUG"
    ) {
      console.debug(logPrefix, JSON.stringify(messages, null, 4));
      console.trace();
      console.log("\n");
    }
  } catch (e) {
    console.error("Failed to log: ", e);
  }
}
