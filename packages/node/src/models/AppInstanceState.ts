import { Node as NodeTypes } from "@counterfactual/types";
import { Signature } from "ethers/utils";

import { Protocol } from "../machine";
import { ProtocolExecutionState, ProtocolState } from "../machine/enums";

type ContextStore = {
  commitment: string;
  signatures: Map<string, Signature>; // xpub to signature
};

export type AppInstanceStateJson = {
  requestId: string;
  multiSigAddresses: string[];
  protocol: Protocol;
  nodeMethod: NodeTypes.MethodName;
  nodeParams: NodeTypes.MethodParams;
  protocolExecutionState: ProtocolExecutionState;
  protocolState: ProtocolState;
  contextsStore: [
    string,
    { commitment: string; signatures: [string, Signature][] }
  ][];
  retryCount: number;
  lastUpdate: number;
};

export class AppInstanceState {
  // id: string;
  requestId: string;
  multiSigAddresses: string[]; // multisig address this operation working on
  protocol: Protocol;
  nodeMethod: NodeTypes.MethodName;
  nodeParams: NodeTypes.MethodParams;
  // states, from bigger to smaller
  protocolState: ProtocolState;
  protocolExecutionState: ProtocolExecutionState;

  contextsStore: Map<string, ContextStore>;
  retryCount: number;
  lastUpdate: number;

  constructor(
    requestId: string,
    multiSigAddresses: string[],
    protocol: Protocol,
    method: NodeTypes.MethodName,
    nodeParams: NodeTypes.MethodParams
  ) {
    this.requestId = requestId;
    // this.id = id;
    this.multiSigAddresses = multiSigAddresses;
    this.protocol = protocol;
    this.nodeMethod = method;
    this.nodeParams = nodeParams;
    this.protocolExecutionState = ProtocolExecutionState.ACTIVE;
    this.protocolState = 0;
    this.contextsStore = new Map();
    this.retryCount = 0;
    this.lastUpdate = Date.now();
  }

  toJson(): AppInstanceStateJson {
    this.lastUpdate = Date.now();
    return {
      // id: this.id,
      requestId: this.requestId,
      multiSigAddresses: this.multiSigAddresses,
      protocol: this.protocol,
      nodeMethod: this.nodeMethod,
      nodeParams: this.nodeParams,
      protocolExecutionState: this.protocolExecutionState,
      protocolState: this.protocolState,
      contextsStore: [...this.contextsStore.entries()].map(d => {
        const contextStore = {
          commitment: d[1].commitment,
          signatures: [...d[1].signatures.entries()]
        };
        return [d[0], contextStore];
      }),
      retryCount: this.retryCount,
      lastUpdate: this.lastUpdate
    };
  }

  static fromJson(source: AppInstanceStateJson) {
    const ais = new AppInstanceState(
      source.requestId,
      source.multiSigAddresses,
      source.protocol,
      source.nodeMethod,
      source.nodeParams
    );
    if (!source.contextsStore) {
      source.contextsStore = [];
    }
    ais.protocolExecutionState = source.protocolExecutionState;
    ais.protocolState = source.protocolState;
    ais.contextsStore = new Map([
      ...source.contextsStore.map(
        (c): [string, ContextStore] => {
          const contextStore = {
            commitment: c[1].commitment,
            signatures: new Map([...(c[1].signatures || [])])
          } as ContextStore;
          return [c[0], contextStore];
        }
      )
    ]);
    ais.retryCount = source.retryCount;
    ais.lastUpdate = source.lastUpdate;
    return ais;
  }

  saveCommitment(commitmentName: string, commitment: string) {
    if (this.contextsStore.has(commitmentName)) {
      throw Error("Commitments must store before any signatures.");
    }
    this.contextsStore.set(commitmentName, {
      commitment: commitment,
      signatures: new Map()
    });
  }

  saveSignature(commitmentName: string, xpub: string, signature: Signature) {
    if (!this.contextsStore.has(commitmentName)) {
      throw Error("Commitment name not found");
    }
    const sigs = this.contextsStore.get(commitmentName)!.signatures;
    sigs.set(xpub, signature);
    this.contextsStore.set(commitmentName, {
      ...this.contextsStore.get(commitmentName)!,
      signatures: sigs
    });
  }

  getCommitment(commitmentName: string): string | undefined {
    if (this.contextsStore.has(commitmentName)) {
      return this.contextsStore.get(commitmentName)!.commitment;
    }
    return undefined;
  }

  getSignaturesFor(commitmentName: string): Signature[] {
    // BAD?
    if (this.contextsStore.has(commitmentName)) {
      return [...this.contextsStore.get(commitmentName)!.signatures.values()];
    }
    return [];
  }

  getSignaturesMapFor(commitmentName: string): Map<string, Signature> {
    // BAD?
    if (this.contextsStore.has(commitmentName)) {
      return this.contextsStore.get(commitmentName)!.signatures;
    }
    return new Map<string, Signature>();
  }

  getSignatureFor(commitmentName: string, xpub: string): Signature {
    if (!this.contextsStore.has(commitmentName)) {
      throw Error("Commitment name not found");
    }
    const signatures = this.contextsStore.get(commitmentName)!.signatures;
    if (!signatures.has(xpub)) {
      throw Error(`Signature for ${xpub} not found`);
    }
    return signatures.get(xpub)!;
  }

  setStateTerminal() {
    this.protocolExecutionState = ProtocolExecutionState.INACTIVE;
  }

  isProtocolStateTerminal() {
    return this.protocolExecutionState === ProtocolExecutionState.INACTIVE;
  }
}
