import {
  CreateChannelController,
  DepositController,
  DisputeCancelController,
  DisputeResolutionController,
  DisputeStateController,
  DisputeTransaction,
  DisputeVirtualAgreementController,
  DisputeVirtualStateController,
  GetAllChannelAddressesController,
  GetAppInstanceController,
  GetAppInstanceDetailsController,
  GetAppInstanceStateController,
  GetChannelAddressFromCounterController,
  GetFreeBalanceStateController,
  GetInstalledAppInstancesController,
  GetMyFreeBalanceForStateController,
  GetProposedAppInstancesController,
  InstallAppInstanceController,
  InstallVirtualAppInstanceController,
  ProposeCancelDisputeController,
  ProposeInstallAppInstanceController,
  ProposeInstallVirtualAppInstanceController,
  RejectInstallController,
  TakeActionController,
  UninstallController,
  UninstallVirtualController,
  UpdateStateController,
  WithdrawController
} from "./methods";

const controllers = [
  /**
   * Stateful / interactive methods
   */
  CreateChannelController,
  DepositController,
  DisputeCancelController,
  DisputeStateController,
  DisputeVirtualStateController,
  DisputeVirtualAgreementController,
  DisputeResolutionController,
  DisputeTransaction,
  InstallAppInstanceController,
  InstallVirtualAppInstanceController,
  ProposeInstallAppInstanceController,
  ProposeInstallVirtualAppInstanceController,
  ProposeCancelDisputeController,
  RejectInstallController,
  TakeActionController,
  UninstallController,
  UninstallVirtualController,
  UpdateStateController,
  WithdrawController
];

const staticControllers = [
  /**
   * Constant methods
   */
  GetAllChannelAddressesController,
  GetChannelAddressFromCounterController,
  GetAppInstanceController,
  GetAppInstanceStateController,
  GetAppInstanceDetailsController,
  GetFreeBalanceStateController,
  GetMyFreeBalanceForStateController,
  GetInstalledAppInstancesController,
  GetProposedAppInstancesController
];

/**
 * Converts the array of connected controllers into a map of
 * Node.MethodNames to the _executeMethod_ method of a controller.
 *
 * Throws a runtime error when package is imported if multiple
 * controllers overlap (should be caught by compiler anyway).
 */
export const methodToImplementation = controllers.reduce((acc, controller) => {
  if (acc[controller.methodName]) {
    throw new Error(
      `Fatal: Multiple controllers connected to ${controller.methodName}`
    );
  }

  // const handler = new controller();

  acc[controller.methodName] = new controller();
  return acc;
}, {});

export const staticMethodToImplementation = staticControllers.reduce(
  (acc, controller) => {
    if (acc[controller.methodName]) {
      throw new Error(
        `Fatal: Multiple controllers connected to ${controller.methodName}`
      );
    }

    const handler = new controller();

    acc[controller.methodName] = handler.execute.bind(handler);
    return acc;
  },
  {}
);
